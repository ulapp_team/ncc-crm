import { LightningElement } from 'lwc';
import FOOTER_IMAGE from '@salesforce/resourceUrl/communitySurveyIndependentFooter';

export default class SurveyFooter extends LightningElement {
    footerImage = FOOTER_IMAGE;
}