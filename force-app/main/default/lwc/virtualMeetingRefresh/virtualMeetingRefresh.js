import { api, LightningElement, wire } from 'lwc';
// import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getRecord, getFieldValue, getRecordNotifyChange } from 'lightning/uiRecordApi';
import { subscribe, unsubscribe, onError } from 'lightning/empApi';
import MEETING_INFO_FIELD from '@salesforce/schema/Session__c.Meeting_Info__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VirtualMeetingRefresh extends LightningElement {
  @api recordId;

//   @wire(getRecord, { recordId: '$recordId',[MEETING_INFO_FIELD] }) sessionRec

  @wire(getRecord, { recordId : '$recordId',fields:[MEETING_INFO_FIELD]})
  session;


  channelName = '/data/Session__ChangeEvent';
  subscription = {}; // holds subscription, used for unsubscribe

  connectedCallback() {
    this.registerErrorListener();
    this.registerSubscribe();
  }

  disconnectedCallback() {
    unsubscribe(this.subscription, () => console.log('Unsubscribed to change events.'));
  }

  // Called by connectedCallback()
  registerErrorListener() {
    onError(error => {
      console.error('Salesforce error', JSON.stringify(error));
    });
  }

  // Called by connectedCallback()
  registerSubscribe() {
    const changeEventCallback = changeEvent => {
      this.processChangeEvent(changeEvent);
    };

    // Sets up subscription and callback for change events
    subscribe(this.channelName, -1, changeEventCallback).then(subscription => {
      this.subscription = subscription;
    });
  }

  // Called by registerSubscribe()
  processChangeEvent(changeEvent) {
    try {
      const recordIds = changeEvent.data.payload.ChangeEventHeader.recordIds; // avoid deconstruction
      const changeOrigin = changeEvent.data.payload.ChangeEventHeader.changeOrigin;
      const changeType = changeEvent.data.payload.ChangeEventHeader.changeType;
        if (recordIds.includes(this.recordId)){
            console.log('ChangeEventHeader >>> ',JSON.parse(JSON.stringify(changeEvent.data.payload.ChangeEventHeader)));
            let apexChangeOrigin = 'com/salesforce/api/apex/';
            if(changeType === 'UPDATE' && changeOrigin.includes(apexChangeOrigin)){
                //changed by apex
                let changeFields = changeEvent.data.payload.ChangeEventHeader.changedFields;
                if(changeFields.includes('Meeting_Info__c')){
                    getRecordNotifyChange([{ recordId: this.recordId }]); // Refresh all components
                    this.showToast();
                }
            }
        } 
    } catch (err) {
      console.error(err);
    }
  }

  showToast() {
    const event = new ShowToastEvent({
        title: 'Meeting Info successfully loaded.',
        // message: 'Virtual Meeting Info successfully loaded.',
        variant: 'success',
        mode: 'dismissable'
    });
    this.dispatchEvent(event);
}

  get magicNumber() {
    return getFieldValue(this.session.data, MEETING_INFO_FIELD) || 'Test';
  }
}