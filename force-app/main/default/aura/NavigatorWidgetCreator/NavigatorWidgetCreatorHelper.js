({
    updateButtonsState: function(component) {
        let currentTab = component.get("v.currentTabIndex");
        let tabsCompletion = component.get("v.tabsCompletionStatus");
        
        component.set("v.isNextDisabled", !tabsCompletion[currentTab]);
        
        let allTabsCompleted = tabsCompletion.every(status => status);
        component.set("v.isSaveDisabled", !allTabsCompleted);
        
        component.set("v.isLoading",false);
    },
    
    submitForm: function(widget) {
        /*return new Promise((resolve, reject) => {
            widget.submit();
            resolve();
        });*/
        widget.submit();
    },

    showToast: function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})