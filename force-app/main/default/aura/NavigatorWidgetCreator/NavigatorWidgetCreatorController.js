({
    doInit : function(component, event, helper) {
        component.set("v.isLoading",true);
        console.log(component.get("v.recordId"));
        
        let action = component.get("c.getRelatedWidgets");
        action.setParams({
            "navigatorId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            let state = response.getState();
            
            if (state === "SUCCESS") {
                let relatedWidgets = response.getReturnValue();
                console.log(relatedWidgets);
                if (relatedWidgets && relatedWidgets.length > 0) {
                    component.set("v.isWizardDisabled",true);
                } else {
                    component.set("v.isWizardDisabled",false);
                }
            } else {
                let errors = response.getError();
                console.error(errors);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    navigateTabs : function(component, event, helper) {
        var action = event.getSource().get("v.name");
        var currentIndex = component.get("v.currentTabIndex");
        console.log(action);
        if (action === "Previous" && currentIndex > 0) {
            currentIndex--;
        } else if(action === "Next" && currentIndex < 4) { // 4 because we have 5 tabs (0 to 4 index)
            currentIndex++;
        } 
        
        console.log(currentIndex);
        if (currentIndex == 0) {
            component.set("v.isPreviousHidden",true);
            component.set("v.isViewDisabled",true);
            component.set("v.isFirstWidget",true);
        } else {
            component.set("v.isPreviousHidden",false);
            component.set("v.isViewDisabled",false);
            component.set("v.isFirstWidget",false);
        }
        
        component.set("v.defaultView",component.get("v.defaultViewList")[currentIndex]);
        
        if (currentIndex == 4) {
            component.set("v.isNextHidden",true);
        } else {
            component.set("v.isNextHidden",false);
        }
        
        component.set("v.defaultSize", component.get("v.defaultSizeList")[currentIndex]);
        
        component.set("v.currentTabIndex", currentIndex);
        helper.updateButtonsState(component);
    },
    
    handleInputChange: function(component, event, helper) {
        let currentTab = component.get("v.currentTabIndex");
        let allRequiredInputs = component.find("requiredInput");
        let allViewInputs = component.find("widgetView"); //still a requiredField
        let allNameInputs = component.find("widgetName"); //still a requiredField
        
        component.set("v.isLoading",true);
        
        // If there's only one input (for the first time), it won't be an array. Handle that scenario.
        if(!Array.isArray(allRequiredInputs)) {
            allRequiredInputs = [allRequiredInputs];
        }
        
        if (allViewInputs) {
            if(!Array.isArray(allViewInputs)) {
                allViewInputs = [allViewInputs];
            }
        }
        
        if (allNameInputs) {
            if(!Array.isArray(allNameInputs)) {
                allNameInputs = [allNameInputs];
            }
        }
        
        let allInputs;
        
        if (allNameInputs) {
            allInputs = allRequiredInputs.concat(allNameInputs);
        } else {
            allInputs = allRequiredInputs;
        }
        
        if (allViewInputs) {
            allInputs = allRequiredInputs.concat(allViewInputs);
        } else {
            allInputs = allRequiredInputs;
        }
        
        
        if(component.get("v.totalRequiredFieldsPerWidget") == 0) {
            component.set("v.totalRequiredFieldsPerWidget", allInputs.length);
        }
        
        if (component.get("v.totalRequiredFieldsPerWidget") * (currentTab + 1) != allInputs.length) {
            allInputs.length -= allInputs.length - component.get("v.totalRequiredFieldsPerWidget") * (currentTab + 1);
        }
        
        let allFilled = allInputs.reduce((validSoFar, inputCmp) => {
            return validSoFar && !!inputCmp.get("v.value");
        }, true);
        
        let tabsCompletion = component.get("v.tabsCompletionStatus");
        tabsCompletion[currentTab] = allFilled;
        
        console.log(tabsCompletion[currentTab]);
        
        component.set("v.tabsCompletionStatus", tabsCompletion);
        
        helper.updateButtonsState(component);
    },
    
    afterFormLoad: function(component, event, helper) {
        if (!component.get("v.showImagePreview")) {
            component.set("v.showImagePreview", true);
        }
        
        //for Widget Name
        let widgetNameComponents = component.find("widgetName");
        try {
            console.log(widgetNameComponents);
            // Ensure it's always an array for consistent handling
            if (!Array.isArray(widgetNameComponents)) {
                widgetNameComponents = [widgetNameComponents];
            }
            
            widgetNameComponents.forEach(widgetNameComponent => {
                let dataId = widgetNameComponent.get("v.class"); // If you've used the label attribute to store data-id
                console.log(dataId);
                
                if (dataId==0) {
                    widgetNameComponent.set("v.value",'Event Activity');
                }
                
            });
        } catch (err) {
            console.error(err);
        }
        
        
        component.set("v.isLoading",false);
    },
    
	 handleSubmit: function(component, event, helper) {
        event.preventDefault(); // stop the form from submitting
        var fields = event.getParam("fields");
         console.log("SUBMIT TRIGGERED");
        // You can do additional processing here if needed
        //component.find("yourFormId").submit(fields);
    },
    
    saveWidgets: function(component, event, helper) {
        component.set("v.isLoading",true);
        
        let allWidgets = component.find("recordEditForm");
        let widgetsToSave = [];
        console.log(allWidgets);
        
        if (!Array.isArray(allWidgets)) {
            allWidgets = [allWidgets]; // Ensure it's always an array for consistent handling
        }        
        
        //for widget Size
        let inputComponents = component.find("widgetSize");
        try {
            console.log(inputComponents);
            // Ensure it's always an array for consistent handling
            if (!Array.isArray(inputComponents)) {
                inputComponents = [inputComponents];
            }
            
            inputComponents.forEach(inputComponent => {
                let dataId = inputComponent.get("v.class"); // If you've used the label attribute to store data-id
                console.log(dataId);
                
                console.log(component.get("v.defaultSizeList")[dataId]);
                inputComponent.set("v.value",component.get("v.defaultSizeList")[dataId]);
            });
        } catch (err) {
            console.error(err);
        }       
        
        //for Widget View
        let widgetViewComponents = component.find("widgetView");
        try {
            console.log(widgetViewComponents);
            // Ensure it's always an array for consistent handling
            if (!Array.isArray(widgetViewComponents)) {
                widgetViewComponents = [widgetViewComponents];
            }
            
            widgetViewComponents.forEach(widgetViewComponent => {
                let dataId = widgetViewComponent.get("v.class"); // If you've used the label attribute to store data-id
                console.log(dataId);
                
                if (dataId==0) {
                    console.log(component.get("v.defaultViewList")[dataId]);
                    widgetViewComponent.set("v.value",component.get("v.defaultViewList")[dataId]);
                }
                
            });
        } catch (err) {
            console.error(err);
        }
        
        
        //allWidgets.map(widget => helper.submitForm(widget));
        var counter = 0;
        console.log(allWidgets);
        
        allWidgets.forEach((widget, index) => {
            console.log('inside');
            /*let field = component.find("widgetSize" + index);
            //let field = widget.find("widgetSize");
            console.log(field);
            if (field) {
                let currentValue = field.get("v.value");
                
                // Modify the value as needed. Here's a simple example:
                let newValue = component.get("v.counter")[currentIndex];
                field.set("v.value", newValue);
            	counter++;
            }*/
            
            helper.submitForm(widget);
        }).catch(error => {
            console.error('Error saving widgets:', error);
            helper.showToast('Error', 'Error saving widgets. Please try again.', 'error');
        });
        
        /*let promises = allWidgets.map(widget => helper.submitForm(widget)); // Convert each widget save operation into a promise
        
        Promise.all(promises).then(results => {
            // All widgets saved successfully
            console.log('All widgets saved:', results);
            component.set('v.isModalOpen', false);  // Close the modal after successful save
            $A.get('e.force:refreshView').fire();   // Refresh the view to reflect the new data (optional)
            helper.showToast('Success', 'All widgets have been saved successfully!', 'success');
        }).catch(error => {
            console.error('Error saving widgets:', error);
            helper.showToast('Error', 'Error saving widgets. Please try again.', 'error');
        });*/
    },

    handleSuccess: function(component, event, helper) {
        var updatedRecord = event.getParam("response");
        
        var widgetTabsStatus = component.get("v.widgetTabsStatus");
        var widgetTabsErrors = component.get("v.widgetTabsErrors");
        
        widgetTabsStatus.push(updatedRecord.id);
        widgetTabsErrors.push(null);
        
        component.set("v.widgetTabsStatus", widgetTabsStatus);
        component.set("v.widgetTabsErrors", widgetTabsErrors);
        
        console.log("Save successful: " + updatedRecord.id);
        
        var currentCount = component.get("v.totalSuccessCounter");
        component.set("v.totalSuccessCounter", currentCount + 1);
        if (component.get("v.totalSuccessCounter") >= 5) {
            component.set("v.isLoading",false);
            component.set('v.isModalOpen', false);
            component.set("v.isWizardDisabled",true);
            $A.get('e.force:refreshView').fire();  
            helper.showToast('Success', 'All widgets have been saved successfully!', 'success');
        }
    },
            
    handleError: function(component, event, helper) {
        component.set("v.isLoading",false);
        helper.showToast('Error', 'Error saving widgets. Please try contact your administrator.', 'error');
        
        let errors = event.getParam("error");
        console.error("Save error:", errors);
        
        var widgetTabsStatus = component.get("v.widgetTabsStatus");
        var widgetTabsErrors = component.get("v.widgetTabsErrors");
        
        widgetTabsStatus.push(null);
        widgetTabsErrors.push(errors.message);
        
        component.set("v.widgetTabsStatus", widgetTabsStatus);
        component.set("v.widgetTabsErrors", widgetTabsErrors);
        
    },
    
    openModal : function(component, event, helper) {
        component.set("v.isModalOpen", true);
    },

    closeModal : function(component, event, helper) {
        component.set("v.currentTabIndex", 0);
        helper.updateButtonsState(component);
        component.set("v.isModalOpen", false);
    },
})