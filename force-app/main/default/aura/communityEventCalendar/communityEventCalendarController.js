({
    scriptsLoaded : function(component, event, helper) {
        helper.getEvents(component, event,helper);
    },
    
    refreshPage : function(component, event, helper){
        console.log('Refresh Page');
        
        component.set('v.loaded',true);
        var delay=1500; // 1.5 seconds delay
        setTimeout(function() {
			$A.get('e.force:refreshView').fire();
    	}, delay);
        component.set('v.loaded',false);
    },
})