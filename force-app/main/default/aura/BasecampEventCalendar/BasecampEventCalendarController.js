({
    renderCalendar : function(component, event, helper) {
        var eventsMap = component.get("v.events");
         
        console.log('=========renderCalendar============');
        console.log(eventsMap);
        
        $(document).ready(function(){
            var eventArray = [];
            $.each(eventsMap, function(index, value){
                if(component.get('v.eventRecId') == null){
                    component.set('v.eventRecId', value.eventRecId);
                }
                
                var startDateTimeNew = new Date(value.startDateTime);
                var endDateTimeNew = new Date(value.endDateTime);
                var startDatetime_Start = moment(startDateTimeNew.toLocaleString('en-US', { timeZone: value.commonTimezone})).format();
                var startDatetime_End = moment(endDateTimeNew.toLocaleString('en-US', { timeZone: value.commonTimezone})).format();
				                
                component.set('v.dateDisabled', false);
                
                var newEvent = {
                    id : value.Id,
                    trueTitle : value.title,
                    status : value.status,
                    location: value.location,
                    title : value.title + ' | ' + value.status,
                    timezoneAbbr : value.timezoneAbbr,
                    start: startDatetime_Start, 
                    end : startDatetime_End, 
                    description : value.description,
                    owner : value.owner
                }
                eventArray.push(newEvent);
            });
            var calendarButtons = component.get('v.calendarButtons');
            $('#calendar').fullCalendar({
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: calendarButtons
                },
                defaultDate: component.get('v.defaultDate'),
                navLinks: false, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                weekends: component.get('v.weekends'),
                eventOrder: "title,-duration",
                eventOrderStrict: true,
                eventBackgroundColor: component.get('v.eventBackgroundColor'),
                eventBorderColor: component.get('v.eventBorderColor'),
                eventTextColor: component.get('v.eventTextColor'),
                events: eventArray,
                timezone: 'EDT', 
                eventResize: function(event, delta, revertFunc) {
                    var evObj = {
                        "Id" : event.id,
                        "title" : event.title,
                        "startDateTime" : moment(event.start._i).add(delta).utc().format(),
                        "endDateTime" : moment(event.end._i).add(delta).utc().format(),
                        "description" : event.description
                    };
                    helper.upsertEvent(component, evObj);
                },
                eventClick: function(event) {
                    if (event.url) {
                        return false;
                    }
                },
                dayClick: function(date, jsEvent, view) {
                    return false;
                },
                
                viewRender: function(view,element) {
                    var now = new Date();
                    var end = new Date();
                    
                    //Limit only to last month, this month and next month
                    end.setMonth(now.getMonth() + 1); 
                    now.setMonth(now.getMonth() - 1); 
                    
                    if ( end < view.end) {
                        $("#calendar .fc-next-button").hide();
                        return false;
                    }
                    else {
                        $("#calendar .fc-next-button").show();
                    }
                    
                    if ( view.start < now) {
                        $("#calendar .fc-prev-button").hide();
                        return false;
                    }
                    else {
                        $("#calendar .fc-prev-button").show();
                    }
                },

                eventRender: function(event, element) {
                    element.find('.fc-time')
                    .append('<span class="fc-time"> ' + 
                        event.timezoneAbbr + '</span>'
                    );
                }
            });
        });
    },
   
    closeModal : function(component, event, helper) {
        helper.closeModal(component, event);
    },
})