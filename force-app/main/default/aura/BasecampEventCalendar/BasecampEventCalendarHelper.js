({
    openModal : function(component, event) {
        component.set("v.isOpen", true);
    },
    closeModal : function(component, event) {
        component.set("v.isOpen", false);
    },
    saveAndRefresh : function(component,event){
        component.set('v.loaded',true);
        var delay=1500; // 1 seconds delay
        setTimeout(function() {
            $A.get('e.force:refreshView').fire();
        }, delay);
        component.set('v.loaded',false);
    },
    getEventTimezone : function(component, event, helper) {
        // CCN-EVE-3113-DV - Gian: Created new function to get the event object's timezone to be set as default timezone per session created
        const urlParams = new URLSearchParams(window.location.search);
        var action = component.get("c.getEventTimeZone");
    	var eventIdQueryParam = urlParams.get('id');
        action.setParams({
            "eventId": eventIdQueryParam
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.timezoneAbbr", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})