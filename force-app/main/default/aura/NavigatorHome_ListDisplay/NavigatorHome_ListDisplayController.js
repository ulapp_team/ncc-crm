({
	openEmail : function(component, event, helper) {
		const index = event.getSource().get("v.name");
        component.set("v.openedModalIndex", parseInt(index));
	}, 
    closeEmail : function(component, event, helper) {
		component.set("v.openedModalIndex", -1);
	}, 
})