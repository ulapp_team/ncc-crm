({
    doInit: function(component, event, helper) {
       
        helper.retrieveData(component);
        
    },

    handleChangeEventType : function(component, event, helper) {
        console.log('save');
        console.log('changeType >>> ', component.get("v.changeType"));
        console.log('newType >>> ', component.get("v.newType"));
        console.log('newPlatform >>> ', component.get("v.newPlatform"));
        console.log('optionAnswer >>> ',component.get("v.optionAnswer"));
        console.log('confirmMessage >>> ',component.get("v.confirmMessage"));
        let confirmMessage = component.get("v.confirmMessage");
        let optionAnswer = component.get("v.optionAnswer");

        let valid = helper.validateField(component,'evType');

        console.log('valid evType >>> ',valid);

        if(valid) valid = helper.validateField(component,'vMeet');

        console.log('valid vMeet >>> ',valid);

        if(valid && confirmMessage != '' && confirmMessage != undefined){
            if(optionAnswer == undefined || optionAnswer ==  ''){
                valid = false;
                console.log('valid optionAnswer >>> ',valid);
            } 
        }
        
        console.log('valid final >>> ',valid);

        if(valid){
            if(component.get("v.newType") == component.get("v.oldType") && component.get("v.newPlatform") == component.get("v.oldPlatform")){
                console.log('do nothing, no changes');
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "info",
                    "title": "Info",
                    "message": "The event type is still the same"
                });
                resultsToast.fire();
            }
            else if(component.get("v.newType") != component.get("v.oldType") && component.get("v.changeType") != '' && component.get("v.optionAnswer") != '' && component.get("v.optionAnswer") != undefined){
                console.log('processChanges');
                helper.processChanges(component);
            }
            else if(component.get("v.newType") != component.get("v.oldType") && component.get("v.changeType") != '' && component.get("v.optionAnswer") == undefined){
                console.log('option answer is undefined, update the fiedls but no integration');
                helper.processChanges(component);
            }
            else{
                console.log('No valid action');
            }
        }
    },

    handleCreateLoad : function(component, event, helper) {
        component.set('v.oldType',component.find("evType").get("v.value"));
        component.set('v.oldPlatform',component.find("vMeet").get("v.value"));
        component.set('v.newType',component.find("evType").get("v.value")); //Set Default;
        component.set('v.newPlatform',component.find("vMeet").get("v.value")); //Set Default;
        console.log(component.get('v.oldType'));
        console.log(component.get('v.oldPlatform'));
        component.set('v.isLoading',false);
        $A.util.addClass(component.find("spinnerCont"), "slds-hide");    
        $A.util.addClass(component.find("spinner"), "slds-hide");    
        if(component.get('v.oldType') == 'Virtual'){
            component.set("v.requireVMeet",true);
        }
        else{
            component.set("v.requireVMeet",false);
        }
    },

    handleTypeChange : function(component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let data = component.get("v.data");
        if(fieldName == 'Event_Type__c'){
            if(event.getParam("value") == 'Virtual'){
                component.set("v.requireVMeet",true);
            }
            else{
                component.set("v.requireVMeet",false);
            }
            component.set('v.newType',event.getParam("value"));

            console.log('newType >>> ',component.get('v.newType'));
            console.log('newPlatform >>> ',component.get('v.newPlatform'));
            console.log('oldType >>> ',component.get('v.oldType'));
            console.log('oldPlatform >>> ',component.get('v.oldPlatform'));
            console.log('data >>> ',data);

            if((component.get('v.oldType') == 'In-Person' || component.get('v.oldType') == 'Hybrid') && component.get('v.newType') == 'Virtual'){
                component.set("v.changeType","InPersonToVirtual");
            }
            if((component.get('v.oldType') == 'Virtual' || component.get('v.oldType') == 'Hybrid') && component.get('v.newType') == 'In-Person'){
                component.set("v.changeType","VirtualToInPerson");
            }

            if((component.get('v.oldType') == 'Virtual' || component.get('v.oldType') == 'Hybrid') && component.get('v.newType') == 'In-Person' && data.checkHasMSMeetingSession){
                console.log('Virtual to In-Person');
                component.set("v.changeType","VirtualToInPerson");
                component.set("v.optionClass","slds-show");
                // var uiMsg = component.find("uiMsg");
                // if($A.util.hasClass(uiMsg, "slds-hide")){
                //     $A.util.removeClass(uiMsg, "slds-hide");
                // }
                component.set('v.confirmMessage','You are changing a virtual event to onsite. This event has sessions that will be impacted. Are you sure you would like to remove the virtual meeting platform information from all session records within this event?');
                //YES -the meeting locations and any MS Team information would be removed from the Meeting Location and the Meeting info fields on all event records. Any additional meeting information text would remain in the field.
                //NO - the meeting locations and MS Team information would remain in existing sessions.
            }
            else if(component.get('v.oldType') == component.get('v.newType') || component.get('v.newType') == ''){
                component.set("v.changeType","");
                component.set("v.optionClass","slds-hide");
                component.set('v.confirmMessage','');
                component.find("vMeet").set("v.value",component.get('v.oldPlatform'));
                component.set('v.optionAnswer','');
            }
        } 
    },

    handleMeetingPlatformChange : function(component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let data = component.get("v.data");
        // if(fieldName == 'Event_Type__c'){
        //     if(event.getParam("value") == 'Virtual'){
        //         component.set("v.requireVMeet",true);
        //     }
        //     else{
        //         component.set("v.requireVMeet",false);
        //     }
        //     component.set('v.newType',event.getParam("value"));
        // } 
        if(fieldName == 'Virtual_Meeting_Platform__c'){
            component.set('v.newPlatform',event.getParam("value"));
        } 

        console.log('newType >>> ',component.get('v.newType'));
        console.log('newPlatform >>> ',component.get('v.newPlatform'));
        console.log('oldType >>> ',component.get('v.oldType'));
        console.log('oldPlatform >>> ',component.get('v.oldPlatform'));
        console.log('data >>> ',data);

        //In Person to Virtual 
        if((component.get('v.oldType') == 'In-Person' || component.get('v.oldType') == 'Hybrid') && component.get('v.newType') == 'Virtual' && component.get('v.newPlatform') !== undefined && data.checkHasSession){
            console.log('In Person to Virtual');
            //component.set("v.changeType","InPersonToVirtual");
            component.set("v.optionClass","slds-show");
            // var uiMsg = component.find("uiMsg");
            // if($A.util.hasClass(uiMsg, "slds-hide")){
            //     $A.util.removeClass(uiMsg, "slds-hide");
            // }
            component.set('v.confirmMessage','You are changing an onsite event to virtual and have selected a designated Virtual Meeting Platform. This event has sessions that will be impacted. Are you sure you would like to add the virtual meeting platform information on all session records within this event?');
            //YES - the meeting locations and any MS Team information would be added to the Meeting Location and the Meeting info fields on all event session records.
            //NO - no MS teams information or meeting links are added to existing sessions.
        }
        else if(component.get('v.oldType') == component.get('v.newType') || component.get('v.newType') == ''){
            component.set("v.changeType","");
            component.set("v.optionClass","slds-hide");
            component.set('v.confirmMessage','');
            component.set('v.optionAnswer','');
        }
        //Virtual to In-Person
        /*else if(component.get('v.oldType') == 'Virtual' && component.get('v.newType') == 'In-Person' && data.checkHasMSMeetingSession){
            console.log('Virtual to In-Person');
            component.set("v.changeType","VirtualToInPerson");
            component.set("v.optionClass","slds-show");
            // var uiMsg = component.find("uiMsg");
            // if($A.util.hasClass(uiMsg, "slds-hide")){
            //     $A.util.removeClass(uiMsg, "slds-hide");
            // }
            component.set('v.confirmMessage','You are changing a virtual event to onsite. This event has sessions that will be impacted. Are you sure you would like to remove the virtual meeting platform information from all session records within this event?');
            //YES -the meeting locations and any MS Team information would be removed from the Meeting Location and the Meeting info fields on all event records. Any additional meeting information text would remain in the field.
            //NO - the meeting locations and MS Team information would remain in existing sessions.
        }*/
        //Remove message
        /*else {
            console.log('Remove message');
            component.set("v.changeType","");
            component.set("v.optionClass","slds-hide");
            // var uiMsg = component.find("uiMsg");
            // if(!$A.util.hasClass(uiMsg, "slds-hide")){
            //     $A.util.addClass(uiMsg, "slds-hide");
            // }
            component.set('v.confirmMessage','');
        }*/
        console.log(component.get('v.confirmMessage'));
    },

    handleAnswerChange : function(component, event, helper) {
        console.log('handleAnswerChange >>> ', component.find("uiMsg").get("v.value"));
        component.set("v.optionAnswer",component.find("uiMsg").get("v.value"));
    },

})