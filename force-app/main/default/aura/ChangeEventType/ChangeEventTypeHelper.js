({
    retrieveData : function(component) {
        var action = component.get("c.getData");
        action.setParams({ eventId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var responseValue = response.getReturnValue(); 
                console.log('data >>> ',JSON.parse(JSON.stringify(responseValue)));
                component.set("v.data",responseValue);
                component.set("v.hasMSMeetingSession",component.get("v.data").checkHasMSMeetingSession);
                component.set("v.hasSession",component.get("v.data").checkHasSession);
                console.log('responseValue >>> ',component.get("v.data"));
            }
            else if(state === "ERROR"){
                var error = response.getError();
                if(error){
                    console.log("Errors"+JSON.stringify(error));
                    if(error[0] && error[0].message){
                        throw new Error("Error"+ error[0].message);
                    }
                }else{
                   throw new Error("Unknown Error");
                }
            }
            
        });
        
        // Enqueue Action
        $A.enqueueAction(action);
    },

    showSpinner: function(component) {
		var spinnerMain =  component.find("spinnerSave");
		$A.util.removeClass(spinnerMain, "slds-hide");
	},
 
	hideSpinner : function(component) {
		var spinnerMain =  component.find("spinnerSave");
		$A.util.addClass(spinnerMain, "slds-hide");
	},

    processChanges : function(component) {
        this.showSpinner(component);
        console.log("helper >> processChanges");
        var action = component.get("c.saveData");
        let hasMSMeeting = component.get("v.hasMSMeetingSession");
        let hasSession = component.get("v.hasSession");
        let changeType =  component.get("v.changeType");
        let optionAnswer =  component.get("v.optionAnswer");
        // let hasMSMeetingResult = hasMSMeeting ? 'hasMSMeeting' : 'hasNoMSMeeting';
        //let optionAnswer = component.get("v.optionAnswer") != '' && component.get("v.optionAnswer") != undefined ? ('-'+component.get("v.optionAnswer")) : '';
        //optionAnswer = changeType +'-'+hasMSMeetingResult+ optionAnswer;
        let integAction = '';
        // if(optionAnswer == '' || optionAnswer == undefined) integAction = 'NoAction';
        // else {
            if(changeType == 'InPersonToVirtual' && hasSession) {
                if(optionAnswer == 'Yes') integAction = changeType +'-'+ 'AddVirtualPlatformInfo';
                else if(optionAnswer == 'No') integAction = changeType +'-'+ 'DoNotAddVirtualPlatformInfo';
            }
            else if(changeType == 'InPersonToVirtual' && !hasSession){
                if(optionAnswer == 'Yes') integAction = changeType +'-'+ 'RunIntegration';
            }
            else if(changeType == 'VirtualToInPerson' && hasMSMeeting){
                if(optionAnswer == 'Yes') integAction = changeType +'-'+ 'RemoveVirtualPlatformInfo';
                else if(optionAnswer == 'No') integAction = changeType +'-'+ 'DoNotRemoveVirtualPlatformInfo';
            }
        // }
        console.log("helper >> integAction >> ",integAction);
        // InPersonToVirtual-AddVirtualPlatformInfo
        // InPersonToVirtual-DoNotAddVirtualPlatformInfo
        // InPersonToVirtual-RunIntegration
        // VirtualToInPerson-RemoveVirtualPlatformInfo
        // VirtualToInPerson-DoNotRemoveVirtualPlatformInfo

        action.setParams({  eventId : component.get("v.recordId"),
                            newType : component.get("v.newType"),
                            newPlatform : component.get("v.newPlatform"),
                            optionAnswer : integAction});
        action.setCallback(this, function(response) {
            var state = response.getState();
    
            if(state === "SUCCESS"){
                var responseValue = response.getReturnValue(); 
                console.log('data >>> ',JSON.parse(JSON.stringify(responseValue)));
                component.set("v.savedata",responseValue);
                console.log('savedata >>> ',component.get("v.savedata"));

                // Display the total in a "toast" status message
                this.hideSpinner(component);
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "success",
                    "title": "Success",
                    "message": "Event type successfully changed"
                });
                resultsToast.fire();
                $A.get('e.force:refreshView').fire();
                // Close the action panel
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();

            }else if(state === "ERROR"){
                this.hideSpinner(component);
                var error = response.getError();
                if(error){
                    console.log("Errors"+error);
                    if(error[0] && error[0].message){
                        throw new Error("Error"+ error[0].message);
                    }
                }else{
                   throw new Error("Unknown Error");
                }
            }
            
        });
        
        // Enqueue Action
        $A.enqueueAction(action);
        
    },

    validateField : function(component, auraId) {
        let fields = component.find(auraId);
        if (fields) {
            fields = Array.isArray(fields) ? fields : [fields];
        }
        else {
            fields = [];
        }
        const valid = fields.reduce((validSoFar, field) => {
            return (validSoFar && field.reportValidity());
        }, true);

        return valid;
    }
})