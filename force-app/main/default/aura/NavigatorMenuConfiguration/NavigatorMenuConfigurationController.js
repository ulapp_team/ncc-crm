({
	doInit : function(component, event, helper) {
		helper.getAvailableFields(component, event, helper);
	},
    
    handleRegistrationForm : function(component, event, helper){
        helper.saveRegistrationForm(component, event, helper);
    },
    
    handleEdit : function(component, event, helper){
        component.set("v.isReadOnly" , false);
    }, 

    handleCancel : function(component, event, helper){
        component.set("v.isReadOnly" , true);
        component.set("v.menuConfigurationList", component.get("v.menuConfigurationListCopy"));
    },

    checkAllFields : function(component, event, helper){
        let isChecked = event.getSource().get("v.checked");
        let checkBoxName = event.getSource().getLocalId();
        let resultList = component.get("v.menuConfigurationList");
        let hasRequiredInactive = false;

        for (let i = 0 ; i < resultList.length ; i++){
            if (resultList[i].key != 'My_Navigator') {
                if (checkBoxName == "selectAllFieldCheckbox"){
                    resultList[i].active = isChecked;
                } else {
                    if(resultList[i].active){
                        resultList[i].active = isChecked;
                    }else{
                        hasRequiredInactive = true;
                    }
                }
            }
        }

        if(hasRequiredInactive){
            component.set("v.requiredAll", false);
            helper.showToast("Error", "You have inactive fields.", "error");
        }

        component.set("v.menuConfigurationList", resultList);
    },

    checkSelectAllAndSetRequired : function(component, event, helper){
        let resultList = component.get("v.menuConfigurationList");
        let isSelectAll = true;
        let isRequiredAll = true;
        let isTitleRank = false;
        let isTitle =false;
       
        for (let i = 0 ; i < resultList.length ; i++){
            if(resultList[i].label == 'Title/Rank' && resultList[i].active){
                isTitleRank = true;
            }
            if(resultList[i].label == 'Title' && resultList[i].active){
                isTitle = true;
            }
            if(isTitleRank && isTitle && (resultList[i].label == 'Title' || resultList[i].label == 'Title/Rank')){
                resultList[i].active = false;
                helper.showToast("Error", "Please select either the Title field or the Title/Rank field.", "error");
            }
            
            if (!resultList[i].active){
                isSelectAll = false;
                resultList[i].required = false;
                isRequiredAll = false;
            }

            if(!resultList[i].required){
                isRequiredAll = false;
            }
        }
        component.set("v.selectAll", isSelectAll);
        component.set("v.requiredAll", isRequiredAll);
        component.set("v.menuConfigurationList", resultList);
    }, 

    checkRequiredAll : function(component, event, helper){
        let resultList = component.get("v.menuConfigurationList");
        let isRequiredAll = true;

        for (let i = 0 ; i < resultList.length ; i++){
            if(resultList[i].active){
                if(!resultList[i].required){
                    isRequiredAll = false;
                }
            }else if(resultList[i].required && !resultList[i].active){
                helper.showToast("Error", "Field is not active.", "error");
                resultList[i].required = false;
                isRequiredAll = false;
                break;
            }
            
        }
        component.set("v.menuConfigurationList", resultList);
        component.set("v.requiredAll", isRequiredAll);
    }
})