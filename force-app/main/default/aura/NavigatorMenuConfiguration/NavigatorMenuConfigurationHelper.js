({
	getAvailableFields : function(component, event, helper) {
		var action = component.get("c.getMenuConfigurationFields");
        action.setParams({ 
            IdParams : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var resultEvent = response.getReturnValue();
                console.log(resultEvent);
                component.set("v.menuConfigurationList", resultEvent);
                component.set("v.menuConfigurationListCopy", resultEvent);
                helper.checkSelectAllBoxes(component, resultEvent);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        // log the error passed in to AuraHandledException
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                this.showToast("Error", errors[0].message, "error");
            }
            
            component.set("v.isEditDisabled", false);
        });
        
        $A.enqueueAction(action);
	},

    checkSelectAllBoxes : function(component, results) {
        let isSelectAll = true;
        let isRequiredAll = true;

        for (let i = 0 ; i < results.length ; i++) {
            if (!results[i].active){
                isSelectAll = false;
            }

            if (!results[i].required){
                isRequiredAll = false;
            }
        }

        component.set("v.selectAll", isSelectAll);
        component.set("v.requiredAll", isRequiredAll);
    },
    
    saveRegistrationForm : function(component, event, helper) {
        component.set("v.isReadOnly", true);

        let updatedFields = component.get("v.menuConfigurationList");
        let action = component.get("c.saveMenuConfigurationFields");
        let fieldsForSaving = {};

        for (let i = 0 ; i < updatedFields.length ; i++) {
            fieldsForSaving[updatedFields[i]["key"]] = {'active': updatedFields[i]["active"], 
                                                        'fieldLabel': updatedFields[i]["fieldLabel"], 
                                                        'sortOrder': updatedFields[i]["sortOrder"].toString(),
                                                        'pageUrl': updatedFields[i]["pageUrl"],
                                                        'sldsIconName': updatedFields[i]["sldsIconName"],
                                                        'iconimageurl': updatedFields[i]["iconimageurl"]}; 
            console.log(fieldsForSaving);
            console.log(updatedFields[i]["key"]);
            
        }

        action.setParams({ 
            recordId : component.get("v.recordId"),
            updatedJSON : JSON.stringify(fieldsForSaving)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.showToast("Success", "Menu have been saved successfully", "success");
                component.set("v.menuConfigurationListCopy", component.get("v.menuConfigurationList"));
            }
        });
        
        $A.enqueueAction(action);
    },

    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type,
            "mode" : "pester"
        });
        toastEvent.fire();
    }
})