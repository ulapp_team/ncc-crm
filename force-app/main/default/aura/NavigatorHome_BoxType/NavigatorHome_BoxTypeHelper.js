({
	getRecordsToDisplay : function(component, event, helper) {
        const navWidget = component.get("v.navWidget");
		const action = component.get('c.getRecordDetails');
        
        /*console.log('@@@@@@@@@@@@@@@@');
        console.log(navWidget.Widget_Parent_Object__c);
        console.log(navWidget.Widget_Child_Object__c);
        console.log(navWidget.Conditions__c);
        console.log('@@@@@@@@@@@@@@@@');
        
        console.log(component.get("v.contact").contactId);
        console.log(JSON.stringify(navWidget));*/
        
        let jsonString = JSON.stringify(navWidget);
        
        //console.log('@@@@@@@@ END @@@@@@@@');
        
        action.setParams({ navigatorWidget : jsonString, contactId : component.get("v.contact").contactId});
        action.setCallback(this, function(response){
            
            let state = response.getState();
            
            if (state === "SUCCESS") {
                const recordsToDisplay = response.getReturnValue();
                component.set('v.recordsToDisplay', recordsToDisplay);
                console.log('recordsToDisplay : ', recordsToDisplay);
                
                var aMap = [];
                
                var parentRecTitleTemp;
                var valuesToPush = [];
                var shouldPushToArray = false;
                
                try {
                    console.log(recordsToDisplay);
                    for (let i = 0; i < recordsToDisplay.length; i++) {
                        console.log(recordsToDisplay[i]);
                        console.log(valuesToPush);
                        if (recordsToDisplay[i].parentRecTitle) {
                            if (recordsToDisplay[i].objType == 'Communication__c' ) {
                                aMap.push({key0: recordsToDisplay[i].parentRecTitle, 
                                           key1: recordsToDisplay[i].parentRecId,
                                           key2: '',  
                                           key3: recordsToDisplay[i].objType, 
                                           value: [recordsToDisplay[i].emailObj]});

                            //Added new condition XEREYES 01/22/2023
                            } else if (recordsToDisplay[i].objType == 'EmailMessage' ) {
                                aMap.push({key0: recordsToDisplay[i].parentRecTitle, 
                                           key1: recordsToDisplay[i].parentRecId,
                                           key2: '',  
                                           key3: recordsToDisplay[i].objType, 
                                           value: [recordsToDisplay[i].emailMessageObj]});
                            } else {
                                console.log(parentRecTitleTemp != recordsToDisplay[i].parentRecTitle);
                                if (parentRecTitleTemp != recordsToDisplay[i].parentRecTitle) {
                                    
                                    // START : CCN-NAV-3550-DV NavigatorHome Journey Updates
                                    let urlStr = '';
                                    if (recordsToDisplay[i].objType == 'Milestone__c') {
                                        //urlStr='/navigator/s/navigator-journey?journeyId=';
                                        urlStr = recordsToDisplay[i].recordUrl;
                                    } else {
                                        urlStr='/navigator/s/navigator-events?id=';
                                        urlStr+=recordsToDisplay[i].parentRecId;
                                        urlStr+='&id=';
                                        urlStr+=component.get('v.navigatorRecord').id;
                                        urlStr+='&contactId=';
                                        urlStr+=component.get("v.contact").contactId;
                                        urlStr+='&token=';
                                        urlStr+=component.get("v.token");
                                    }
                                    console.log('urlStr: ', urlStr)
                                    // END : CCN-NAV-3550-DV
                                    console.log(valuesToPush.length > 0);
                                    if (valuesToPush.length > 0) { 
                                        
                                        aMap.push({key0: recordsToDisplay[i].parentRecTitle, 
                                                   key1: recordsToDisplay[i].parentRecId, 
                                                   key2: urlStr,  
                                                   key3: recordsToDisplay[i].objType, 
                                                   value: valuesToPush});
                                    }
                                    
                                    valuesToPush = [];
                                    console.log('Emptying array');
                                    parentRecTitleTemp = recordsToDisplay[i].parentRecTitle;
                                    
                                    valuesToPush.push(recordsToDisplay[i]);
                                    shouldPushToArray = false;
                                    //Code below ensures the map is populated even if there's only 1 record
                                    if ((i == recordsToDisplay.length - 1 && recordsToDisplay.length == 1) 
                                        || (i == 0)) {
                                        aMap.push({key0: recordsToDisplay[i].parentRecTitle, 
                                                   key1: recordsToDisplay[i].parentRecId,
                                                   key2: urlStr,  
                                                   key3: recordsToDisplay[i].objType, 
                                                   value: valuesToPush});
                                        //valuesToPush = [];
                                        //console.log('Emptying array 2');
                                    }
                                } else {
                                    shouldPushToArray = true;
                                    valuesToPush.push(recordsToDisplay[i]);
                                }
                                console.log(valuesToPush.length > 0);
                                console.log(recordsToDisplay.length > i+1);
                                if (shouldPushToArray && !(recordsToDisplay.length > i+1)) {
                                    aMap.pop();
									// START : CCN-NAV-3550-DV NavigatorHome Journey Updates
                                    let urlStr = '';
                                    if (recordsToDisplay[i].objType == 'Milestone__c') {
                                        //urlStr='/navigator/s/navigator-journey?journeyId=';
                                        urlStr = recordsToDisplay[i].recordUrl;
                                    } else {
                                        urlStr='/navigator/s/navigator-events?id=';
                                        urlStr+=recordsToDisplay[i].parentRecId;
                                        urlStr+='&id=';
                                        urlStr+=component.get('v.navigatorRecord').id;
                                        urlStr+='&contactId=';
                                        urlStr+=component.get("v.contact").contactId;
                                        urlStr+='&token=';
                                        urlStr+=component.get("v.token");
                                    }
                                    
                                    console.log('urlStr_below: ', urlStr);
                                    // END : CCN-NAV-3550-DV
                                    console.log(valuesToPush.length > 0);
                                    if (valuesToPush.length > 0) { 
                                        
                                        aMap.push({key0: recordsToDisplay[i].parentRecTitle, 
                                                   key1: recordsToDisplay[i].parentRecId, 
                                                   key2: urlStr,  
                                                   key3: recordsToDisplay[i].objType, 
                                                   value: valuesToPush});
                                    }

                                    valuesToPush = [];
                                } 
                            }
                        } 
                    }
                }
                catch(err) {
                   console.error(err);
                }
                
                component.set('v.recordsToDisplayAsMap', aMap);
                
                console.log('@@@@@@@@ recordsToDisplayAsMap @@@@@@@@@');
                console.log(component.get('v.recordsToDisplayAsMap'));
                
            }
            else if (state === "ERROR") {
                console.error(response.getError());
            }
                        
        	
        });
        $A.enqueueAction(action);
	}
})