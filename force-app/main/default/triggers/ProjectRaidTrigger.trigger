trigger ProjectRaidTrigger on Project_Raid__c (before insert) {
    if(Trigger.isBefore && Trigger.isInsert){
        ProjectRaidTriggerHandler.onBeforeInsert(trigger.New);
    }
}