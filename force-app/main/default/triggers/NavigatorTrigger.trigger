trigger NavigatorTrigger on Navigator__c (after insert) {

    if(Trigger.isAfter && Trigger.isInsert){
        NavigatorTriggerHandler.updateLoginPageUrl(Trigger.newMap.keySet());
    }

}