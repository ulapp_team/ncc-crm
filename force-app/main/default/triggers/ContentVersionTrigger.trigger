trigger ContentVersionTrigger on ContentVersion(before insert, after insert){ 

    if(Trigger.isBefore){
        System.debug(trigger.new.size());
        System.debug(trigger.new);
        for(ContentVersion objCV : trigger.new){
            if(String.isNotBlank(objCV.Guest_Record_fileupload__c) && (objCV.Guest_Record_fileupload__c).contains('-----')){
                    objCV.Description = (objCV.Guest_Record_fileupload__c);
            }
        }
    }
    if(Trigger.isAfter){
        List<ContentDocumentLink> listCDLs = new List<ContentDocumentLink>();
        
        //Connect Widgets Mapping
        Map<Id, Id> widgetFilesMap = new Map<Id, Id>();
        List<Id> widgetIds = new List<Id>();
        List<Id> contentDocIds = new List<Id>();
        String base_rule = URL.getOrgDomainURL().toExternalForm();
        
        for(ContentVersion objCV : trigger.new){
            if(String.isNotBlank(objCV.Guest_Record_fileupload__c) && !(objCV.Guest_Record_fileupload__c).contains('-----')){
                ContentDocumentLink objCDL = new ContentDocumentLink();
                objCDL.LinkedEntityId = objCV.Guest_Record_fileupload__c;
                objCDL.ContentDocumentId = objCV.ContentDocumentId;
                
                if(String.valueOf(Id.valueOf((objCV.Guest_Record_fileupload__c)).getSObjectType()) == 'Telemeet__c'){
                    listCDLs.add(objCDL);
                }
            
                //Connect Widgets Attachments
                if(String.valueOf(Id.valueOf((objCV.Guest_Record_fileupload__c)).getSObjectType()) == 'Connect_Widget__c'){
                    widgetFilesMap.put(objCV.Guest_Record_fileupload__c, objCV.ContentDocumentId);
                    widgetIds.add(objCV.Guest_Record_fileupload__c);
                    contentDocIds.add(objCV.ContentDocumentId);
                }
                if(String.valueOf(Id.valueOf((objCV.Guest_Record_fileupload__c)).getSObjectType()) == 'Connect_Configurator__c'){
                    contentDocIds.add(objCV.ContentDocumentId);
                }
                
            }
            if(String.isNotBlank(objCV.Guest_Record_fileupload__c) && (objCV.Guest_Record_fileupload__c).contains('-----')){
                contentDocIds.add(objCV.ContentDocumentId);
            }
        }
        
        
        //Update Connect Widgets logo URL
        if(!widgetFilesMap.isEmpty() || !contentDocIds.isEmpty()){
            List<ContentVersion> cvList = [Select Id, ContentDocumentId, ContentBodyId, FileType, Title From ContentVersion Where ContentDocumentId IN: contentDocIds];
            Map<Id, String> cvMap = new Map<Id, String>();
            
            List<ContentDistribution> cdList = new List<ContentDistribution>();
            if(!contentDocIds.isEmpty()){
                for(ContentVersion cv: cvList){
                    ContentDistribution newDist = new ContentDistribution();
                    newDist.ContentVersionId = cv.Id;
                    newDist.Name = cv.Id;
                    newDist.PreferencesNotifyOnVisit = false;
                    newDist.PreferencesAllowViewInBrowser = true;
                    newDist.PreferencesAllowOriginalDownload=true;
                    cdList.add(newDist);
                }
                
                if(cdList.size() > 0){
                   insert cdList;
                }
            }
            
            
            if(!widgetFilesMap.isEmpty()){
                List<ContentDistribution> distribution = [Select Id, ContentDocumentId, ContentDownloadUrl, DistributionPublicUrl, PdfDownloadUrl from ContentDistribution where id IN :cdList];
                for(ContentDistribution cd : distribution){
                    cvMap.put(cd.ContentDocumentId, cd.ContentDownloadUrl);
                }
                
                List<Connect_Widget__c> widgetList = [Select Id From Connect_Widget__c Where Id IN: widgetIds];
                for(Connect_Widget__c cw : widgetList){
                    cw.Widget_Logo__c = cvMap.get(widgetFilesMap.get(cw.Id));
                }
                
                update widgetList;
            }
        }
        
        if(listCDLs.size() > 0){
            insert listCDLs;
        }
    }
}