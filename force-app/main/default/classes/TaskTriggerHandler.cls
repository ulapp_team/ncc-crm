/******************************************************************************
* @author       RLugpatan
* @date         04.25.2023
* @description  Sends email notification to the Journey Contact Us User when participant submits a Contact Us message
*
*            No.  Date(dd-mm-yyy) Author            Description
*           ----  ---------   --------------------  -----------------------------
* @version   1.0  25-04-2023      RLugpatan			Initial version - CCN-JOU-2862-DV
******************************************************************************/
public without sharing class TaskTriggerHandler extends TriggerHandler{
    
    protected override void afterInsert(Map<Id, SObject> newRecordsMap) {
        
        Id contactUsTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Community Journey - Contact Us').getRecordTypeId();
        
        List<Task> lstContactUsTasks = new List<Task>();
        Set<Id> setJourneyIds = new Set<Id>();
        Set<Id> setParticipantIds = new Set<Id>();
        
        for (Task recordTask : (List<Task>) newRecordsMap.values()){
            if (recordTask.RecordTypeId == contactUsTaskRecordTypeId){
                lstContactUsTasks.add(recordTask);
                setJourneyIds.add(recordTask.WhatId);
                setParticipantIds.add(recordTask.WhoId);
            }
        }
        
        if(lstContactUsTasks.size() > 0){
            List<Journey__c> lstJourneys = new List<Journey__c>();
            Map<Id, Journey__c> mapRelatedJourneys = new Map<Id, Journey__c>([SELECT Id, Name, Contact_Us_User__c, Contact_Us_User__r.Name, Contact_Us_User__r.FirstName, Contact_Us_User__r.LastName FROM Journey__c WHERE Id IN: setJourneyIds]);
            Map<Id, Contact> mapRelatedContacts = new Map <Id, Contact>([SELECT Id, Name, Email, Phone FROM Contact WHERE Id IN: setParticipantIds]);
            
            sendEmail(
                createEmailFromTemplate(lstContactUsTasks, mapRelatedJourneys, mapRelatedContacts)
            );
        }
        
    }
    
    
    private List<Messaging.SingleEmailMessage> createEmailFromTemplate(List<Task> lstContactUsTasks,  Map<Id, Journey__c> mapRelatedJourneys, Map<Id, Contact> mapRelatedContacts){
        
        Compass_Setting__c defaultSetting = [SELECT ID, Email_Sender_Id__c FROM Compass_Setting__c WHERE Default__c = TRUE LIMIT 1];
        
        OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress Where Id =: defaultSetting.Email_Sender_Id__c];
        
        EmailTemplate contactUsEmailTemplate = [SELECT Id, Name, Body FROM EmailTemplate Where Name = 'Contact Us Task Email Template' LIMIT 1];
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        String sendToField = '';
        String taskJourneyField = '';
        String taskFromNameField = '';
        String taskSubjectField = '';
        String taskDescriptionField = '';
        String taskContactUsNameField = '';
        String taskContactUsEmailField = '';
        String taskContactUsPhoneField = '';
        
        for (Task recordTask :  lstContactUsTasks){
            sendToField = mapRelatedJourneys.get(recordTask.WhatId).Contact_Us_User__c != null ? mapRelatedJourneys.get(recordTask.WhatId).Contact_Us_User__c : '';
            taskJourneyField = mapRelatedJourneys.get(recordTask.WhatId).Name != null ? mapRelatedJourneys.get(recordTask.WhatId).Name : '';
            taskFromNameField = mapRelatedContacts.get(recordTask.WhoId).Name != null ? mapRelatedContacts.get(recordTask.WhoId).Name : '';
            taskSubjectField = recordTask.Subject != null ? recordTask.Subject : '';
            taskDescriptionField = recordTask.Description != null ? recordTask.Description : '';
            taskContactUsNameField = mapRelatedJourneys.get(recordTask.WhatId).Contact_Us_User__r.Name != null ? mapRelatedJourneys.get(recordTask.WhatId).Contact_Us_User__r.FirstName + ' ' + mapRelatedJourneys.get(recordTask.WhatId).Contact_Us_User__r.LastName : '';
            taskContactUsEmailField = mapRelatedContacts.get(recordTask.WhoId).Email != null ? mapRelatedContacts.get(recordTask.WhoId).Email : '';
            taskContactUsPhoneField = recordTask.Phone_Number__c != null ? recordTask.Phone_Number__c : mapRelatedContacts.get(recordTask.WhoId).Phone != null ? mapRelatedContacts.get(recordTask.WhoId).Phone : '';
            
            String emailTemplateBody = contactUsEmailTemplate.Body;
            emailTemplateBody = emailTemplateBody.replace('{!ContactUsUser}', taskContactUsNameField);
            emailTemplateBody = emailTemplateBody.replace('{!TaskJourney}', taskJourneyField);
            emailTemplateBody = emailTemplateBody.replace('{!TaskFromName}', taskFromNameField);
            emailTemplateBody = emailTemplateBody.replace('{!TaskSubject}', taskSubjectField);
            emailTemplateBody = emailTemplateBody.replace('{!TaskDescription}', taskDescriptionField);
            emailTemplateBody = emailTemplateBody.replace('{!ContactUsEmail}', taskContactUsEmailField);
            emailTemplateBody = emailTemplateBody.replace('{!ContactUsPhoneNumber}', taskContactUsPhoneField);
            
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(
                contactUsEmailTemplate.Id, 
                sendToField,
                recordTask.Id
            );
            
            mail.setPlainTextBody(emailTemplateBody);
            mail.setReplyTo(owa.Address);
            mail.setTargetObjectId(sendToField);
            mail.setSaveAsActivity(false); 
            mail.setOrgWideEmailAddressId(owa.Id);
            emails.add(mail);
        }
        
        return emails;
    }
    
    public void sendEmail(List<Messaging.SingleEmailMessage> emails){
        Messaging.sendEmail(emails);
        system.debug('It went here sending email>>>');
    }
}