/*******************************************************************************
 * @author       Jayson Labnao
 * @date         14.08.2021
 * @description  Navigator Home Page Components Controller test class
 * @group          Test Class
 * @revision     14.08.2021 - jlabnao - Created
 *******************************************************************************/

 @isTest
 private class NavigatorHomeComponentsController_Test {
     @testSetup
     static void setup(){
         
         Account acc = TestUtility.createAccRec('TestAccount');
         insert acc;
         
         Contact testContact = NavigatorTestDataFactory.createContact('testing', 'testing@test.com', 'ABCD1234');
         testContact.Contact_Id__c = '9LIshQ4sJWP';
         insert testContact;
         
         OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
         EmailTemplate eTemp = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
         system.runAs( new User(Id = UserInfo.getUserId(),
                                TimezonesIdKey = 'America/New_York')){
             insert eTemp;
         
         Compass_Setting__c setting = new Compass_Setting__c();
         setting.Name = 'Default Settings';
         setting.Email_Sender_Name__c = 'NAME';
         setting.Email_Template_Invitation_Id__c = eTemp.Id;
         setting.Email_Template_Registration_Id__c = eTemp.Id;
         setting.Email_Sender_Id__c = owea.Id;
         insert setting;
         
         Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
         testCampaign.Account__c = acc.Id;
         insert testCampaign;
         
         Journey__c testJourney = NavigatorTestDataFactory.createJourney('testJourney', testCampaign.Id, 'Approved');
         testJourney.End_Date__c = Date.valueOf(System.DateTime.now().addDays(5));
         insert testJourney;
 
         Event__c testEvent = NavigatorTestDataFactory.createEvent('testEvent', testCampaign.Id, '1234ABCD', Date.ValueOf(System.now()));
         testEvent.Time_Zone__c = 'ET';
         insert testEvent;
 
         Session__c testSession = NavigatorTestDataFactory.createSession('testSession', testEvent.Id, Date.ValueOf(System.now()));
         testSession.IsActive__c = TRUE;
         testSession.Start_Date_Time__c = System.now();
         testSession.End_Date_Time__c = System.now().addHours(1);
         testSession.Time_Zone__c = 'ET';
         insert testSession;
         
         Session_Participant__c sessionParticipant = new Session_Participant__c();
         sessionParticipant.Event__c = testEvent.Id;
         sessionParticipant.Session__c = testSession.Id;
         sessionParticipant.Contact__c = testContact.Id;
         sessionParticipant.Status__c = 'Registered';
         insert sessionParticipant;
         
         Milestone__c milestone = new Milestone__c();
         milestone.Journey__c = testJourney.Id;
         milestone.Name = 'testMilestone';
         milestone.Type__c = 'Event';
         milestone.Related_RecordId__c = testSession.Id;
         insert milestone;
         
         Journey_Participant__c testParticipant = NavigatorTestDataFactory.createJourneyParticipant(testJourney.Id, testContact.Id);
         Insert testParticipant;
         
         Participant_Milestone__c participantMilestone = new Participant_Milestone__c();
         participantMilestone.Milestone__c = milestone.Id; 
         participantMilestone.Contact__c = testContact.Id;
         participantMilestone.Is_Active__c = TRUE;
         participantMilestone.Start_Date__c = System.Date.today().addDays(-2);
         participantMilestone.End_Date__c = System.Date.today().addDays(2);
         participantMilestone.Journey__c = testParticipant.Id;
         participantMilestone.Checkpoint__c = true;
         insert participantMilestone;
         
         Navigator__c testNav = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
         testNav.Change_Agent_Lead_Label__c = 'Test';
         testNav.About_Label__c = 'TestAbout';
         testNav.About_Image_Video_Link__c = 'salesforce.com';
         
         Insert testNav;
 
         Navigator_Item__c testNavItem = NavigatorTestDataFactory.createNavigatorItem('testNavigatorItem', 'Test Tab', testNav.Id , 1);
         Insert testNavItem;
 
         Navigator_Section__c testNavSection = NavigatorTestDataFactory.createNavigatorSection('testSection', testNavItem.Id, 1);
         Insert testNavSection;
 
         Navigator_Section_Attribute__c testNavSectionAttr = NavigatorTestDataFactory.createNavigatorSectionAttribute('testSectionAttribute', testNavSection.Id, 'Text', 1, 'Test Content');
         Insert testNavSectionAttr;
                                    
         }
     }
 
     @isTest static void getParticipantMilestones_Test(){
         Contact testContact = [SELECT Id, Contact_Id__c FROM Contact LIMIT 1];
        
         User u = [SELECT Id from User where Id = : UserInfo.getUserId()];
         
         System.runAs(u){ 
             
             test.startTest();
             List<Participant_Milestone__c> pMilestoneList = NavigatorHomeComponentsController.getParticipantMilestones(testContact.Contact_Id__c);
             test.stopTest();
             
             System.assert(pMilestoneList.size() > 0);
         }
        
     }
 
     @isTest static void getEventsAndSessions_Test(){
         Contact testContact = [SELECT Id, Contact_Id__c FROM Contact LIMIT 1];
         Session_Participant__c sParticipant = [SELECT Id FROM Session_Participant__c LIMIT 1];
         Navigator__c testNav = [SELECT Id from Navigator__c LIMIT 1];
         
         System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
         System.debug(testContact);
         
         Date dateToday = System.today();
         System.debug(dateToday.month());
         System.debug(dateToday.year());
 
         User u = [SELECT Id,TimezonesIdKey from User where Id = : UserInfo.getUserId()];
         
         
         System.debug(u.TimezonesIdKey);
         
         System.runAs(u){ 
             test.startTest();
             NavigatorHomeComponentsController.getContactCommunications(testContact.Contact_Id__c);
             List<NavigatorHomeComponentsController.EventSessionWrapper> evtSessionWrapList = 
                 NavigatorHomeComponentsController.getEventsAndSessions(testContact.Contact_Id__c, dateToday.month(), dateToday.year(), testNav.Id);
             test.stopTest();
             
             /* System.assert(evtSessionWrapList.size() > 0);
             System.assert(evtSessionWrapList[0].event != null);
             System.assert(evtSessionWrapList[0].sessions.size() > 0); */
         }
     }
     
     @isTest static void getNavigatorDetails_Test(){
         Navigator__c testNav = [SELECT Id FROM Navigator__c LIMIT 1];
         
         test.startTest();
         Navigator__c navRec = NavigatorHomeComponentsController.getNavigatorRecord(testNav.Id);
         NavigatorUtility.NavigatorWrapper nav = NavigatorHomeComponentsController.getNavigatorDetails(testNav.Id);
         
         test.stopTest();
 
         System.assert(nav != null, 'Method should return a navigator record.');
         System.assert(navRec != null, 'Method should return a navigator record.');
     }
     
     @isTest static void getSectionAttributes_Test(){
         Navigator__c testNav = [SELECT Id FROM Navigator__c LIMIT 1];
         
         test.startTest();
         List<NavigatorUtility.NavigatorSectionAttributeWrapper> sectionAttributes = NavigatorHomeComponentsController.getSectionAttributes(testNav.Id, 'testSection');
         test.stopTest();
 
         System.assert(sectionAttributes.size() > 0, 'Method should return a Navigator Section Attribute record.');
     }
 
 
 }