/*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Service Class for navigator
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
  public class NavigatorHomeComponentService {
    static final String FIELDSETNAME            = 'NavigatorHomeFields';
    static final String EVENT_WHERECONDITION    = 'Session__r.IsActive__c = TRUE AND Session__r.End_Date_Time__c >= TODAY AND CALENDAR_MONTH(Session__r.Start_Date_Time__c) =: month AND CALENDAR_YEAR(Session__r.Start_Date_Time__c) =: year';
    static final String EVENT_ORDERBY           = 'ORDER BY Session__r.Start_Date_Time__c ASC';
    
    static Integer month = System.Date.today().month();
    static Integer year = System.Date.today().year();

    /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets all related Session_Participant__c records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    public static List<Session_Participant__c> getParticipantSessions(Navigator_Widget__c navWidget, String contactId/*, Integer month, Integer year*/){
       NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();
        
        return navService.getParticipantSessions(navWidget,contactId);
    }
    
    /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets all related Participant_Milestone__c records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    public static List<Participant_Milestone__c> getParticipantMilestones(Navigator_Widget__c navWidget, String contactId){
        NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();
        
        return navService.getParticipantMilestones(navWidget,contactId);
    }
    
    /*******************************************************************************
   * @author       Alyana Navarro
   * @date         10/02/2023
   * @description  Gets all related Communication__c records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    public static List<Communication__c> getEmails(Navigator_Widget__c navWidget, String contactId){
        
        NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();
        
        return navService.getEmails(navWidget,contactId);
    }

    /*******************************************************************************
    * @author       Abram Vixen Reyes
    * @ticket       CCN-NAV-3521-DV
    * @date         01/21/2023
    * @description  Gets all EmailMessage record from contact ActivityHistory related to the campaign
    * @revision     
    *******************************************************************************/
    public static List<EmailMessage> getEmailsActivityHistory(Navigator_Widget__c navWidget, String contactId){
            
        NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();
        
        return navService.getEmailsActivityHistory(navWidget,contactId);
    }

    /*******************************************************************************
     * @author       Leif Erickson de Gracia
     * @ticket       CCN-NAV-3521-DV
     * @date         10/02/2023
     * @description  Gets all related Participant__c records
     * @revision     11/22/2023 - Leif Erickson de Gracia - Finalized
     * 
     *******************************************************************************/
    public static List<Participant__c> getParticipantRecords(String contactId, Id campaignId){
        NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();

        return navService.getParticipantRecords(contactId,campaignId);
    }

    /*******************************************************************************
     * @author       Leif Erickson de Gracia
     * @ticket       CCN-NAV-3521-DV
     * @date         11/28/2023
     * @description  Gets the related Participant__c records via session participant
     * @revision     11/28/2023 - Leif Erickson de Gracia - Finalized
     *******************************************************************************/
    public static List<Participant__c> getParticipantRecordsViaSession(String contactId, Integer month, Integer year, Id campaignId){
        NavigatorServiceWithoutSharing navService = new NavigatorServiceWithoutSharing();

        return navService.getParticipantRecordsViaSession(contactId, month, year,campaignId);
    }
    
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @date         10/02/2023
   * @description  Bypasses sharing for the guest site profile. 
   *               this should have no problems since we are using the contact_Id__c as filter
   * @revision     11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    public without sharing class NavigatorServiceWithoutSharing {
        
        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Participant_Milestone__c records
         * @revision     11/27/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        /* public List<Participant_Milestone__c> getEventParticipantMilestones(String contactId){
            return [SELECT 
                        Id, 
                        Contact__c, 
                        Contact__r.Contact_Id__c,
                        Milestone__r.Related_RecordId__c, 
                        Type__c, 
                        Milestone_Status__c
                    FROM Participant_Milestone__c
                    WHERE Contact__r.Contact_Id__c = :contactId
                    AND Type__c = 'Event'
                    AND Milestone__r.Related_RecordId__c != NULL];
        } */
        
        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Session__c records
         * @revision     11/05/2023 - Leif Erickson de Gracia - Finalized
         * 				 11/07/2023 - Leif Erickson de Gracia - Deprecated
         *******************************************************************************/
        /*public List<Session__c> getSessions(String contactId, Integer monthParam, Integer yearParam){
            return [SELECT  
                        Id, 
                        Name, 
                        Event__c,
                        Start_Date_Time__c, 
                        End_Date_Time__c, 
                        Session_Details__c
                    FROM Session__c
                    WHERE Id IN (SELECT Session__c 
                                 FROM Session_Participant__c 
                                 WHERE Contact__r.Contact_Id__c = :contactId 
                                 OR Participant__r.Member_Contact__r.Contact_Id__c =:contactId)
                    AND End_Date_Time__c >= TODAY
                    AND CALENDAR_MONTH(Start_Date_Time__c) = :monthParam
                    AND CALENDAR_YEAR(Start_Date_Time__c) = :yearParam
                    AND IsActive__c = TRUE
                    ORDER BY Start_Date_Time__c ASC];
        }*/
        
        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Participant_Milestone__c records
         * @revision     11/27/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        public List<Participant_Milestone__c> getParticipantMilestones(Navigator_Widget__c navWidget, String contactId){
            
            List<Participant_Milestone__c> pMilestonesList = new List<Participant_Milestone__c>();
            
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            
            // START : CCN-NAV-3679-DV Added new logic for missed milestones
            if (navWidget.Conditions__c == 'Missed') {
                List<Participant_Milestone__c>  pMilestonesListTemp = [SELECT 
                                                                           Id, 
                                                                           Milestone__c, 
                                                                           Milestone__r.Name, 
                                                                           Milestone__r.Type__c,
                                                                           Milestone__r.Journey__c, 
                                                                           Milestone__r.Journey__r.Name,
                                                                           Milestone__r.Related_RecordId__c, 
                                                                           Milestone_Progress__c, 
                                                                           Milestone_Status__c, 
                                                                           Milestone__r.Session_Catch_Up_Start_Date__c,
                                                                           Milestone__r.Session_Catch_Up_End_Date__c,
                                                                           Journey__r.Id,
                                                                           Journey__r.Journey_URL__c
                                                                       FROM Participant_Milestone__c 
                                                                       WHERE Contact__r.Contact_Id__c = :contactId
                                                                       AND Milestone_Status__c =: navWidget.Conditions__c
                                                                       AND Milestone__r.Session_Catch_Up_Start_Date__c != null
                                                                       AND Milestone__r.Session_Catch_Up_Start_Date__c <= :local
                                                                       AND Milestone__r.Session_Catch_Up_End_Date__c >= :local
                                                                       AND Milestone__r.Journey__r.Campaign__c = : navWidget.Navigator__r.Campaign__c
                                                                       AND Is_Active__c = TRUE];
                
                if (pMilestonesListTemp.isEmpty() == false) {
                    for (Participant_Milestone__c rec: pMilestonesListTemp) {
                        if (rec.Milestone__r.Session_Catch_Up_Start_Date__c <= rec.Milestone__r.Session_Catch_Up_End_Date__c) {
                            pMilestonesList.add(rec);
                        }
                    }
                }
                // End : CCN-NAV-3679-DV
            } else {
                // START : CCN-NAV-3550-DV NavigatorHome Journey Updates - Added Journey Participant fields  in query
                pMilestonesList = [SELECT 
                                       Id, 
                                       Milestone__c, 
                                       Milestone__r.Name, 
                                       Milestone__r.Type__c,
                                       Milestone__r.Journey__c, 
                                       Milestone__r.Journey__r.Name,
                                       Milestone__r.Related_RecordId__c, 
                                       Milestone_Progress__c, 
                                       Milestone_Status__c, 
                                       Journey__r.Id,
                                       Journey__r.Journey_URL__c
                                   FROM Participant_Milestone__c 
                                   WHERE Contact__r.Contact_Id__c = :contactId
                                   AND Milestone_Status__c =: navWidget.Conditions__c
                                   AND Milestone__r.Journey__r.Campaign__c = : navWidget.Navigator__r.Campaign__c
                                   //AND Checkpoint__c != TRUE 
                                   AND Is_Active__c = TRUE];
                
                // END : CCN-NAV-3550-DV
            }
            
            
            return pMilestonesList;
        }
        
        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Communication__c records
         * @revision     11/27/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        public List<Communication__c> getEmails(Navigator_Widget__c navWidget, String contactId){
            List<Communication__c> commsList = [
                SELECT 
                    Id, 
                    Subject__c, 
                    Sender_Email_Address__c, 
                    Email_Date_and_Time__c, 
                    Body__c, 
                    Journey__c
                FROM Communication__c
                WHERE Id IN (
                    SELECT CC_Communication__c 
                    FROM Communication_Recipient__c 
                    WHERE Contact__r.Contact_Id__c = :contactId
                )
                AND Campaign__c = : navWidget.Navigator__r.Campaign__c
                ORDER BY Email_Date_and_Time__c DESC
            ];
            
            return commsList;
        }

        /*******************************************************************************
        * @author       Abram Vixen Reyes
        * @ticket       CCN-NAV-3521-DV
        * @date         01/21/2023
        * @description  Gets the related EmailMessage records from the contacts ActivityHistories
        * @revision     
        *******************************************************************************/
        public List<EmailMessage> getEmailsActivityHistory(Navigator_Widget__c navWidget, String contactId){

            Set<Id> emailMessageToGet = new Set<Id>();
            Map<Id, List<Id>> commsIdEmailMessageId = new Map<Id, List<Id>>();
            Map<Id, List<Id>> participantIdEmailMessageId = new Map<Id, List<Id>>();
            Map<Id, List<Id>> parkingLotIdEmailMessageId = new Map<Id, List<Id>>();
            Map<Id, List<Id>> issueIdEmailMessageId = new Map<Id, List<Id>>();
            List<EmailMessage> emailMessageList = new List<EmailMessage>();
            List<Contact> ccList = [
                SELECT Id, Name,
                    (
                        SELECT Subject, ActivitySubtype, WhatId, AlternateDetailId 
                        FROM ActivityHistories 
                        WHERE ActivitySubtype = 'Email' OR ActivitySubtype = 'ListEmail'
                    )
                FROM Contact
                WHERE Contact_Id__c =: contactId
            ];
            
            if(!ccList.isEmpty()){
                for(ActivityHistory ah : ccList[0].ActivityHistories){
                    if(ah.WhatId != null && ah.AlternateDetailId != null){
                        if(ah.WhatId.getSObjectType().getDescribe().getName() == 'Communication__c'){
                            if(commsIdEmailMessageId.containsKey(ah.WhatId)){
                                commsIdEmailMessageId.get(ah.WhatId).add(ah.AlternateDetailId);
                            } else {
                                commsIdEmailMessageId.put(ah.WhatId, new List<Id>{ah.AlternateDetailId});
                            }
                        } else if(ah.WhatId.getSObjectType().getDescribe().getName() == 'Participant__c'){
                            if(participantIdEmailMessageId.containsKey(ah.WhatId)){
                                participantIdEmailMessageId.get(ah.WhatId).add(ah.AlternateDetailId);
                            } else {
                                participantIdEmailMessageId.put(ah.WhatId, new List<Id>{ah.AlternateDetailId});
                            }
                        } else if(ah.WhatId.getSObjectType().getDescribe().getName() == 'Parking_Lot__c'){
                            if(parkingLotIdEmailMessageId.containsKey(ah.WhatId)){
                                parkingLotIdEmailMessageId.get(ah.WhatId).add(ah.AlternateDetailId);
                            } else {
                                parkingLotIdEmailMessageId.put(ah.WhatId, new List<Id>{ah.AlternateDetailId});
                            }
                        } else if(ah.WhatId.getSObjectType().getDescribe().getName() == 'Issue__c'){
                            if(issueIdEmailMessageId.containsKey(ah.WhatId)){
                                issueIdEmailMessageId.get(ah.WhatId).add(ah.AlternateDetailId);
                            } else {
                                issueIdEmailMessageId.put(ah.WhatId, new List<Id>{ah.AlternateDetailId});
                            }
                        }
                        
                    }
                }

                if(!commsIdEmailMessageId.isEmpty()){
                    for(Communication__c cc : [SELECT Id, Campaign__c FROM Communication__c WHERE Campaign__c =: navWidget.Navigator__r.Campaign__c AND Id IN: commsIdEmailMessageId.keySet()]){
                        emailMessageToGet.addAll(commsIdEmailMessageId.get(cc.Id));
                    }
                }
    
                if(!participantIdEmailMessageId.isEmpty()){
                    for(Participant__c cc : [SELECT Id, Event__r.Campaign__c FROM Participant__c WHERE Event__r.Campaign__c =: navWidget.Navigator__r.Campaign__c AND Id IN: participantIdEmailMessageId.keySet()]){
                        emailMessageToGet.addAll(participantIdEmailMessageId.get(cc.Id));
                    }
                }

                if(!parkingLotIdEmailMessageId.isEmpty()){
                    for(Parking_Lot__c cc : [SELECT Id, Event__r.Campaign__c FROM Parking_Lot__c WHERE Event__r.Campaign__c =: navWidget.Navigator__r.Campaign__c AND Id IN: parkingLotIdEmailMessageId.keySet()]){
                        emailMessageToGet.addAll(parkingLotIdEmailMessageId.get(cc.Id));
                    }
                }

                if(!issueIdEmailMessageId.isEmpty()){
                    for(Issue__c cc : [SELECT Id, Event__r.Campaign__c FROM Issue__c WHERE Event__r.Campaign__c =: navWidget.Navigator__r.Campaign__c AND Id IN: issueIdEmailMessageId.keySet()]){
                        emailMessageToGet.addAll(issueIdEmailMessageId.get(cc.Id));
                    }
                }
    
                if(!emailMessageToGet.isEmpty()){
                    emailMessageList = [
                        SELECT Id, HtmlBody, TextBody, Subject
                        FROM EmailMessage
                        WHERE Id IN: emailMessageToGet
                        ORDER BY CreatedDate DESC
                    ];
                }
            }

            return emailMessageList;
        }
        
        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Session_Participant__c records
         * @revision     11/27/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        public List<Session_Participant__c> getParticipantSessions(Navigator_Widget__c navWidget, String contactId/*, Integer month, Integer year*/){
            return [SELECT Id, 
                        Name, 
                        Event__c, 
                        Event__r.Id, 
                        Event__r.Name, 
                        Event__r.Event_Id__c,
                        Session__c, 
                        Session__r.Id, 
                        Session__r.Name, 
                        Session__r.Start_Date_Time__c, 
                        Session__r.End_Date_Time__c, 
                        Session__r.Session_Details__c
                    FROM Session_Participant__c 
                    WHERE Contact__r.Contact_Id__c =: contactId 
                    AND Status__c in ('Registered','Confirmed') // added by Von CCN-NAV-3521-DV
                    AND Session__r.IsActive__c = TRUE 
                    AND Session__r.End_Date_Time__c >= TODAY 
                    AND CALENDAR_MONTH(Session__r.Start_Date_Time__c) =: month 
                    AND CALENDAR_YEAR(Session__r.Start_Date_Time__c) =: year
                    AND (Session__r.Campaign__c = : navWidget.Navigator__r.Campaign__c
                        OR Session__r.Event__r.Campaign__c = : navWidget.Navigator__r.Campaign__c)
                    ORDER BY Session__r.Start_Date_Time__c ASC];
        }

        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         10/27/2023
         * @description  Gets the related Participant__c records
         * @revision     11/22/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        public List<Participant__c> getParticipantRecords(String contactId, Id campaignId){
            List<Participant__c> participantList = [SELECT 
                                                        Id, 
                                                        Event_Name__c,
                                                        Event__c, 
                                                        Status__c
                                                    FROM Participant__c 
                                                    WHERE Member_Contact__r.Contact_Id__c = :contactId
                                                    AND Status__c IN ('Invited','')
                                                    AND Event__r.Campaign__c = : campaignId];
            
            return participantList;
        }

        /*******************************************************************************
         * @author       Leif Erickson de Gracia
         * @ticket       CCN-NAV-3521-DV
         * @date         11/28/2023
         * @description  Gets the related Participant__c records via session participant
         * @revision     11/28/2023 - Leif Erickson de Gracia - Finalized
         *******************************************************************************/
        public List<Participant__c> getParticipantRecordsViaSession(String contactId, Integer month, Integer year, Id campaignId){
            List<Participant__c> participantList = [SELECT 
                                                        Id, 
                                                        Event_Name__c,
                                                        Event__c, 
                                                        Status__c
                                                    FROM Participant__c 
                                                    WHERE Member_Contact__r.Contact_Id__c = :contactId
                                                    AND Id IN (
                                                            SELECT Participant__c 
                                                            FROM Session_Participant__c 
                                                            WHERE Status__c in ('Registered','Confirmed')
                                                            AND Participant__r.Event__r.End_Date_Time__c >= TODAY
                                                            AND CALENDAR_MONTH(Session_Start_Date__c) = :month
                                                            AND CALENDAR_YEAR(Session_Start_Date__c) = :year
                                                            AND Participant__r.Member_Contact__r.Contact_Id__c = :contactId
                                                            AND Participant__r.Event__r.Campaign__c = : campaignId
                                                            ) 
                                                        ];
            
            return participantList;
        }
    }
}