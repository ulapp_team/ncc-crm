@isTest (SeeAllData=true) 
public class TaskTriggerHandlerTest {
    
    @IsTest
    static void testSendEmailAfterContactUsTask(){
        Test.startTest();
        
        User ceoUser = [SELECT Id, UserRole.DeveloperName FROM User WHERE UserRole.DeveloperName='CEO' AND isActive = True LIMIT 1];
        User contactUs = [Select Id, Name From User Where LastName LIKE '%De Guzman%' And IsActive = TRUE LIMIT 1];
        
        System.runAs ( ceoUser ) {
            Profile compassAgilePMUserId = [SELECT Id FROM Profile WHERE Name = 'Compass Agile PM User'];
            
            Account testAccount = new Account(name ='Ulapp');
            insert testAccount; 
            
            Contact testContact = new Contact(LastName ='testCon',AccountId = testAccount.Id, Email='test1@com.com');
            insert testContact;  
            
            Campaign__c testCampaign = new Campaign__c();
            testCampaign.Name = 'Test Campaign Name';
            insert testCampaign;
            
            Journey__c testJourney = new Journey__c();
            testJourney.Name = 'Test Journey';
            testJourney.Campaign__c = testCampaign.Id;
            testJourney.Status__c = 'For Review';
            testJourney.End_Date__c = System.today() + 3;
            insert testJourney;
        }

        Id contactUsTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Community Journey - Contact Us').getRecordTypeId();
        
		Contact testContact = [SELECT Id, Name, Email, Phone FROM Contact Where LastName ='testCon' LIMIT 1];
        Journey__c testJournery = [SELECT Id, Name, Contact_Us_User__c, Contact_Us_User__r.Name, Contact_Us_User__r.FirstName, Contact_Us_User__r.LastName FROM Journey__c  Where Contact_Us_User__c != null limit 1];
        Task testTaskData = new Task();
        testTaskData.RecordTypeId = contactUsTaskRecordTypeId;
        testTaskData.OwnerId = UserInfo.getUserId();
        testTaskData.WhoId = testContact.Id;
        testTaskData.WhatId = testJournery.Id;
        insert testTaskData;

        Test.stopTest();
    }
    
}