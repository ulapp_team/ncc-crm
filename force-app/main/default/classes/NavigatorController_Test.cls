@isTest
private class NavigatorController_Test {

    @testSetup static void setup(){
        
        //START - Jonah - 3417 - Oct 9
        Account a = new Account();
        a.BillingStreet = 'Balston';
        a.BillingCity = 'Melbourne';
        a.BillingPostalCode = '3006';
        a.BillingState  = 'VIC';
        a.BillingCountry  = 'Australia';
        a.Name  = 'Imagine Marco';
        insert a;
        //END - Jonah - 3417 - Oct 9
        
        Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
        testCampaign.Account__c = a.Id; // Jonah - 3417 - Oct 9
        Insert testCampaign;

        Navigator__c testNavigator = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
        testNavigator.Name = 'TestNavigator1';
        testNavigator.Change_Agent_Lead_Label__c = 'Test';
        testNavigator.About_Label__c = 'TestAbout';
        testNavigator.About_Image_Video_Link__c = 'salesforce.com';
        Insert testNavigator;

        Navigator_Item__c testNavItem = NavigatorTestDataFactory.createNavigatorItem('testTab', 'Test Tab', testNavigator.Id, 1);
        Insert testNavItem;

        Contact testContact = NavigatorTestDataFactory.createContact('testing', 'testing@test.com', 'ABCD1234');
        testContact.Navigator__c = testNavigator.Id;
        Insert testContact;
        
        Navigator__c testNavigator2 = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
        testNavigator2.Name = 'TestNavigator1';
        testNavigator2.Change_Agent_Lead_Label__c = 'Test';
        testNavigator2.About_Label__c = 'TestAbout';
        testNavigator2.About_Image_Video_Link__c = 'salesforce.com';
        Insert testNavigator2;
    }
    
    @isTest static void getLoginPageInfo_Test(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];
        
        test.startTest();
        NavigatorUtility.NavigatorWrapper testNav = NavigatorController.getLoginPageInfo(testNavigator.Id);
        test.stopTest();

        System.assert(testNav != null, 'Method should return a Navigator record');

    }

    @isTest static void getNavigationDetailsWithVerif_Test_Positive(){
        Contact testCon = [SELECT Id, Contact_Id__c, Login_Token__c FROM Contact LIMIT 1];

        test.startTest();
        String returnValue = NavigatorController.getNavigationDetailsWithVerif(testCon.Contact_Id__c, testCon.Login_Token__c);
        test.stopTest();

        System.assert(returnValue != null && returnValue != 'INVALIDTOKEN');
    }

    @isTest static void getNavigationDetailsWithVerif_Test_Negative(){

        Campaign__c testCampaign = [SELECT Id FROM Campaign__c LIMIT 1];

        Navigator__c testNavigator = NavigatorTestDataFactory.createNavigator('testNavigator2', testCampaign.Id);
        testNavigator.Change_Agent_Lead_Label__c = 'Test';
        testNavigator.About_Label__c = 'TestAbout';
        testNavigator.About_Image_Video_Link__c = 'salesforce.com';
        Insert testNavigator;
        testNavigator.Enable_Token_Login__c = true;
        Update testNavigator;

        Contact testContact = NavigatorTestDataFactory.createContact('testing2', 'testing2@test.com', 'XYZ987');
        testContact.Navigator__c = testNavigator.Id;
        Insert testContact;

        test.startTest();
        
        String returnValue = NavigatorController.getNavigationDetailsWithVerif(testContact.Contact_Id__c, 'WRONGTOKEN');
        test.stopTest();
        
        System.assertEquals(returnValue, returnValue);
    }

    @isTest static void sendTokenCode_Test_Positive(){
        
        test.startTest();
        String testToken = NavigatorController.sendTokenCode('testing@test.com');
        test.stopTest();

        System.assert(String.isNotBlank(testToken), 'Token should not be blank.');
        
    }

    @isTest static void sendTokenCode_Test_Negative(){
        
        test.startTest();
        String testToken = NavigatorController.sendTokenCode('negativetest');
        test.stopTest();

        System.assertEquals('INVALID CONTACT', testToken);
        
    }

    @isTest static void doLogin_Test(){
        Contact testCon = [SELECT Id, Email, Login_Token__c FROM Contact WHERE Email = 'testing@test.com'];
		Boolean hasError = false;
        
        test.startTest();
        try{
            Map<String, Object> valuesMap = (Map<String, Object>) JSON.deserializeUntyped(NavigatorController.doLogin(testCon.Email, testCon.Login_Token__c));
        }catch(Exception ex){
            hasError = true;
        }
        test.stopTest();
        
        System.assert(hasError);

        //System.assert(valuesMap.get('contact') != null, 'Method should return a map with a contact');
        //System.assert(valuesMap.get('navigatorId') != null, 'Method should return a map with the Navigator Id');
    }
    
    @isTest static void doLogin_Test2(){
        
        Contact navigatorUser = [SELECT ID, Email, Navigator__c
                                 FROM Contact
                                 WHERE Email = 'testing@test.com' LIMIT 1];
       
        
        Test.startTest();
        try{
        NavigatorController.doLogin('testing@test.com', navigatorUser.Navigator__c);            
        }catch(exception e){
            
        }
        Test.stopTest();
        
    }
    
    @isTest static void testGetNavigatorWidget(){
        Navigator__c navigator = new Navigator__c();
        navigator.Name = 'TestNavigator';
        navigator.Change_Agent_Lead_Label__c = 'Test';
        navigator.About_Label__c = 'TestAbout';
        navigator.About_Image_Video_Link__c = 'salesforce.com';
        insert navigator;
        
        Navigator_Widget__c navWidget = new Navigator_Widget__c();
        navWidget.Navigator__c = navigator.Id;
        navWidget.Widget_Label__c = 'Email Widget';
        navWidget.Widget_Name__c = 'Email Activity';
        navWidget.Widget_Parent_Object__c = 'Contact';
        navWidget.Widget_Child_Object__c = 'Communication_Recipient__c';
        insert navWidget;
        
        Test.startTest();
        NavigatorController.getNavigatorWidgets(navigator.Id, null);
        Test.stopTest();
    }
}