/**
 * @description Controller class for 'JourneyPage' Aura Component
 * @revision
 *           11.09.2020 - APRivera - Updated Class
 **/
public with sharing class TheProviderJourney {
  /*******************************************************************************
   * @author       Angelo Rivera
   * @date         11.09.2020
   * @description  Wrapper Class for User Journey, Milestone, Metrics and Actions
   * @revision     11.09.2020 - APRivera - Created
   *******************************************************************************/
  public class UserJourneyWrapperClass {
    @AuraEnabled
    public Journey_Participant__c userJourney { get; set; }
    @AuraEnabled
    public List<Participant_Milestone__c> lstUserMilestones { get; set; }
    @AuraEnabled
    public Map<String, List<Participant_Milestone_Metric__c>> mapMetricsByMilestoneName {
      get;
      set;
    }
    @AuraEnabled
    public Map<String, List<Task>> mapTasksByMilestoneName { get; set; }
    @AuraEnabled
    public List<Milestone__c> journeyMilestoneList;
    @AuraEnabled
    public Journey__c journeyData;
    @AuraEnabled
    public Boolean urlFromJourney;
    @AuraEnabled
    public String CampaignLogoURL;
    @AuraEnabled
    public String JourneyLogoURL;
  }

  /*******************************************************************************
   * @author       Angelo Rivera
   * @date         11.09.2020
   * @description  Retrieves User Journey Record with related User Milestone, Actions and Metrics
   * @param        strJourneyId - List of User Milestones to be processed
   * @return       journeyWrapper - instance of UserJourneyWrapperClass
   * @revision     11.09.2020 - APRivera - Created
   * @revision     12.24.2020 - APRivera - CCN-1086 - Fixed milestone data volume issue
   *******************************************************************************/
  @AuraEnabled
  public static UserJourneyWrapperClass getUserJourney(Id strJourneyId) {
    UserJourneyWrapperClass journeyWrapper = new UserJourneyWrapperClass();

    Set<Id> setJourneyIds = new Set<Id>();
    Set<Id> setUserJourneyIds = new Set<Id>();
    String objectNameOfId = strJourneyId.getSObjectType()
      .getDescribe()
      .getName();
    System.debug(LoggingLevel.DEBUG, '!@# objectNameOfId: ' + objectNameOfId);
    setUserJourneyIds.add(strJourneyId);
    //retrieves User Journey Record
    Map<Id, Journey_Participant__c> mapUserJourneysByIds = new Map<Id, Journey_Participant__c>();
    if (objectNameOfId.equals('Journey_Participant__c')) {
      mapUserJourneysByIds = JourneyParticipantServices.getUserJourneys(setUserJourneyIds);
      System.debug(LoggingLevel.DEBUG, '!@# mapUserJourneysByIds: ' + mapUserJourneysByIds.size());
      journeyWrapper.urlFromJourney = false;

      if (!mapUserJourneysByIds.isEmpty()) {
        journeyWrapper.userJourney = new Journey_Participant__c();
        journeyWrapper.lstUserMilestones = new List<Participant_Milestone__c>();
        journeyWrapper.userJourney = mapUserJourneysByIds.get(strJourneyId);
        setJourneyIds.add(journeyWrapper.userJourney.Journey__c);
        journeyWrapper.CampaignLogoURL = journeyWrapper.userJourney.Journey__r.Campaign_Logo_URL__c;
        journeyWrapper.JourneyLogoURL = journeyWrapper.userJourney.Journey__r.Journey_Image_URL__c;

        List<Participant_Milestone__c> lstJourneyParticipantsMilestones = new List<Participant_Milestone__c>();
        lstJourneyParticipantsMilestones = JourneyParticipantServices.getJourneyParticipantMilestones(strJourneyId);

        if (!lstJourneyParticipantsMilestones.isEmpty()) {
          journeyWrapper.lstUserMilestones.addAll(lstJourneyParticipantsMilestones);
          Set<Id> setMilestoneIds = new Set<Id>();
          for (Participant_Milestone__c um : journeyWrapper.lstUserMilestones) {
            setMilestoneIds.add(um.Id);
          }
          System.debug('+++ setMilestoneIds' + setMilestoneIds);

          if (!setMilestoneIds.isEmpty()) {
            Map<Id, Participant_Milestone__c> mapMilestoneByIds = JourneyParticipantServices.getUserMilestones(setMilestoneIds);
            journeyWrapper.mapMetricsByMilestoneName = new Map<String, List<Participant_Milestone_Metric__c>>();
            journeyWrapper.mapTasksByMilestoneName = new Map<String, List<Task>>();
            for (Participant_Milestone__c um : mapMilestoneByIds.values()) {
              //retrieves User Milestone Metrics
              if (!um.Participant_Milestone_Metrics__r.isEmpty()) {
                journeyWrapper.mapMetricsByMilestoneName.put(
                  um.Name,
                  um.Participant_Milestone_Metrics__r
                );
              }
              //retrieves User Milestone Actions
              if (!um.Tasks.isEmpty()) {
                journeyWrapper.mapTasksByMilestoneName.put(um.Name, um.Tasks);
              }
            }
          }
        }
      }
    System.debug('+++ setJourneyIds' + setJourneyIds);
      Journey__c jData = JourneyParticipantServices.getMilestoneFromJourney(setJourneyIds);
      journeyWrapper.journeyMilestoneList = jData.Milestones__r;
      journeyWrapper.journeyData = jData;
    } else if (objectNameOfId.equals('Journey__c')) {
      setJourneyIds.add(strJourneyId);
      Journey__c jData = JourneyParticipantServices.getMilestoneFromJourney(setJourneyIds);
      journeyWrapper.journeyMilestoneList = jData.Milestones__r;
      journeyWrapper.journeyData = jData;
      journeyWrapper.urlFromJourney = true;
    }
    System.debug(LoggingLevel.DEBUG, '!@# Data: ' + journeyWrapper);
    return journeyWrapper;
  }

  /*******************************************************************************
   * @author       Kyzer Buhay
   * @date         05.02.2021
   * @description  Task Creation for Contact Us form submission
   * @param        userJourney - User's Journey to be tagged as Related to and owned by Contact Us User field. If empty, use Journey Owner
   * @return       newTask - newly task record created
   *******************************************************************************/
  @AuraEnabled
  public static Boolean createTaskContactUs(String contactUserId, String fName, String lName, String email, String phone,
                                            Id userJourney, String subject, String comment) {
    return new ElevatedContext().createTaskWithoutSharing(contactUserId, fName, lName, email, phone, userJourney, subject, comment);
  }
    
    /*******************************************************************************
    * @author       Kyzer Buhay
    * @date         05.02.2021
    * @description  used in Survey completion Button in dashboard
    * @param        participant Milestone selected in Journey Dashboard
    *******************************************************************************/  
    @AuraEnabled
    public static Boolean processSurveyAssessmentComplete(String participantMilestoneId){
        Boolean hasError = false;
        try{
            Participant_Milestone_Metric__c pmmRecord;
            List<Participant_Milestone__c> participantMilestoneList = new List<Participant_Milestone__c>();
            
            if(FlsUtils.isAccessible('Participant_Milestone_Metric__c', new List<String>{ 'Completion_Date__c' })){
                pmmRecord = JourneyParticipantServices.getParticipantMilestoneMetric(participantMilestoneId);
                pmmRecord.Completion_Date__c = System.now();
                hasError = new ElevatedContext().updateParticipantMilestoneMetric(pmmRecord);  
            }
            
            if(FlsUtils.isAccessible('Participant_Milestone__c', new List<String>{ 'Contact__c', 'Checkpoint_Name__c', 'Milestone_Progress__c', 'Checkpoint__c' })){
                participantMilestoneList = new ElevatedContext().getParticipantMilestoneCheckpoint(pmmRecord);
                    
                Integer divider = 0;
                Double totalProgress = 0;
                Participant_Milestone__c targetCheckpointPM;
                for(Participant_Milestone__c participantMilestone : participantMilestoneList){
                    Decimal milestoneProgress = 0;
                    milestoneProgress = participantMilestone.Milestone_Progress__c != null ? participantMilestone.Milestone_Progress__c : 0;
                    if(participantMilestone.Checkpoint__c){
                        targetCheckpointPM = participantMilestone;
                    }else{
                        divider += 1;
                        totalProgress = totalProgress + milestoneProgress;
                    } 
                }
                
                if(targetCheckpointPM != null){
                    targetCheckpointPM.Checkpoint_Progress__c = totalProgress/divider;
                    hasError = new ElevatedContext().updateParticipantMilestone(targetCheckpointPM);
                }
            }
        }catch(Exception e){
            System.debug(e);
            hasError = true;
        }
       
        return hasError;
    }
    
    /*******************************************************************************
    * @author       Rianno Rizarri
    * @date         10.17.2021
    * @description  used in Event catch up Button in dashboard
    * @param        participant Milestone selected in Journey Dashboard
    *******************************************************************************/  
    @AuraEnabled
    public static Boolean processEventCatchUp(String participantMilestoneId){
        Boolean hasError = false;
        // try{
            List<Participant_Milestone_Metric__c> participantMilestoneMetrics = new List<Participant_Milestone_Metric__c>();
            // participantMilestoneMetrics = [
            //     SELECT Id, Milestone__r.Contact__c
            //     FROM Participant_Milestone_Metric__c
            //     WHERE Milestone__c =: participantMilestoneId
            // ];
            Participant_Milestone__c participantMilestone = new Participant_Milestone__c();
            participantMilestone = [SELECT Contact__c, (SELECT Metric_used__c, Completion_Date__c FROM Participant_Milestone_Metrics__r 
                                                        WHERE Metric_used__c IN ('Attendance Date', 'Invitation Date','Registration Date')) 
                                    FROM Participant_Milestone__c WHERE ID =: participantMilestoneId];
            system.debug(participantMilestoneId);
            system.debug('participantMilestone');
            system.debug(participantMilestone);
            system.debug(participantMilestone != null);
        	Boolean hasSession = new ElevatedContext().updateParticipantSession(participantMilestoneId, participantMilestone.Contact__c);
            if (participantMilestoneMetrics != null){
                for(Participant_Milestone_Metric__c participantMilestoneMetricRec : participantMilestone.Participant_Milestone_Metrics__r){
                    // Updated by RiannoRizarri [CCN843] March252022
                    String metricUsed = '';
                    if(hasSession){
                        if(participantMilestoneMetricRec.Metric_used__c == 'Attendance Date'){
                            participantMilestoneMetrics.add(new Participant_Milestone_Metric__c(Id=participantMilestoneMetricRec.Id,
                                                                                                Completion_Date__c = System.now(), metric_used__c = 'Catch-Up Date'));
                        }else{
                            if(participantMilestoneMetricRec.Completion_Date__c == null){
                                participantMilestoneMetrics.add(new Participant_Milestone_Metric__c(Id=participantMilestoneMetricRec.Id,
                                                                                                Completion_Date__c = System.now()));
                            }
                        }
                    }else{
                        if(participantMilestoneMetricRec.Metric_used__c == 'Attendance Date'){
                            participantMilestoneMetrics.add(new Participant_Milestone_Metric__c(Id=participantMilestoneMetricRec.Id,
                                                                                                Completion_Date__c = System.now(), metric_used__c = 'Catch-Up Date'));
                        }else{
                            participantMilestoneMetrics.add(new Participant_Milestone_Metric__c(Id=participantMilestoneMetricRec.Id,
                                                                                                Completion_Date__c = null));
                        }
                    }
                    
                    // Updated by RiannoRizarri [CCN843] March252022
                }
				
                hasError = new ElevatedContext().updateParticipantMilestoneMetricList(participantMilestoneMetrics);
                
            }
        // }catch(Exception e){
        //     hasError = true;
        // }
       
        return hasError;
    }

  
    Public without sharing class ElevatedContext {
        /*******************************************************************************
        * @author       Kyzer Buhay
        * @date         05.02.2021
        * @description  without sharing actions which access can't be provided to Guest User
        *******************************************************************************/
        public Boolean createTaskWithoutSharing( String contactUserId, String fName, String lName, String email,
                                                    String phone, Id userJourney, String subject, String comment ) {
          Boolean hasError = false;
            try {
                Task newTask = new Task();
                Journey__c journeyRec = new Journey__c();
                Id recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Community_Journey_Contact_Us').getRecordTypeId();
                newTask.RecordTypeId = recordTypeId;
                
                if (FlsUtils.isAccessible( 'Journey__c', new List<String>{ 'Contact_Us_User__c' })) {
                    journeyRec = [ SELECT Id, OwnerId, Contact_Us_User__r.Id FROM Journey__c WHERE Id = :userJourney LIMIT 1 ];
                }
                newTask.OwnerId = String.isEmpty(journeyRec.Contact_Us_User__r.Id) ? journeyRec.OwnerId : journeyRec.Contact_Us_User__c;
                newTask.WhoId = contactUserId;
        
                newTask.Subject = subject;
                newTask.WhatId = userJourney;
                newTask.Description = comment;
                newTask.Type = 'Email';
                newTask.Task_Origin__c = 'Journey Participant - Contact Us Page';
                newTask.ActivityDate = System.today();
        		    newTask.Phone_Number__c = phone; //CCN-JOU-1224-DV Xen Reyes 10 Apr 2022
                
                insert newTask;

                //CCN-JOU-1224-DV Xen Reyes 28 Apr 2022
                List<Contact> conList = [SELECT Id, Phone FROM Contact WHERE Id =: contactUserId LIMIT 1];
                if(!conList.isEmpty()){
                  if(conList[0].Phone == null && phone != null){
                    String invalidNumbers = '[^0-9]';  
                    conList[0].Phone = phone.replaceAll( invalidNumbers, '' );
                    update conList[0];
                  }
                }

            } catch (Exception e) {
                hasError = true;
                System.debug( LoggingLevel.DEBUG, '!@# ERROR IN INSERT: ' + e.getMessage() );
            }
                                                        
            return hasError;
        }
        
        /*******************************************************************************
        * @author       Kyzer Buhay
        * @date         05.02.2021
        * @description  without sharing dml update for Participant Milestone Metric Records;
        *******************************************************************************/
        public Boolean updateParticipantMilestoneMetric(Participant_Milestone_Metric__c pmmRecord){
            Boolean hasError = false;
            try{
                if(pmmRecord != null){
                    update pmmRecord;
                }
            }catch(Exception e){
                hasError = true;
            }
            
            return hasError;
        }
        
        /*******************************************************************************
        * @author       Rianno Rizarri
        * @date         10.17.2021
        * @description  without sharing dml update for Participant Milestone Metric Records;
        *******************************************************************************/
        public Boolean updateParticipantMilestoneMetricList(List<Participant_Milestone_Metric__c> pmmRecord){
            Boolean hasError = false;
            try{
                if(pmmRecord != null){
                    update pmmRecord;
                }
            }catch(Exception e){
                hasError = true;
            }
            
            return hasError;
        }

                
      /*******************************************************************************
        * @author       Rianno Rizarri
        * @date         10.17.2021
        * @description  without sharing dml update for Participant Milestone Metric Records;
        *******************************************************************************/
        public Boolean updateParticipantSession(Id participantMilestoneId, Id contactId){
            Boolean hasSession = false;
            system.debug(participantMilestoneId);
            system.debug(contactId);
            Set<Id> milestoneRelatedRecordSet = new Set<Id>();
            // Updated by RiannoRizarri [CCN-1609] May 3 2022
            Milestone__c eventMilestone = new Milestone__c();
            eventMilestone = [SELECT Related_RecordId__c 
                              FROM Milestone__c 
                              WHERE ID IN(SELECT Milestone__c 
                                          FROM Participant_Milestone__c 
                                          WHERE ID =: participantMilestoneId)];
            if(eventMilestone <> null){
                List<Milestone__c> childMilestones = new List<Milestone__c>();
                childMilestones = [SELECT Related_RecordId__c 
                                   FROM Milestone__c 
                                   WHERE Parent_Milestone__c =: eventMilestone.Id];
                
                if(childMilestones.size() > 0){
                    for(Milestone__c childMilestone : childMilestones){
                        milestoneRelatedRecordSet.add(childMilestone.Related_RecordId__c); 
                    }
                }
                String objectType = '';
                if(eventMilestone.Related_RecordId__c <> null){
                    milestoneRelatedRecordSet.add(eventMilestone.Related_RecordId__c);
                    objectType = checkObject(eventMilestone.Related_RecordId__c);
                }
                if(objectType == 'Session__c'){
                    // Updated by RiannoRizarri [CCN843] March252022
                    List<Session_Participant__c> sessionParticipant = new List<Session_Participant__c>();
                    sessionParticipant = [SELECT Contact__c FROM Session_Participant__c 
                                          WHERE Session__r.Id IN: milestoneRelatedRecordSet AND 
                                          (Contact__c =: contactId 
                                           OR Participant__r.Member_Contact__c =: contactId)];
                    if(sessionParticipant.size() > 0){
                        Session_Participant__c updateSeshPart = new Session_Participant__c();
                        updateSeshPart.Status__c = 'Catchup';
                        updateSeshPart.Catchup_Date__c = System.Now();
                        updateSeshPart.Id = sessionParticipant[0].Id;
                        Update updateSeshPart;
                        hasSession = true;
                    }
                    // Updated by RiannoRizarri [CCN843] March252022
                }
            }
            // Updated by RiannoRizarri [CCN-1609] May 3 2022
            return hasSession;
        }
        
        /*******************************************************************************
        * @author       Kyzer Buhay
        * @date         05.02.2021
        * @description  without sharing actions which access can't be provided to Guest User
        *******************************************************************************/
        public Boolean updateParticipantMilestone(Participant_Milestone__c pmRecord){
            Boolean hasError = false;
            try{
                if(pmRecord != null){
                    //pmmRecord.Completion_Date__c = System.now();
                    update pmRecord;
                }
            }catch(Exception e){
                hasError = true;
            }
            
            return hasError;
        }
        
        public List<Journey_Participant__c> getJourneyViaJourneyAndContact(String journeyId, String contactEmail){
            return [SELECT Id, Journey_URL__c FROM Journey_Participant__c WHERE Journey__c = :journeyId AND Contact__r.Email = :contactEmail];
        }
        
        public List<Participant_Milestone__c> getParticipantMilestoneCheckpoint(Participant_Milestone_Metric__c pmmRecord){
            return [SELECT Id, Contact__c, Checkpoint_Name__c, Milestone_Progress__c, Checkpoint__c
                                                                        FROM Participant_Milestone__c 
                                                                        WHERE Contact__r.Id =: pmmRecord.Milestone__r.Contact__c AND 
                                                                        (Checkpoint_Name__r.Id =: pmmRecord.Milestone__r.Checkpoint_Name__c OR 
                                                                                        Milestone__r.Id =: pmmRecord.Milestone__r.Checkpoint_Name__c)];
        }
    }

  /*******************************************************************************
   * @author       Jayson Sarion
   * @date         05.05.2021
   * @description  Validate email to match Journey Participants
   * @param        inputEmail - user email
   * @param       journeyId - Journey Paricipant that match the Email and Journey
   * @return       journeyId - Journey Paricipant that match the Email and Journey
   *******************************************************************************/
  @AuraEnabled
  public static String validateParticipant(String inputEmail, String journeyId) {
    List<Journey_Participant__c> journeyParticipantList = new List<Journey_Participant__c>();
    if (FlsUtils.isAccessible('Journey_Participant__c', new List<String>{ 'Journey_URL__c' })) {
      journeyParticipantList = new ElevatedContext().getJourneyViaJourneyAndContact(journeyId, inputEmail);
    }
    if (!journeyParticipantList.isEmpty()) {
      return journeyParticipantList[0].Journey_URL__c;
    }

    return 'false';
  }

  /*******************************************************************************
   * @author       Jayson Sarion
   * @date         05.05.2021
   * @description  Check Object instance of ID
   * @param        objId - record Id
   * @return       id object
   *******************************************************************************/
  @AuraEnabled
  public static String checkObject(Id objId) {
    return String.valueOf(objId.getSobjectType());
  }
}