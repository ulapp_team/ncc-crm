/******************************************************************************    
    * Changes (version)
    *            No.  Date(dd-mm-yyyy)          Author                      Description
    *           ----  -----------------    -------------------       -----------------------------
    * @version   1.0  23-06-2022             Von Pernicia            [CCN-EVE-2390-DV] Intial Version. Helper to pull information that will be display in the email template
    ******************************************************************************/ 
public with sharing class EventDeliveryConfirmationHelper {

    @testVisible
    public static Boolean throwFakeException = false;

    // Get sessions details
    public static EventConfirmationWrapper getSessionsDetails(String internalResourceId) {
        String idStringForUrl = ''; 
        String domainName = [SELECT Id, Domain_Name_Event_Registration__c FROM Compass_Setting__c WHERE Name = 'Default Settings' LIMIT 1].Domain_Name_Event_Registration__c;
        EventConfirmationWrapper eventConfirmationWrapper = new EventConfirmationWrapper(); 
        Set<String> statusToConsider = new Set<String>{'Registered', 'Confirmed', 'Declined', 'Canceled'}; 
        List<SessionWrapper> sessionsWrappers = new List<SessionWrapper>();
        List<Internal_Resource__c> internalResourceList = new List<Internal_Resource__c>(); 
        List<Id> sessionParticipantIds = new List<Id>(); 
        Map<Id, Internal_Resource__c> mapInternalResource; 

        // get event and contact ids
        if( FlsUtils.isAccessible('Internal_Resource__c', new List<String>{'Contact__c','Event__c'})){
            internalResourceList = [SELECT Id, Contact__c, Event__c
                               FROM Internal_Resource__c 
                               WHERE Id =: internalResourceId LIMIT 1];
            mapInternalResource = new Map<Id, Internal_Resource__c>(internalResourceList); 
        }

        

        if (mapInternalResource <> null && mapInternalResource.size() > 0){ 
            String strTZone = [SELECT Id, TimeZoneSidKey FROM Organization LIMIT 1].TimeZoneSidKey;
            if(FlsUtils.isAccessible('Internal_Resource_Session__c', new List<String>{'Name','Event__c','Contact__c','Session__c'})){
                   for (Internal_Resource_Session__c s : [SELECT  Id, 
                                                            Name,
                                                            Event__c, 
                                                            Contact__c,
                                                            Resource_Session_Role__c,
                                                            Resource_Session_Status__c,
                                                            Confirm_Response_Link__c,
                                                            Decline_Response_Link__c,
                                                            Session__r.Start_Date_Time__c,
                                                            Session__r.End_Date_Time__c,
                                                            Event__r.Location__r.Name,
                                                            Session__r.Time_Zone__c,
                                                            Session__r.Name, 
                                                            Session__r.Location__c
                                                    FROM    Internal_Resource_Session__c
                                                    WHERE   Contact__c =: mapInternalResource.get(internalResourceId).Contact__c
                                                    AND     Resource_Session_Status__c IN :statusToConsider 
                                                    AND     Resource__c =: mapInternalResource.get(internalResourceId).Id
                                                    ORDER BY Resource_Registration_Date_Time__c]){ 
                                                        
                                                        if(s != null && (s.Session__r.Start_Date_Time__c > System.now())) {
                                                            SessionWrapper sw = new SessionWrapper();
                                                            sw.Name = s.Session__r.Name;
                                                            sw.SessionDate = s.Session__r.Start_Date_Time__c.format('MMMM dd, yyyy', strTZone);
                                                            sw.SessionTime = s.Session__r.Start_Date_Time__c.format('h:mm a', strTZone) + ' to '; 
                                                            sw.SessionTime2 = s.Session__r.End_Date_Time__c.format('h:mm a', strTZone) + ' ' + s.Session__r.Time_Zone__c;
                                                            sw.SessionLocation = s.Session__r.Location__c != null ? s.Session__r.Location__c : s.Event__r.Location__r.Name;
                                                            sw.confirmResponseLink = s.Confirm_Response_Link__c;
                                                            sw.declineResponseLink = s.Decline_Response_Link__c;
                                                            sw.status = s.Resource_Session_Status__c;
                                                            sw.Role = s.Resource_Session_Role__c;
                                                            sessionsWrappers.add(sw);
                                                            
                                                            
                                                            if(s.Resource_Session_Status__c == 'Registered') {
                                                                sessionParticipantIds.add(s.Id);   
                                                            }
                                                        }
                                                    }
                   
                   for(Id spId : sessionParticipantIds) {
                       idStringForUrl += String.valueOf(spId) + '+';
                   }
                   
                   eventConfirmationWrapper.showConfirmDeclineAllButton = sessionParticipantIds.size() > 1 ? true : false; 
                   eventConfirmationWrapper.confirmAllLink = domainName + '/?id=' + idStringForUrl.removeEnd('+') + '&response=Confirmed'; 
                   eventConfirmationWrapper.declineAllLink = domainName + '/?id=' + idStringForUrl.removeEnd('+') + '&response=Declined'; 
                   eventConfirmationWrapper.sessionList = sessionsWrappers; 
               }
        }

        return eventConfirmationWrapper; 
    }
    
    public class EventConfirmationWrapper {
        public Boolean showConfirmDeclineAllButton {get;set;}
        public String confirmAllLink {get;set;}
        public String declineAllLink {get;set;}
        public List<SessionWrapper> sessionList {get;set;}
    }
    
    public class SessionWrapper{
        public String Name {get; set;}
        public String SessionDate {get; set;}
        public String SessionTime {get; set;}
        public String SessionTime2 {get;set;}
        public String SessionLocation {get; set;}
        public String confirmResponseLink {get;set;}
        public String declineResponseLink {get;set;}
        public String status {get;set;}
        public String Role {get;set;}
    }
    
    // Get formatted date time
    public static String getFormattedDateTimeHelper(String campaignOwnerId, String campaignId, String startOrEndDate, String defined_format, String timeOrDate){
        String FormattedDatetime;
        if (campaignOwnerId == null || campaignId == null || startOrEndDate == null){
            return '';
        } else {
            String queryString = (startOrEndDate.toLowerCase() == 'start') ? 'SELECT Start_Date_Time__c, End_Date_Time__c, OwnerId, Contact_Us_User__c, Time_Zone__c FROM Event__c WHERE Id =: campaignId LIMIT 1'
                : 'SELECT Start_Date_Time__c, End_Date_Time__c, OwnerId, Contact_Us_User__c, Time_Zone__c FROM Event__c WHERE Id =: campaignId LIMIT 1';
            List<sObject> sobjList = Database.query(queryString);
            if (sobjList.size() > 0){
                Event__c campaign = (Event__c)sobjList[0];
                if (campaign != null ){
                    String strTZone = [SELECT Id, TimeZoneSidKey FROM Organization LIMIT 1].TimeZoneSidKey;
                    if (campaign != null && throwFakeException == false){
                        DateTime localDateTime;
                        if (startOrEndDate.toLowerCase() == 'start' && campaign.Start_Date_Time__c != null){
                            localDateTime = campaign.Start_Date_Time__c;
                        } 
                        if (startOrEndDate.toLowerCase() == 'end' && campaign.End_Date_Time__c != null ){
                            localDateTime = campaign.End_Date_Time__c; 
                        }
                        if (localDateTime != null) {
                            if(timeOrdate.toLowerCase() == 'date' || (startOrEndDate.toLowerCase() == 'start' && timeOrdate.toLowerCase() != 'date')) {
                                FormattedDatetime = localDateTime.format(defined_format, strTZone);
                            }
                            else {
                                FormattedDatetime = localDateTime.format(defined_format, strTZone) + ' ' + campaign.Time_Zone__c;
                            }
                        }
                            
                        return FormattedDatetime;
                    }
                }  
            }
          
            return '';
            
        }
    }
}