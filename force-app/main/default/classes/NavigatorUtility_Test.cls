@isTest
public with sharing class NavigatorUtility_Test {
    
    @TestSetup
    static void setup(){
        Contact testAgent = new Contact();
        testAgent.LastName = 'test';
        testAgent.Title = 'test';
        testAgent.Profile_Picture_URL__c = 'test';
        Insert testAgent;
        
        Navigator__c testNav = new Navigator__c();
        testNav.Name = 'test';
        testNav.Header_Text__c = 'test';
        testNav.Header_Image__c = 'test';
        testNav.Footer_Image__c = 'test';
        testNav.Branding_Logo__c = 'test';
        testNav.App_Logo__c = 'test';
        testNav.AWS_Link__c = 'test';
        testNav.Mascot_Image__c = 'test';
        testNav.Login_Page_URL__c = 'test';
        testNav.Enable_Token_Login__c = false;
        testNav.Theme_Color_1__c = 'test';
        testNav.Theme_Color_2__c = 'test';
        testNav.Theme_Color_3__c = 'test';
        testNav.Theme_Color_4__c = 'test';
        testNav.Change_Agent__c = testAgent.Id;
        testNav.Live_Call_Number__c = 'test';
        testNav.Mascot_Message__c = 'test';
        testNav.About_Label__c = 'test';
        testNav.Change_Agent_Lead_Label__c = 'test';
        Insert testNav;

        Navigator_Item__c testNavItem = new Navigator_Item__c();
        testNavItem.Name = 'test';
        testNavItem.Navigator__c = testNav.Id;
        testNavItem.Sort_Order__c = 1;
        testNavItem.Page_URL__c = 'left';
        testNavItem.SLDS_Icon_Name__c = 'test';
        testNavItem.Expanded_Items__c = 'test';
        testNavItem.Image_URL__c = 'test';
        testNavItem.Label__c = 'test';
        testNavItem.Site_Page_Name__c = 'test';
        Insert testNavItem;

        Navigator_Section__c testNavSection = new Navigator_Section__c();
        testNavSection.Name = 'test';
        testNavSection.Sort_Order__c = 1;
        testNavSection.Navigator_Item__c = testNavItem.Id;
        Insert testNavSection;

        Navigator_Section_Attribute__c testNavSectionAttr = new Navigator_Section_Attribute__c();
        testNavSectionAttr.Name = 'test';
        testNavSectionAttr.Type__c = 'text';
        testNavSectionAttr.Alignment__c = 'left';
        testNavSectionAttr.Sort_Order__c = 1;
        testNavSectionAttr.Value__c = 'test';
        testNavSectionAttr.Navigator_Section__c = testNavSection.Id;
        Insert testNavSectionAttr;

        Contact testCon = new Contact();
        testCon.Title = 'test';
        testCon.FirstName = 'test';
        testCon.LastName = 'testUser';
        testCon.Email = 'test@test.com';
        testCon.Login_Token__c = 'test';
        testCon.Profile_Picture_URL__c = 'test';
        testCon.Navigator__c = testNav.Id;
        Insert testCon;

        Compass_Setting__c compassSetting = new Compass_Setting__c();
        compassSetting.Name = 'Default Settings';
        compassSetting.Domain_Name__c = 'test';
        Insert compassSetting;

        
        //START - Jonah - 3417 - Oct 9
        Account a = new Account();
        a.BillingStreet = 'Balston';
        a.BillingCity = 'Melbourne';
        a.BillingPostalCode = '3006';
        a.BillingState  = 'VIC';
        a.BillingCountry  = 'Australia';
        a.Name  = 'Imagine Marco';
        insert a;
        //END - Jonah - 3417 - Oct 9
        
        Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = 'test';
        testCampaign.Account__c = a.Id; // Jonah - 3417 - Oct 9
        Insert testCampaign;

        Event__c testEvent = new Event__c();
        testEvent.Name = 'test';
        testEvent.Start_Date_Time__c = Date.today();
        testEvent.End_Date_Time__c = Date.today().addDays(1);
        testEvent.Time_Zone__c = 'PST';
        testEvent.Event_Time_Details__c = 'test';
        testEvent.Campaign__c = testCampaign.Id;
        Insert testEvent;
    }

    @isTest static void buildContactObj_TestPositive(){
        Contact testCon = [
            SELECT Title, Name, FirstName, LastName, Email, Login_Token__c, Profile_Picture_URL__c, Navigator__c 
            FROM Contact 
            WHERE LastName = 'testUser'
        ];

        Test.startTest();
        NavigatorUtility.ContactWrapper testConWrap = NavigatorUtility.buildContactObj(testCon);
        Test.stopTest();

        System.assert(testConWrap != null);
    }

    @isTest static void buildNavSectionAttributeObj_TestPositive(){
        Navigator_Section_Attribute__c testNavSectionAttr = [
            SELECT Name, Type__c, Alignment__c, Sort_Order__c, Value__c
            FROM Navigator_Section_Attribute__c LIMIT 1
        ];

        Test.startTest();
        NavigatorUtility.NavigatorSectionAttributeWrapper testNavSectAttrWrap = NavigatorUtility.buildNavSectionAttributeObj(testNavSectionAttr);
        Test.stopTest();

        System.assert(testNavSectAttrWrap != null);
    }

    @isTest static void buildNavigatorObj_TestPositive(){
        Navigator__c testNav = [
            SELECT Name, Header_Text__c, Header_Image__c, Footer_Image__c, Branding_Logo__c, App_Logo__c, AWS_Link__c, Mascot_Image__c, Login_Page_URL__c,
                Enable_Token_Login__c, Theme_Color_1__c, Theme_Color_2__c, Theme_Color_3__c, Theme_Color_4__c, Change_Agent__c, Live_Call_Number__c,
                Change_Agent__r.Name, Change_Agent__r.Title, Change_Agent__r.Profile_Picture_URL__c, Mascot_Message__c
            FROM Navigator__c LIMIT 1
        ];

        Test.startTest();
        NavigatorUtility.NavigatorWrapper testNavWrap = NavigatorUtility.buildNavigatorObj(testNav);
        Test.stopTest();

        System.assert(testNavWrap != null);
    }

    @isTest static void buildNavItemWrapper_TestPositive(){
        Navigator_Item__c testNavItem = [
            SELECT Name, Navigator__c, Sort_Order__c, Page_URL__c, SLDS_Icon_Name__c, Expanded_Items__c, Image_URL__c
            FROM Navigator_Item__c LIMIT 1
        ];

        Test.startTest();
        NavigatorUtility.NavigatorItemWrapper testNavItemWrap = NavigatorUtility.buildNavItemWrapper(testNavItem);
        Test.stopTest();

        System.assert(testNavItemWrap != null);
    }

    @isTest static void buildEventObj_TestPositive(){
        Event__c testEvent = [
            SELECT Name, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, Event_Time_Details__c 
            FROM Event__c LIMIT 1
        ];

        Test.startTest();
        NavigatorUtility.EventWrapper testEventWrap = NavigatorUtility.buildEventObj(testEvent);
        Test.stopTest();

        System.assert(testEventWrap != null);
    }

    @isTest static void buildContactObj_TestNegative(){
        Contact testCon = null;

        Test.startTest();
        NavigatorUtility.ContactWrapper testConWrap = NavigatorUtility.buildContactObj(testCon);
        Test.stopTest();

        System.assert(testConWrap != null);
    }

    
    @isTest static void buildNavigatorObj_TestNegative(){
        Navigator__c testNav = null;

        Test.startTest();
        NavigatorUtility.NavigatorWrapper testNavWrap = NavigatorUtility.buildNavigatorObj(testNav);
        Test.stopTest();

        System.assert(testNavWrap != null);
    }

    @isTest static void buildNavSectionAttributeObj_TestNegative(){
        Navigator_Section_Attribute__c testNavSectionAttr = null;

        Test.startTest();
        NavigatorUtility.NavigatorSectionAttributeWrapper testNavSectAttrWrap = NavigatorUtility.buildNavSectionAttributeObj(testNavSectionAttr);
        Test.stopTest();

        System.assert(testNavSectAttrWrap != null);
    }

    @isTest static void buildNavItemWrapper_TestNegative(){
        Navigator_Item__c testNavItem = null;

        Test.startTest();
        NavigatorUtility.NavigatorItemWrapper testNavItemWrap = NavigatorUtility.buildNavItemWrapper(testNavItem);
        Test.stopTest();

        System.assert(testNavItemWrap != null);
    }
    @isTest static void buildEventObj_TestNegative(){
        Event__c testEvent = null; 

        Test.startTest();
        NavigatorUtility.EventWrapper testEventWrap = NavigatorUtility.buildEventObj(testEvent);
        Test.stopTest();

        System.assert(testEventWrap != null);
    }
}