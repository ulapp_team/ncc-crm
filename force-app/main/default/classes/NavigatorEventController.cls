/*******************************************************************************
 * @author       Angelo Rivera
 * @date         05.07.2021
 * @description  Navigator Event Controller
 * @revision     05.07.2021 - APRivera - Created
 *******************************************************************************/

public without sharing class NavigatorEventController {

    public class ContactEvents{
        @AuraEnabled public List<Event__c> listEvents {get; set;}
        @AuraEnabled public String eventId {get; set;}
        @AuraEnabled public Boolean hasEvent {get; set;}
        @AuraEnabled public String communityURL {get; set;}
    }

    /*******************************************************************************
     * @description  Returns the list of contact's events
     * @param        contactId - contact record Id
     * @param        strFilter - event filter
     * @return       ContactEvents
     * @revision    07.07.2021 - APRivera - Created
     *******************************************************************************/
    @AuraEnabled
    public static ContactEvents getContactEvents(String contactId, String strFilter){
        ContactEvents contactEvents = new ContactEvents();
        List<Event__c> lstParticipantsEvents = new List<Event__c>();
        lstParticipantsEvents = getEvents(contactId, strFilter);
        if(!lstParticipantsEvents.isEmpty()){
            contactEvents.hasEvent = true;
            if(lstParticipantsEvents.size() == 1){
                contactEvents.eventId = lstParticipantsEvents.get(0).Event_Id__c;
            }else{
                contactEvents.listEvents = new List<Event__c>();
                for(Event__c e : lstParticipantsEvents){
                        contactEvents.listEvents.add(e);
                }
            }
        }
        contactEvents.communityURL = JourneyParticipantServices.getCommunityURL('Navigator');
        return contactEvents;
    }


    /*******************************************************************************
     * @description  retrieves the list of contact's events
     * @param        contactId - contact record Id
     * @param        strFilter - Event status filter
     * @return       lstEvents - List of Events
     * @revision    07.07.2021 - APRivera - Created
     *******************************************************************************/
    public static List<Event__c> getEvents(String contactId, String strFilter){
        String strEventStatus = '';
        if(strFilter.contains('Upcoming')) strEventStatus = 'Upcoming';
        if(strFilter.contains('On-Going')) strEventStatus = 'On-Going';
        if(strFilter.contains('Completed')) strEventStatus = 'Completed';
        if(strFilter.contains('Missed')) strEventStatus = 'Missed';

        List<Participant_Milestone__c> lstParticipantMilestones = new List<Participant_Milestone__c>();
        lstParticipantMilestones = getParticipantMilestones(contactId);

        List<Event__c> lstEvents = new List<Event__c>();

        if(!lstParticipantMilestones.isEmpty()){
            Set<Id> setRecordIds = new Set<Id>();

            for(Participant_Milestone__c pm : lstParticipantMilestones){
                Id recordId = (Id)pm.Milestone__r.Related_RecordId__c;
                if(recordId.getSobjectType() == Event__c.SObjectType){
                    if(strEventStatus != ''){
                        if(pm.Milestone_Status__c == strEventStatus){
                            setRecordIds.add(recordId);
                        }
                    }else{
                        setRecordIds.add(recordId);
                    }
                }
            }

            if(!setRecordIds.isEmpty()){
                lstEvents = getEventRecords(setRecordIds);
            }
        }
        return lstEvents;
    }

    /*******************************************************************************
     * @description  retrieves the list of Events
     * @param        setEventIds - Set Of Event Ids
     * @return       lstEvents - List of Events
     * @revision    07.07.2021 - APRivera - Created
     *******************************************************************************/
    public static List<Event__c> getEventRecords(Set<Id> setEventIds){
        List<Event__c> lstEvents = new List<Event__c>();
        lstEvents = [SELECT Id, Name, Event_Id__c FROM Event__c WHERE Id IN: setEventIds];
        return lstEvents;
    }

    /*******************************************************************************
     * @description  retrieves the list of Participant Milestones
     * @param        contactId - Contact_Id__c
     * @return       lstParticipantMilestones - List of Participant Milestones
     * @revision    07.07.2021 - APRivera - Created
     *******************************************************************************/
    public static List<Participant_Milestone__c> getParticipantMilestones(String contactId){
        List<Participant_Milestone__c> lstParticipantMilestones = new List<Participant_Milestone__c>();
        lstParticipantMilestones = [SELECT Id, Contact__c, Contact__r.Contact_Id__c,
                                                                Milestone__r.Related_RecordId__c, Type__c, Milestone_Status__c
                                                    FROM Participant_Milestone__c
                                                    WHERE Contact__r.Contact_Id__c =: contactId
                                                    AND Type__c = 'Event'
                                                    AND Milestone__r.Related_RecordId__c != NULL];
        return lstParticipantMilestones;
    }

}