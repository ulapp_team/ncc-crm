/*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Controller for NavigatorHome_BoxType
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized Class
   *               01/22/2023 - Xen Reyes - Added EmailMessage variable in wrapper
   *******************************************************************************/
public class NavigatorHomeComponentsNewController {
    public class SessionWrapper{
        @AuraEnabled 
        public String name;
        @AuraEnabled 
        public String id;
        @AuraEnabled 
        public String eventId;
        @AuraEnabled
        public DateTime startDate;
        @AuraEnabled
        public DateTime endDate;
        @AuraEnabled
        public String sessionTime;
    }
    
    public class DetailsWrapper{
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String id;
        @AuraEnabled
        public String objType;
        @AuraEnabled
        public String parentRecId;
        @AuraEnabled
        public String parentRecTitle;
        @AuraEnabled
        public String parentObjectName;
        @AuraEnabled
        public String recordUrl; // START-END : CCN-NAV-3550-DV NavigatorHome Journey Updates
        @AuraEnabled
        public Communication__c emailObj;
        @AuraEnabled
        public List<SessionWrapper> sessionList;
        @AuraEnabled
        public EmailMessage emailMessageObj;
    }
    
    static Map<Id, DetailsWrapper> detailsWrapperMap = new Map<Id, DetailsWrapper>();
    
   /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets related Navigator_Widget__c
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    @AuraEnabled
    public static List<Navigator_Widget__c> getRelatedWidgets(String navigatorId) {
        List<Navigator_Widget__c> widgetList = [SELECT 
                                                    Id 
                                                FROM Navigator_Widget__c 
                                                WHERE Navigator__c = :navigatorId];
        return widgetList;
    }

    /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets details on the passed record
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *               01/22/2023 - Xen Reyes - Added EmailMessage condition when parent object is contact
   *******************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<DetailsWrapper> getRecordDetails(String navigatorWidget, String contactId) {
        Navigator_Widget__c navWidget = (Navigator_Widget__c) JSON.deserialize(navigatorWidget, Navigator_Widget__c.class);
        system.debug(navWidget);
        List<DetailsWrapper> detailsWrapperList = new List<DetailsWrapper>();
        
        switch on navWidget.Widget_Parent_Object__c{
            when 'Event__c' {
                if('Session_Participant__c'.equalsIgnoreCase(navWidget.Widget_Child_Object__c)) {
                    detailsWrapperList = getEventDetails(navWidget, contactId);
                }
            } 
            when 'Journey__c' {
                if('Participant_Milestone__c'.equalsIgnoreCase(navWidget.Widget_Child_Object__c)) {
                    detailsWrapperList = getMilestones(navWidget, contactId);
                }
            }
            when 'Contact' {
                if('Communication_Recipient__c'.equalsIgnoreCase(navWidget.Widget_Child_Object__c)) {
                    detailsWrapperList = getEmails(navWidget, contactId);
                } else if('EmailMessage'.equalsIgnoreCase(navWidget.Widget_Child_Object__c)) {
                    detailsWrapperList = getEmailMessage(navWidget, contactId);
                }
            }
        }
        
        System.debug('detailsWrapperList');
        System.debug(detailsWrapperList);
        return detailsWrapperList;
    }
    
    /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets Participant_Milestone__c records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *               01/22/2023 - Xen Reyes - Added EmailMessage parameter in createDetailsWrapper
   *******************************************************************************/
    public static List<DetailsWrapper> getMilestones(Navigator_Widget__c navWidget, String contactId) {
        List<DetailsWrapper> detailsWrapperList = new List<DetailsWrapper>();
        
        System.debug(navWidget.Conditions__c);
        System.debug(contactId);
        for(Participant_Milestone__c pMilestones : NavigatorHomeComponentService.getParticipantMilestones(navWidget, contactId)){
            DetailsWrapper detWrapperTemp = createDetailsWrapper(pMilestones.Milestone__c, pMilestones.Milestone__r.Name, null, pMilestones,null,null);
            detailsWrapperList.add(detWrapperTemp);
        }
        
        return detailsWrapperList;
    }
    
    /*******************************************************************************
    * @author       Alyana Navarro
    * @ticket       CCN-NAV-3521-DV
    * @date         10/02/2023
    * @description  Gets all communication records
    * @revision     10/02/2023 - Alyana Navarro - Created
    *               11/01/2023 - Leif Erickson de Gracia - Finalized
    *               01/22/2023 - Xen Reyes - Added EmailMessage parameter in createDetailsWrapper
    *******************************************************************************/
    private static List<DetailsWrapper> getEmails(Navigator_Widget__c navWidget, String contactId) {
        List<DetailsWrapper> detailsWrapperList = new List<DetailsWrapper>();
        
        for(Communication__c emailRec : NavigatorHomeComponentService.getEmails(navWidget, contactId)){
            DetailsWrapper detWrapperTemp = createDetailsWrapper(emailRec.Id, emailRec.Subject__c, null, null,emailRec,null);
            detailsWrapperList.add(detWrapperTemp);
        }
        
        return detailsWrapperList;
    }

    /*******************************************************************************
    * @author       Abram Vixen Reyes
    * @ticket       CCN-NAV-3521-DV
    * @date         01/21/2023
    * @description  Gets all EmailMessage record from contact ActivityHistory related to the campaign
    * @revision     
    *******************************************************************************/
    private static List<DetailsWrapper> getEmailMessage(Navigator_Widget__c navWidget, String contactId) {
        List<DetailsWrapper> detailsWrapperList = new List<DetailsWrapper>();
        
        for(EmailMessage emailRec : NavigatorHomeComponentService.getEmailsActivityHistory(navWidget, contactId)){
            DetailsWrapper detWrapperTemp = createDetailsWrapper(emailRec.Id, emailRec.Subject, null, null,null,emailRec);
            detailsWrapperList.add(detWrapperTemp);
        }
        
        return detailsWrapperList;
    }

    /*******************************************************************************
   * @author       Alyana Navarro
   * @ticket       CCN-NAV-3521-DV
   * @date         10/02/2023
   * @description  Gets Event records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *               01/22/2023 - Xen Reyes - Added EmailMessage parameter in createDetailsWrapper
   *******************************************************************************/
    private static List<DetailsWrapper> getEventDetails(Navigator_Widget__c navWidget, String contactId){
        List<DetailsWrapper> detailsWrapperList = new List<DetailsWrapper>();
        Map<Id, DetailsWrapper> detailsWrapperMap = new Map<Id, DetailsWrapper>();
            
        for(Session_Participant__c pSession : NavigatorHomeComponentService.getParticipantSessions(navWidget, contactId)){
            Session__c eventSession = pSession.Session__r;
            Event__c event = pSession.Event__r;
            
            DetailsWrapper detWrapperTemp = createDetailsWrapper(event.id, event.Name, createSessionWrapper(event.Event_Id__c, eventSession.Id, eventSession.Name, eventSession.Start_Date_Time__c, eventSession.End_Date_Time__c),null,null,null);
        	detailsWrapperMap.put(detWrapperTemp.id, detWrapperTemp);
        }
        
        return detailsWrapperMap.values();
    }
    
    /*******************************************************************************
   * @author       Alyana Navarro
   * @date         10/02/2023
   * @description  Creates the wrapper class for aura frontend usage
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *               01/22/2023 - Xen Reyes - Added EmailMessage parameter and in wrapper
   *******************************************************************************/
    public static DetailsWrapper createDetailsWrapper(  String id, 
                                                        String name, 
                                                        SessionWrapper session, 
                                                        Participant_Milestone__c milestone, 
                                                        Communication__c emailObj,
                                                        EmailMessage emailMessageObj) {
        DetailsWrapper componentDetails = detailsWrapperMap.get(id);
         
        if(componentDetails == null || componentDetails.id == null){
            componentDetails = new DetailsWrapper();
        }
        
        componentDetails.id = id;
        componentDetails.name = name;
        
        if(milestone != null){
            componentDetails.parentRecId = milestone.Milestone__r.Journey__c;
            componentDetails.parentRecTitle = milestone.Milestone__r.Journey__r.Name;
            componentDetails.recordUrl = milestone.Journey__r.Journey_URL__c; // START-END : CCN-NAV-3550-DV NavigatorHome Journey Updates
        } else if (session != null){ componentDetails.parentRecId = session.eventId; componentDetails.parentRecTitle = name; } else { componentDetails.parentRecId = id; componentDetails.parentRecTitle = name; }
        
        componentDetails.emailObj = emailObj;
        componentDetails.emailMessageObj = emailMessageObj;
        
        componentDetails.objType = String.valueOf(((Id)componentDetails.id).getsobjecttype());
        
        
        if(session != null){ if(componentDetails.sessionList == null){ componentDetails.sessionList = new List<SessionWrapper>(); } componentDetails.sessionList.add(session); }
        
        detailsWrapperMap.put(componentDetails.id, componentDetails);
        
        return componentDetails;
    }

    /*******************************************************************************
   * @author       Alyana Navarro
   * @date         10/02/2023
   * @description  Creates the wrapper class for session records
   * @revision     10/02/2023 - Alyana Navarro - Created
   *               11/01/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/
    private static SessionWrapper createSessionWrapper(String eventId, String id, String name, DateTime startDate, DateTime endDate){ SessionWrapper newSession = new SessionWrapper(); newSession.eventId = eventId; newSession.id = id; newSession.name = name; newSession.startDate = startDate; newSession.endDate = endDate; newSession.sessionTime = startDate.format('h:mm a') + '-' + endDate.format('h:mm a'); return newSession; }
}