/*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  Controller for NavigatorMenuConfiguration and the sidebar. we need the guest site users to see the metadata too
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
  public without sharing class NavigatorMenuConfigurationController {
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  gets the Menu Configuration Fields from the navigator record or metadata if not available
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
    @AuraEnabled
    public static List<MenuConfigurationFields> getMenuConfigurationFields(String IdParams) {
        List<MenuConfigurationFields> fieldFormWrapperList = new List<MenuConfigurationFields>();
        
        try {
            Navigator__c currentRecord;
            currentRecord = getRecord(IdParams);
            Map<String, Map <String,String>> recordFieldWrapper = new Map<String, Map <String,String>>();

            if (String.isNotBlank(currentRecord.Menu_Configuration_JSON__c)) {
                Map<String, Map <String,String>> regFieldConfig = (Map<String, Map <String,String>>) JSON.deserialize(currentRecord.Menu_Configuration_JSON__c, Map<String, Map <String,String>>.class);
                for(String key : regFieldConfig.keySet()){
                    recordFieldWrapper.put(key, regFieldConfig.get(key));
                }
            }
            
            for(Navigator_Menu_Configuration__mdt fieldMdt : getMenuConfigurationMdt()){
    			final String KEY = fieldMdt.DeveloperName;
                
                MenuConfigurationFields fieldFormWrapper = new MenuConfigurationFields(
                    KEY,  
                    fieldMdt.Label,
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('fieldLabel') ? recordFieldWrapper.get(KEY).get('fieldLabel') : fieldMdt.Field_Label__c,
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('sortOrder') ? recordFieldWrapper.get(KEY).get('sortOrder') : String.ValueOf(fieldMdt.Sort_Order__c),
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('active') ? Boolean.ValueOf(recordFieldWrapper.get(KEY).get('active')) : fieldMdt.Is_Active__c,
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('readOnly') ? Boolean.ValueOf(recordFieldWrapper.get(KEY).get('readOnly')) : fieldMdt.Is_Read_Only__c,
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('pageUrl') ? recordFieldWrapper.get(KEY).get('pageUrl') : String.ValueOf(fieldMdt.Page_Url__c),
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('sldsIconName') ? recordFieldWrapper.get(KEY).get('sldsIconName') : String.ValueOf(fieldMdt.slds_Icon_Name__c),
                    recordFieldWrapper.containsKey(KEY) && recordFieldWrapper.get(KEY).containsKey('iconimageurl') ? recordFieldWrapper.get(KEY).get('iconimageurl') : String.ValueOf(fieldMdt.icon_image_url__c)
                );
                
                fieldFormWrapperList.add(fieldFormWrapper);
            }
        } catch (Exception e) {
            System.debug('@@@@@ error in: ' + e.getLineNumber() + ' - ' + e.getMessage() + ' - ' + e.getCause());
            throw new AuraHandledException(e.getLineNumber() + ' - ' + e.getMessage() + ' - ' + e.getCause());
        }
        

        return fieldFormWrapperList;
    }
    
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  Save stringified JSON format from Aura to Registration_Fields_JSON__c field
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
    @AuraEnabled
    public static Boolean saveMenuConfigurationFields(String recordId, String updatedJSON) {
        Boolean hasErrors = false;
        System.debug(updatedJSON);
        Navigator__c eventRec = new Navigator__c (Id = recordId, Menu_Configuration_JSON__c = updatedJSON);
        try {
                update eventRec;
        } catch(Exception e){
            hasErrors = true;
            System.debug('@@@@@ error in: ' + e.getLineNumber() + ' - ' + e.getMessage() + ' - ' + e.getCause());
        }
        
        return hasErrors;
    }
    
    
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  returns the navigator record with the JSON config field
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
    public static Navigator__c getRecord(String recId) {
        return [SELECT 
                    Id, 
                    Menu_Configuration_JSON__c
                FROM Navigator__c 
                WHERE Id = : recId];
    }

    
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  gets the metadata for the menu configuration
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
    public static List<Navigator_Menu_Configuration__mdt> getMenuConfigurationMdt() {
        return [SELECT 
                    Label, 
                    Is_Active__c, 
                    DeveloperName, 
                    Field_Label__c, 
                    Sort_Order__c, 
                    Is_Read_Only__c,
                    Page_Url__c,
                    slds_Icon_Name__c,
                    icon_image_url__c
                FROM Navigator_Menu_Configuration__mdt
                Order By Sort_Order__c ASC
                LIMIT 5000];
    }
    

    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @ticket       CCN-NAV-1883-DV
   * @date         11/01/2023
   * @description  wrapper class for the configuration fields as a JSON response
   * @revision     11/01/2023 - Leif Erickson de Gracia - Created
   *******************************************************************************/
    public class MenuConfigurationFields {
        @AuraEnabled public String key                  {get; set;}
        @AuraEnabled public String label                {get; set;}
        @AuraEnabled public String fieldLabel           {get; set;}
        @AuraEnabled public String sortOrder            {get; set;}
        @AuraEnabled public Boolean readOnly            {get; set;}
        @AuraEnabled public Boolean active              {get; set;}
        @AuraEnabled public String pageUrl              {get; set;}
        @AuraEnabled public String sldsIconName         {get; set;}
        @AuraEnabled public String iconimageurl         {get; set;}
        
        public MenuConfigurationFields (String key, 
                                        String label, 
                                        String fieldLabel, 
                                        String sortOrder,
                                        Boolean active,
                                        Boolean readOnly,
                                        String pageUrl,
                                        String sldsIconName,
                                        String iconimageurl
                                        ) {
            this.key                = key;
            this.label              = label;
            this.active             = active;
            this.fieldLabel         = fieldLabel;
            this.sortOrder          = sortOrder;
            this.readOnly           = readOnly;
            this.pageUrl            = pageUrl;
            this.sldsIconName       = sldsIconName;
            this.iconimageurl       = iconimageurl;
        }
    }
}