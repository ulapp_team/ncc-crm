@isTest
public class NavigatorMenuConfigurationControllerTest {
     @testSetup
    static void createData(){
        Account acc = TestUtility.createAccRec('TestAccount');
        insert acc;
        
        Contact testContact = NavigatorTestDataFactory.createContact('testing', 'testing@test.com', 'ABCD1234');
        testContact.Contact_Id__c = '9LIshQ4sJWP';
        insert testContact;
        
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        EmailTemplate eTemp = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
        system.runAs( new User(Id = UserInfo.getUserId())){
        	insert eTemp;
        }
        
        Compass_Setting__c setting = new Compass_Setting__c();
        setting.Name = 'Default Settings';
        setting.Email_Sender_Name__c = 'NAME';
        setting.Email_Template_Invitation_Id__c = eTemp.Id;
        setting.Email_Template_Registration_Id__c = eTemp.Id;
        setting.Email_Sender_Id__c = owea.Id;
        insert setting;
        
        Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
        testCampaign.Account__c = acc.Id;
        insert testCampaign;
        
        Journey__c testJourney = NavigatorTestDataFactory.createJourney('testJourney', testCampaign.Id, 'Approved');
        testJourney.End_Date__c = Date.valueOf(System.DateTime.now().addDays(5));
        insert testJourney;

        Event__c testEvent = NavigatorTestDataFactory.createEvent('testEvent', testCampaign.Id, '1234ABCD', System.today());
        testEvent.Time_Zone__c = 'CET';
        insert testEvent;

        Session__c testSession = NavigatorTestDataFactory.createSession('testSession', testEvent.Id, System.today());
        testSession.IsActive__c = TRUE;
        testSession.Time_Zone__c = 'CET';
        insert testSession;
        
        Session_Participant__c sessionParticipant = new Session_Participant__c();
        sessionParticipant.Event__c = testEvent.Id;
        sessionParticipant.Session__c = testSession.Id;
        sessionParticipant.Contact__c = testContact.Id;
        sessionParticipant.Status__c = 'Registered';
        insert sessionParticipant;
        
        Milestone__c milestone = new Milestone__c();
        milestone.Journey__c = testJourney.Id;
        milestone.Name = 'testMilestone';
        milestone.Type__c = 'Event';
        milestone.Related_RecordId__c = testContact.Id;
        insert milestone;
        
        Journey_Participant__c testParticipant = NavigatorTestDataFactory.createJourneyParticipant(testJourney.Id, testContact.Id);
        Insert testParticipant;
        
        Participant_Milestone__c participantMilestone = new Participant_Milestone__c();
        participantMilestone.Milestone__c = milestone.Id; 
        participantMilestone.Contact__c = testContact.Id;
        participantMilestone.Is_Active__c = TRUE;
        participantMilestone.Start_Date__c = System.Date.today().addDays(-2);
        participantMilestone.End_Date__c = System.Date.today().addDays(2);
        participantMilestone.Journey__c = testParticipant.Id;
        participantMilestone.Checkpoint__c = true;
        insert participantMilestone;
    }
    
    @isTest static void testGetMenuConfigurationFields() {
        Navigator__c navigator = new Navigator__c();
        navigator.Name = 'TestNavigator';
        navigator.Change_Agent_Lead_Label__c = 'Test';
        navigator.About_Label__c = 'TestAbout';
        navigator.About_Image_Video_Link__c = 'salesforce.com';
        insert navigator;
        
        Test.startTest();
        NavigatorMenuConfigurationController.getMenuConfigurationFields(String.valueOf(navigator.Id));
        Test.stopTest();
    }
    
    @isTest static void testSaveMenuConfigurationFields() {
        Navigator__c navigator = new Navigator__c();
        navigator.Name = 'TestNavigator';
        navigator.Change_Agent_Lead_Label__c = 'Test';
        navigator.About_Label__c = 'TestAbout';
        navigator.About_Image_Video_Link__c = 'salesforce.com';
        insert navigator;
        
        String jsonToUpdate = '{"My_Navigator":{"active":true,"fieldLabel":"My Navigators","sortOrder":"1"},"My_Events":{"active":true,"fieldLabel":"My Eventsz","sortOrder":"3"},"My_Journey":{"active":false,"fieldLabel":"My Journeys","sortOrder":"2"}}';
        
        Test.startTest();
        NavigatorMenuConfigurationController.saveMenuConfigurationFields(String.valueOf(navigator.Id),jsonToUpdate);
        Test.stopTest();
    }
    
}