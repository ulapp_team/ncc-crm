/*******************************************************************************
 * @author       Angelo Rivera
 * @date         23.07.2021
 * @description  Navigator Journey Controller test class
 * @group          Test Class
 * @revision     23.07.2021 - APRivera - Created
 *******************************************************************************/

@IsTest
private class NavigatorJourneyController_Test {

    @TestSetup
    static void createData(){
        Account acc = TestUtility.createAccRec('TestAccount');
        insert acc;
        
        Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = 'Test Campaign Name';
        testCampaign.Account__c = acc.Id;
        insert testCampaign;
        
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        EmailTemplate eTemp = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
        system.runAs( new User(Id = UserInfo.getUserId())){
        	insert eTemp;
        }
        
        Compass_Setting__c setting = new Compass_Setting__c();
        setting.Name = 'Default Settings';
        setting.Email_Sender_Name__c = 'NAME';
        setting.Email_Template_Invitation_Id__c = eTemp.Id;
        setting.Email_Template_Registration_Id__c = eTemp.Id;
        setting.Email_Sender_Id__c = owea.Id;
        insert setting;
        
        Navigator__c navigator = new Navigator__c();
        navigator.Name = 'TestNavigator';
        navigator.Change_Agent_Lead_Label__c = 'Test';
        navigator.About_Label__c = 'TestAbout';
        navigator.About_Image_Video_Link__c = 'salesforce.com';
        navigator.Campaign__c = testCampaign.Id;
        insert navigator;
        
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        testContact.Email = 'test@email.com';
        testContact.Contact_Id__c = ' ABC1234';
        insert testContact;

        /*Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = 'Test Campaign Name';
        testCampaign.Account__c = acc.Id;
        insert testCampaign;*/

        Journey__c testJourney = new Journey__c();
        testJourney.Name = 'Test Journey';
        testJourney.Campaign__c = testCampaign.Id;
        testJourney.Status__c = 'Approved';
        insert testJourney;

        Journey_Participant__c testParticipant = new Journey_Participant__c();
        testParticipant.Journey__c = testJourney.Id;
        testParticipant.Contact__c = testContact.Id;
        insert testParticipant;
    }


    @IsTest
    static void testJourneyController() {
		List<Navigator__c> basecamp = [SELECT Id, Name FROM Navigator__c];
        String contactId = [SELECT Id, Contact_Id__c FROM Contact LIMIT 1].Contact_Id__c;
        NavigatorJourneyController.ContactJourney contactJourney = new NavigatorJourneyController.ContactJourney();

        Test.startTest();
        contactJourney = NavigatorJourneyController.getContactJourney(contactId, basecamp[0].Id);
        System.assertEquals(contactJourney.lstJourney.size(), 1);
        Test.stopTest();


    }
}