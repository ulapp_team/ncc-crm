/**
 * @description Service Class for LightningFileUploadComponent
 * @ - lightning:fileUpload success functionality in Journey Participant Page
 *           05/03/2021 - Kyzer Buhay - Initial Creation
 **/
public with sharing class LightningFileUploadHandler {
  private static final String DEFAULT_PAGE_NAME = 'Compass';

  public class ContactProfileWrapper {
    @AuraEnabled
    public String profileURL { get; set; }
    @AuraEnabled
    public Boolean hasErrors { get; set; }
    @AuraEnabled
    public String errorMessage { get; set; }
  }

  public class ContentDetailsWrapper {
    public String fileType { get; set; }
    public String contentDocumentId { get; set; }
  }

  @AuraEnabled
  public static ContactProfileWrapper updatePicturePath(String attachToRecId, String contentVersionRecId, String sitePageName) {
    ContactProfileWrapper cpWrapper = new ContactProfileWrapper();
    cpWrapper.hasErrors = false;
    cpWrapper.profileURL = '';
    try {
      //if (FlsUtils.isAccessible('Contact', new List<String>{ 'Profile_Picture_URL__c' })) {
        Contact con = new ElevatedContext().queryContactWithoutSharing(attachToRecId);
        String sitePage = !String.isBlank(sitePageName) ? sitePageName : DEFAULT_PAGE_NAME;
        ContentDetailsWrapper contentDetailReturn = new ElevatedContext().updateContentDocumentParent(contentVersionRecId, con.Id, con.Name);
        String publicProfilePicURL ='/'+ sitePage + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_' + contentDetailReturn.fileType +
          '&versionId=' +contentVersionRecId +'&operationContext=CHATTER';
          
        System.debug('------------------------- ' +publicProfilePicURL);

        con.Profile_Picture_URL__c = publicProfilePicURL;

        new ElevatedContext().updateContactWithoutFLS(con);

        cpWrapper.profileURL = publicProfilePicURL;
      //}
    } catch (Exception e) {
      cpWrapper.hasErrors = true;
      cpWrapper.errorMessage = 'Error: ' + e.getMessage() + ' Stack trace: ' + e.getStackTraceString() ;
      System.debug( LoggingLevel.DEBUG,'!@# ERROR IN INSERT: ' + e.getMessage());
    }
    return cpWrapper;
  }

  @AuraEnabled
  public static Boolean removeAndDeleteImageURL(String participantContactId,String profileImageURL) {
    return new ElevatedContext().removeAndDeleteImageURLProcess(participantContactId, profileImageURL);
  }

  without sharing class ElevatedContext {
    public Contact queryContactWithoutSharing(String contactId) {
      return [SELECT Id, Name, Profile_Picture_URL__c FROM Contact WHERE Id = :contactId LIMIT 1];
    }

    public ContentDetailsWrapper updateContentDocumentParent( Id contentVersionId, Id parentRecordId, String contactName) {
      ContentDetailsWrapper contentDetail = new ContentDetailsWrapper();

      ContentVersion contentVersionRecord = [SELECT Id, ContentBodyId, FileType, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionId];
      ContentDocument conDoc = [SELECT Id, ParentId, Title, FileType  FROM ContentDocument WHERE Id = :contentVersionRecord.ContentDocumentId  LIMIT 1 ];
      contentDetail.contentDocumentId = conDoc.Id;
      contentDetail.fileType = conDoc.FileType;

      ContentDocumentLink cdLink = new ContentDocumentLink();
      cdLink.ContentDocumentId = conDoc.Id;
      cdLink.LinkedEntityId = parentRecordId;
      insert cdLink;
      system.debug('----------------------Hey i got here');
      system.debug('----------------------cdLink ' + cdLink);
      system.debug('----------------------contentDetail ' + contentDetail);

      
      if (!Test.isRunningTest()) { 
        ContentDocumentLink cdLinkGuest = new ContentDocumentLink();
        cdLinkGuest.ContentDocumentId = conDoc.Id;
        cdLinkGuest.LinkedEntityId = UserInfo.getUserId();
        insert cdLinkGuest;
      }
     
      return contentDetail;
    }

    public Boolean removeAndDeleteImageURLProcess( String participantContactId, String profileImageURL ) {
      Boolean isSuccess = true;
      try {
        if (!String.isBlank(profileImageURL) && profileImageURL.containsIgnoreCase('/Compass/sfc')) {
          List<String> profileImageParam = profileImageURL.split('&');
          String versionIdParam = profileImageParam.size() > 2 ? profileImageParam[1] : '';
          List<String> versionId = versionIdParam.split('=');

          ContentVersion cv = [ SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :versionId[1]  LIMIT 1 ];
          ContentDocument cd = [ SELECT Id FROM ContentDocument WHERE Id = :cv.ContentDocumentId  LIMIT 1 ];
          if (cd != null) {
            delete cd;
          }
        }

        Contact con = [ SELECT Id, Profile_Picture_URL__c FROM Contact WHERE Id = :participantContactId LIMIT 1 ];
        if (con != null) {
          con.Profile_Picture_URL__c = '';
          update con;
        }
      } catch (Exception e) {
        isSuccess = false;
        System.debug( LoggingLevel.DEBUG, '!@# ERROR IN INSERT: ' + e.getMessage() );
      }

      return isSuccess;
    }

    public Contact updateContactWithoutFLS(Contact con) {
      update con;
      return con;
    }
  }
}