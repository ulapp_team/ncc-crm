public without sharing class BasecampCalendarController {
    
    /******************************************************************************    
    * Changes (version)
    *            No.  Date(dd-mm-yyy) Author            Description
    *           ----  ---------   --------------------  -----------------------------
    * @version   1.0  ??           ??                   Initial version.
    * @version   2.0  05-10-2022   RuthLugpatan         [CCN-2164] updated upsertEvents to fix the DST issue
    ******************************************************************************/
    
    @AuraEnabled
    public static String getRecId(String eventId){
    	Event__c evt = [Select Id, Event_Id__c FROM Event__c WHERE Event_Id__c =:eventId];
        String recordId = evt.Id;
        //added by DinoBrinas 12032021
        System.debug(' @@@@@ eventId >>> ' + eventId);
        System.debug(' @@@@@ evt >>> ' + evt);
        System.debug(' @@@@@ recordId >>> ' + recordId);

        return recordId;
    }

    @AuraEnabled
    public static List<EventObj> getEvents(String navigatorId, String contactId){
           
           List<sObject> sObjectList = new List<sObject>();
           
           Navigator__c nav = [SELECT Id, Campaign__c FROM Navigator__c WHERE Id = : navigatorId];
           
           List<Session_Participant__c> sessionParticipantList =  [SELECT 	Id, 
                                                    Name, 
                                                    Event__c, 
                                                    Event__r.Id, 
                                                    Event__r.Name, 
                                                    Event__r.Event_Id__c,
                                                    Status__c,
                                                    Session__c,
                                                    Session__r.Propose_Start_Date_Time__c,
                                                    Session__r.Propose_End_Date_Time__c,
                                                    Session__r.Id, 
                                                    Session__r.Name, 
                                                    Session__r.Status__c,
                                                    Session__r.CreatedById, 
                                                    Session__r.Start_Date_Time__c, 
                                                    Session__r.End_Date_Time__c,  
                                                    Session__r.Location__c,
                                                    Session__r.User_Time_Zone__c,
                                                    Session__r.Time_Zone__c,
                                                    Session__r.Event__r.Name, 
                                                    Session__r.Event__r.Time_Zone__c,
                                                    Session__r.Session_Details__c
                                            FROM Session_Participant__c 
                                            WHERE Contact__r.Contact_Id__c =: contactId 
                                            AND Status__c in ('Registered','Confirmed', 'Attended', 'Catchup')
                                            AND Session__r.IsActive__c = TRUE
                                            //AND Session__r.End_Date_Time__c >= NEXT_MONTH 
                                            //AND CALENDAR_MONTH(Session__r.Start_Date_Time__c) =: month 
                                            //AND CALENDAR_YEAR(Session__r.Start_Date_Time__c) =: year
                                            AND (Session__r.Campaign__c = : nav.Campaign__c
                                                 OR Session__r.Event__r.Campaign__c = : nav.Campaign__c)
                                            ORDER BY Session__r.Start_Date_Time__c ASC];
           
           List<Participant__c> participantList = [SELECT 
                                                        Id, 
                                                        Event_Name__c,
                                                        Status__c,
                                                        Event__c, 
                                                        Event__r.Id,
                                                        Event__r.Name,
                                                   		Event__r.Status__c,
                                                   		Event__r.Event_Details__c,
                                                   		Event__r.CreatedById,
                                                   		Event__r.Location__c,
                                                   		Event__r.Time_Zone__c,
                                                   		Event__r.Start_Date_Time__c,
                                                   		Event__r.End_Date_Time__c
                                                    FROM Participant__c 
                                                    WHERE Member_Contact__r.Contact_Id__c = :contactId
                                                    AND Status__c IN ('', 'Invited', 'Registered', 'Confirmed', 'Attended', 'Catch-Up')
                                                    AND Event__r.Campaign__c = : nav.Campaign__c];
           
        
           system.debug(sessionParticipantList);
        
           system.debug(participantList);
        
           // Start populated EventRecords to Return
           
           List<EventObj> eventRecords = new List<EventObj>();
           
           for (Participant__c participant : participantList) {
               String statusStr = '';
               String strStart = '';
               String strEnd = '';
               
               if(participant.Event__r.Status__c != null){
                   statusStr = participant.Status__c;
               }
               
               DateTime strtDteTime = DateTime.Now();
               DateTime strtDteTimeFormatted = DateTime.Now();
               DateTime endDteTime = DateTime.Now();    
               DateTime endDteTimeFormatted = DateTime.Now(); 
               
               strtDteTime = participant.Event__r.Start_Date_Time__c;
               endDteTime = participant.Event__r.End_Date_Time__c;
               
               EventObj newEv = new EventObj(String.valueOF(participant.Event__r.Id),
                                             participant.Event_Name__c,
                                             strtDteTime,
                                             endDteTime,
                                             participant.Event__r.Event_Details__c,
                                             String.valueOf(participant.Event__r.CreatedById),
                                             statusStr,
                                             participant.Event__r.Location__c,
                                             participant.Event__r.Time_Zone__c,
                                             participant.Event__r.Time_Zone__c,
                                             participant.Event__r.Name, 
                                             participant.Event__r.Time_Zone__c
                                            );
               eventRecords.add(newEv);
           }
           
           for (Session_Participant__c sessionParticipant : sessionParticipantList) {
               String statusStr = '';
               String strStart = '';
               String strEnd = '';
               
               if(sessionParticipant.Status__c != null){
                   statusStr = (sessionParticipant.Status__c == 'Catchup') ? 'Catch-Up' : sessionParticipant.Status__c;
               }
               
               DateTime strtDteTime = DateTime.Now();
               DateTime strtDteTimeFormatted = DateTime.Now();
               DateTime endDteTime = DateTime.Now();    
               DateTime endDteTimeFormatted = DateTime.Now();       
               /*if(sessionParticipant.Status__c == 'Proposed' || sessionParticipant.Status__c == 'Rejected'){
                   strtDteTime = sessionParticipant.Session__r.Propose_Start_Date_Time__c;
                   endDteTime = sessionParticipant.Session__r.Propose_End_Date_Time__c;
                   
               }else{*/
                   strtDteTime = sessionParticipant.Session__r.Start_Date_Time__c;
                   endDteTime = sessionParticipant.Session__r.End_Date_Time__c;
               //}
               
               EventObj newEv = new EventObj(String.valueOF(sessionParticipant.Session__r.Id),
                                             sessionParticipant.Session__r.Name,
                                             strtDteTime,
                                             endDteTime,
                                             sessionParticipant.Session__r.Session_Details__c,
                                             String.valueOf(sessionParticipant.Session__r.CreatedById),
                                             statusStr,
                                             sessionParticipant.Session__r.Location__c,
                                             sessionParticipant.Session__r.User_Time_Zone__c,
                                             sessionParticipant.Session__r.Time_Zone__c,
                                             sessionParticipant.Session__r.Event__r.Name, 
                                             sessionParticipant.Session__r.Event__r.Time_Zone__c
                                            );
               eventRecords.add(newEv);
           }
           
           System.Debug('eventRecords: '+eventRecords);
           return eventRecords;
    }
	
    //Added for [CCN-1148] Feb042022
    public class CustomException extends Exception{}
    
    
    /******************************************************************************    
    * Changes (version)
    *            No.  Date(dd-mm-yyy) Author            Description
    *           ----  ---------   --------------------  -----------------------------
    * @version   1.0  25-10-2022   Von Pernicia         [CNN-2192] to compute offset for DST specific for America/New_York
    ******************************************************************************/
    public static Integer getOffset(String dateString) {
        
        Timezone tz = Timezone.getTimeZone(System.Label.Reference_Timezone);
        DateTime dateTimeValue = DateTime.valueOf(dateString);
        Integer timeZoneOffSet = tz.getOffset(dateTimeValue);
        Integer offset = 0;
        if (timeZoneOffSet == -18000000) {
            offset = 5;
        } else {
            offset = 4;
        }
        
        return offset;
    }

    /******************************************************************************    
    * Changes (version)
    *            No.  Date(dd-mm-yyy) Author            Description
    *           ----  ---------   --------------------  -----------------------------
    * @version   1.0  22-06-2023   Gian Bata         [CCN-EVE-3113-DV] get the event object's timezone
    *******************************************************************************/
    @AuraEnabled
    public static String getEventTimeZone(String eventId) {
        Event__c eventRecord = [SELECT Time_Zone__c FROM Event__c WHERE Event_Id__c = :eventId];
        
        return eventRecord.Time_Zone__c;
    }
    
    public class EventObj {
        @AuraEnabled
        public String Id {get;set;}
        @AuraEnabled
        public String title {get;set;}
        @AuraEnabled
        public DateTime startDateTime {get;set;}
        @AuraEnabled
        public DateTime endDateTime {get;set;}
        @AuraEnabled
        public String description {get;set;}
        @AuraEnabled
        public String owner {get;set;}
        @AuraEnabled
        public String status {get;set;}
        @AuraEnabled
        public String location {get;set;}
        @AuraEnabled
        public String timezone {get;set;}
        @AuraEnabled
        public String timezoneAbbr {get;set;}
        @AuraEnabled
        public String eventName {get;set;}
        @AuraEnabled
        public String commonTimezone {get;set;} // Start/End CNN-2192 25-10-2022 Xen
        @AuraEnabled
        public String eventTimeZone {get;set;}

        
        public EventObj(String i,String t, DateTime s, DateTime e, String d, String o, String st, String l, String tz, String tzAbbr, String en, String etz){
            this.Id = i;
            this.title = t;
            this.startDateTime = s;
            this.endDateTime = e;
            this.description = d;
            this.owner = o;
            this.status = st;
            this.location = l;
            this.timezone = tz;
            this.timezoneAbbr = tzAbbr;
            this.eventName = en;
            this.commonTimezone = System.Label.Reference_Timezone; // Start/End CNN-2192 25-10-2022 Xen
        }
        
    }
}