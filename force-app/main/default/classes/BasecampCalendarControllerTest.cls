@isTest
public class BasecampCalendarControllerTest {
    
    @testSetup 
    static void createData(){
        Account acc = TestUtility.createAccRec('TestAccount');
        insert acc;
        
        Contact testContact = NavigatorTestDataFactory.createContact('testing', 'testing@test.com', 'ABCD1234');
        testContact.Contact_Id__c = '9LIshQ4sJWP';
        insert testContact;
        
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        EmailTemplate eTemp = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
        /*system.runAs( new User(Id = UserInfo.getUserId())){
insert eTemp;
}*/
        
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/New_York', UserName='testUserNavigator@testorg.com');
        
        system.runAs(u){
            insert eTemp;
            Compass_Setting__c setting = new Compass_Setting__c();
            setting.Name = 'Default Settings';
            setting.Email_Sender_Name__c = 'NAME';
            setting.Email_Template_Invitation_Id__c = eTemp.Id;
            setting.Email_Template_Registration_Id__c = eTemp.Id;
            setting.Email_Sender_Id__c = owea.Id;
            insert setting;
            
            Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
            testCampaign.Account__c = acc.Id;
            insert testCampaign;
            
            Journey__c testJourney = NavigatorTestDataFactory.createJourney('testJourney', testCampaign.Id, 'Approved');
            testJourney.End_Date__c = Date.valueOf(System.DateTime.now().addDays(5));
            insert testJourney;
            
            Event__c testEvent = NavigatorTestDataFactory.createEvent('testEvent', testCampaign.Id, '1234ABCD', System.today());
            testEvent.Time_Zone__c = 'CET';
            insert testEvent;
            
            Session__c testSession = NavigatorTestDataFactory.createSession('testSession', testEvent.Id, System.today());
            testSession.IsActive__c = TRUE;
            testSession.End_Date_Time__c = System.now().addHours(1);
            testSession.Time_Zone__c = 'CET';
            insert testSession;
            
            Session_Participant__c sessionParticipant = new Session_Participant__c();
            sessionParticipant.Event__c = testEvent.Id;
            sessionParticipant.Session__c = testSession.Id;
            sessionParticipant.Contact__c = testContact.Id;
            sessionParticipant.Status__c = 'Registered';
            insert sessionParticipant;
            
            Milestone__c milestone = new Milestone__c();
            milestone.Journey__c = testJourney.Id;
            milestone.Name = 'testMilestone';
            milestone.Type__c = 'Event';
            milestone.Related_RecordId__c = testContact.Id;
            insert milestone;
            
            
            Participant__c participantData = new Participant__c();
            participantData.Event__c = testEvent.Id;
            participantData.Member_Contact__c = testContact.Id;
            participantData.Session_Modified__c = false;
            participantData.Status__c = '';
            insert participantData;
            
            Navigator__c navigator = new Navigator__c();
            navigator.Name = 'TestNavigator';
            navigator.Campaign__c = testCampaign.Id;
            navigator.Change_Agent_Lead_Label__c = 'Test';
            navigator.About_Label__c = 'TestAbout';
            navigator.About_Image_Video_Link__c = 'salesforce.com';
            insert navigator;
        }
    }
    
    static testMethod void getEventsTest() {
        
        String query = DynamicSOQL('Navigator__c');
        query += ' WHERE name = \'TestNavigator\' LIMIT 1';
        
        Navigator__c navigator = Database.query(query);
        
        List<BasecampCalendarController.EventObj> returnedRecords = BasecampCalendarController.getEvents(navigator.Id, '9LIshQ4sJWP');
        
        System.assert(returnedRecords != null);
    }
    static TestMethod void getRecIdTest(){
        Session__c session1 = [SELECT Id, Event__c, Event__r.Event_Id__c FROM Session__c LIMIT 1];
        String eventId = BasecampCalendarController.getRecId(session1.Event__r.Event_Id__c);
        System.assertEquals(eventId, session1.Event__c);
    }
    
    static TestMethod void getOffSet(){
        String dateString = String.ValueOf(System.now());
        Integer offSet = BasecampCalendarController.getOffset(dateString);
        System.assert(offSet != null);
    }
    
    
    public static String DynamicSOQL(String objectName) 
    {

        String SobjectApiName = objectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();

        List<String>apiNames =  new list<String>();

        for(String apiName : fieldMap.keyset())
        {
            apiNames.add(apiName);
        }

        string allstring = string.join(apiNames,',');

        String query = 'SELECT ' + allstring + ' FROM ' + SobjectApiName;

        return query;
    }
}