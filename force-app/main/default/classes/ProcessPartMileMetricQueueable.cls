/*******************************************************************************
     * @author       Von Pernicia
     * @date         01/01/2022
     * @description  queueable to compute parent milestone
     * @return       strResult
     * @revision     01/01/2022 - [CCN-2429] Von Pernicia: created new queueable to compute parent milestone 
     *******************************************************************************/
public class ProcessPartMileMetricQueueable implements Queueable {

    public String finalJourneyId;
    public String finalContactId; 

    public ProcessPartMileMetricQueueable(String finalJourneyId, String finalContactId) {
        this.finalContactId = finalContactId; 
        this.finalJourneyId = finalJourneyId;
     }

     public void execute(QueueableContext qc) {
        ProcessParticipantMilestonesMetricBatch batchObj = new ProcessParticipantMilestonesMetricBatch();
        batchObj.finalContactId = finalContactId; 
        batchObj.finalJourneyId = finalJourneyId;
        Id batchProcessId = Database.executeBatch(batchObj,2000);
    }
}