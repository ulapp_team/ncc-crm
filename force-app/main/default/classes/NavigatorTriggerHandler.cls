/*******************************************************************************************
* @name: NavigatorTriggerHandler
* @author: JaysonLabnao
* @created: 15-02-2022
* @description: Trigger handler for Navigator Object. Setting up necessary navigator fields.
*
* Changes (version)
* -------------------------------------------------------------------------------------------
*            No.   Date(dd-mm-yyy)  Author                Description
*            ----  ---------        --------------------  -----------------------------
* @version   1.0   15-02-2022       Jayson Labnao         [CCN-1146] Initial version.
*********************************************************************************************/
public with sharing class NavigatorTriggerHandler {

    /*******************************************************************************************
    * @name: updateLoginPageUrl
    * @author: JaysonLabnao
    * @created: 15-02-2022
    * @description: Updates the login page URL field for the Navigator Record. Used @future in
    *               after insert context to ensure that the URL is populated with the Id of the
    *               Navigator Record.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   15-02-2022       Jayson Labnao         [CCN-1146] Initial version.
    *********************************************************************************************/
    @future
    public static void updateLoginPageUrl(Set<Id> navigatorIds){
        String navigatorCommunityUrl = getCommunityURL('Navigator');

        if(String.isBlank(navigatorCommunityUrl)){
            System.debug('Navigator Community URL does not exist.');
            return; 
        }

        Map<Id, Navigator__c> navigatorMap = new Map<Id, Navigator__c>([
            SELECT Id, Login_Page_URL__c
            FROM Navigator__c
            WHERE Id IN :navigatorIds
        ]);

        List<Navigator__c> navigatorUpdateList = new List<Navigator__c>();
        for(Navigator__c navi : navigatorMap.values()){
            if(String.isBlank(navi.Login_Page_URL__c)){
                navi.Login_Page_URL__c = navigatorCommunityUrl + '/s/navigator-login?id=' + navi.Id;
                navigatorUpdateList.add(navi);
            }
        }

        Update navigatorUpdateList;

    }

    /*******************************************************************************************
    * @name: getCommunityURL
    * @author: JaysonLabnao
    * @created: 15-02-2022
    * @description: Helper method to get the Navigator Site/Community base URL.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   15-02-2022       Jayson Labnao         [CCN-1146] Initial version.
    *********************************************************************************************/
    public static String getCommunityURL(string communityName){
        try{
            Site site = [SELECT Id FROM Site WHERE UrlPathPrefix = :communityName WITH SECURITY_ENFORCED LIMIT 1 ];
            String communityUrl = [SELECT SecureURL FROM SiteDetail WHERE DurableId = :site.Id WITH SECURITY_ENFORCED].SecureUrl;
            return communityUrl;
        }
        catch(Exception e){
            System.debug('TRY-CATCH-Exception-->' + e);
            return '';
        }
    }

}