@isTest
private class ContactTriggerHandler_Test {
    @testSetup
    static void setup(){
        Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
        Insert testCampaign;

        Navigator__c testNavigator = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
        Insert testNavigator;

        Contact testContact = NavigatorTestDataFactory.createContact('ExistingContact', 'ExistingContact@test.com', null);
        testContact.Navigator__c = testNavigator.Id;
        Insert testContact;
    }
    
    @isTest static void setUpNavigatorFields_insert_test(){
        Navigator__c navigatorRecord = [SELECT Id FROM Navigator__c WHERE Name = 'testNavigator'];
        List<Contact> newContacts = NavigatorTestDataFactory.createMultipleContact('testContact', 3);
        for(Contact con : newContacts){
            con.Navigator__c = navigatorRecord.Id;
        }

        Test.startTest();
        Insert newContacts;
        List<Contact> insertedContacts = [SELECT Id, Name, Navigator__c, Navigator_Page_URL__c, Login_Token__c FROM Contact WHERE Id IN :newContacts];
        Test.stopTest();

        System.assert(insertedContacts[0].Navigator_Page_URL__c != null, 'Navigator_Page_URL__c was not updated in the contact record');
        System.assert(insertedContacts[0].Login_Token__c != null, 'Login_Token__c was not updated in the contact record');
        System.assert(insertedContacts[0].Navigator_Page_URL__c != insertedContacts[1].Navigator_Page_URL__c);
    }

    @isTest static void setUpNavigatorFields_update_test(){
        Contact existingCon = [SELECT Id, Name, Navigator_Page_URL__c, Contact_Id__c, Login_Token__c FROM Contact WHERE Name = 'ExistingContact' LIMIT 1];
        existingCon.Contact_Id__c = null;
        existingCon.Login_Token__c = null;
        String prevNavUrl = existingCon.Navigator_Page_URL__c;

        Test.startTest();
        Update existingCon;
        contact updatedCon = [SELECT Id, Name, Navigator__c, Navigator_Page_URL__c, Login_Token__c FROM Contact WHERE Id = :existingCon.Id];
        Test.stopTest();

        System.assert(updatedCon.Navigator_Page_URL__c != prevNavUrl, 'Navigator_Page_URL__c was not updated in the contact record');
        System.assert(updatedCon.Login_Token__c != null, 'Navigator_Page_URL__c was not updated in the contact record');
    }

    @isTest static void generateLoginToken_test(){
        String testToken = ContactTriggerHandler.generateLoginToken();
        System.assert(!String.isBlank(testToken), 'Token was not generated');
    }
    
    @isTest static void getCommunityURL_test(){
        String testUrl = ContactTriggerHandler.getCommunityURL('test');
        System.assertEquals('unit-test-url', testUrl);
    }

}