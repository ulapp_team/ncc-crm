public class ParticipantTriggerUtility {
    
    public static void sendEmailToParticipant(Map<String, List<Participant__c>> participantMap, Set<Id> eventIds){
        Map<Id, Event__c> eventsMap = new Map<Id, Event__c>([SELECT Id, Email_Sender_Id__c, Email_Sender_Name__c, Email_Message_CC__c, Email_Message_BCC__c, Do_Not_Send_Event_Confirmation_email__c FROM Event__c WHERE Id =: eventIds]);
        Map<Id, OrgWideEmailAddress> owea = new Map<Id, OrgWideEmailAddress>([SELECT Id FROM OrgWideEmailAddress]);
        Compass_Setting__c compS = [SELECT Id, Name, Email_Template_Invitation_Id__c, Email_Template_Registration_Id__c 
                                    FROM Compass_Setting__c WHERE Name = 'Default Settings' AND Email_Template_Invitation_Id__c != null AND Email_Template_Registration_Id__c != null LIMIT 1]; 
                                    
        if(participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_INVITED)){
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>(); 
            for(Participant__c participant: participantMap.get(UtilityConstant.PARTICIPANT_STATUS_INVITED)){
                if(!(eventsMap.get(participant.Event__c).Do_Not_Send_Event_Confirmation_email__c) && participant != null){
                    String emailSenderId = eventsMap.get(participant.Event__c).Email_Sender_Id__c;
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    message.setTargetObjectId(participant.Member_Contact__c);
                    system.debug('eventsMap: ' + eventsMap);
                    system.debug('OWEA: ' + owea);
                    message.setOrgWideEmailAddressId(owea.get(emailSenderId).Id);
                    message.setSaveAsActivity(true); //CCN-1139 Jan 24, 2022 Xen Reyes
                    message.setWhatId(participant.Id);   
                    message.setTemplateId(compS.Email_Template_Invitation_Id__c);
                    if(eventsMap.get(participant.Event__c).Email_Message_CC__c != null)  message.setCcAddresses(new List<String> {eventsMap.get(participant.Event__c).Email_Message_CC__c});
                    if(eventsMap.get(participant.Event__c).Email_Message_BCC__c != null) message.setBccAddresses(new List<String> {eventsMap.get(participant.Event__c).Email_Message_BCC__c});
                    
                    emails.add(message);
                }
            }
            if(!Test.isRunningTest()){                       
                Messaging.sendEmail(emails);
            }
        }

        if(participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_REGISTERED) || participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_IN_PROGRESS)){
            system.debug('REGISTERED!!!!! ');
            Id eventRegistrationTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: UtilityConstant.EMAIL_TEMPLATE_REGISTRATION_NAME].Id;
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>(); 
            
            //Combine Registered & In Progress Participants - XR Mar 22, 2022
            List<Participant__c> regInProgParticipantsList = new List<Participant__c>();
            if(participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_REGISTERED)) regInProgParticipantsList.addAll(participantMap.get(UtilityConstant.PARTICIPANT_STATUS_REGISTERED));
            if(participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_IN_PROGRESS)) regInProgParticipantsList.addAll(participantMap.get(UtilityConstant.PARTICIPANT_STATUS_IN_PROGRESS));
            
            for(Participant__c participant: regInProgParticipantsList){
                if(!(eventsMap.get(participant.Event__c).Do_Not_Send_Event_Confirmation_email__c) && participant != null){
                    String emailTemplateId = compS.Email_Template_Registration_Id__c != null ? compS.Email_Template_Registration_Id__c : eventRegistrationTemplate;
                    String emailSenderId = eventsMap.get(participant.Event__c).Email_Sender_Id__c;
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    message.setOrgWideEmailAddressId(eventsMap.get(participant.Event__c).Email_Sender_Id__c);
                    message.setSaveAsActivity(true); //CCN-1139 Jan 24, 2022 Xen Reyes
                    message.setWhatId(participant.Id);
                    message.setTemplateId(emailTemplateId);
                    message.setTargetObjectId(participant.Member_Contact__c);
                    emails.add(message);
                }
            }
            if(!Test.isRunningTest()){
                Messaging.sendEmail(emails);
            }
        }
        
        // Updated by DinoBrinas [1153] Feb012022
        // @description: Code is calling Batchjob to send the email
        /* if(participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_REGISTERED)){
            Id eventRegistrationTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: UtilityConstant.EMAIL_TEMPLATE_REGISTRATION_NAME].Id;
            Set<Id> setParticipantIds = new Set<Id>();
            BatchParticipantSendEmail batchobj = new BatchParticipantSendEmail();
            for(Participant__c participantRec: participantMap.get(UtilityConstant.PARTICIPANT_STATUS_REGISTERED)){
                setParticipantIds.add(participantRec.Id);
            }
            
            String queryString = 'SELECT Id, Name, Event__c, Member_Contact__c FROM Participant__c WHERE Id IN:setParticipantIds';
            
            System.debug(' @@@@@ queryString >>>> ' + queryString);
            System.debug(' @@@@@ eventsMap >>>> ' + eventsMap );
            System.debug(' @@@@@ owea >>>> ' + owea);
            System.debug(' @@@@@ compS >>>> ' + compS );
            System.debug(' @@@@@ eventRegistrationTemplate >>>> ' + eventRegistrationTemplate );
            System.debug(' @@@@@ setParticipantIdstemp >>>> ' + setParticipantIds );
            
            // Populating the Batch class
            batchobj.strQuery = queryString;
            batchobj.eventsMap = eventsMap;
            batchobj.owea = owea;
            batchobj.compS = compS;
            batchobj.eventRegistrationTemplate = eventRegistrationTemplate;
            batchobj.setParticipantIdstemp = setParticipantIds;
            Id batchProcessId = Database.executeBatch(batchObj,2);
        } */   
    }

    public static void convertEventDates(List<Participant__c> newList){
        String strTZone = String.ValueOf(userinfo.getTimeZone());
        Set<Id> participantIds;
        for(Participant__c pa: newList){
            participantIds.add(pa.Id);
        }
        
        Map<Id, Participant__c> participantMap = new Map<Id, Participant__c>([SELECT Id, Event__r.Start_Date_Time__c, Event__r.End_Date_Time__c, Event__r.Time_Zone__c FROM Participant__c WHERE Id IN: participantIds]);
        
        for(Participant__c part: newList){
            part.Event_Start_Date__c =  participantMap.get(part.Id).Event__r.Start_Date_Time__c.format('EEEE, MMMMM dd', strTZone);
            part.Event_Start_Time__c =  participantMap.get(part.Id).Event__r.Start_Date_Time__c.format('hh:mm', strTZone);
            part.Event_End_Date__c = participantMap.get(part.Id).Event__r.End_Date_Time__c.format('MMMMM dd, yyyy', strTZone);
            part.Event_End_Time__c = participantMap.get(part.Id).Event__r.End_Date_Time__c.format('hh:mm', strTZone);
        }
    }

    //CCN-1139 Jan 24, 2022 Xen Reyes
    public static void sendEmailBatch(Set<Id> pptIds){
        if(!Test.isRunningTest()){
        	Database.executeBatch(new ParticipantEmailCommunicationBatch(pptIds), 10);
        }
    }

}