/*******************************************************************************************
  * @name: EvidenceCaptureTriggerHandler
  * @author: 
  * @created: 21-06-2023
  * @description: Handler for all the logic of the trigger for the Evidence Capture object
  *
  * Changes (version)
  * -------------------------------------------------------------------------------------------
  *             No.   Date(dd-mm-yyy)   Author                Description
  *             ----  ---------------   --------------------  -----------------------------
  * @version    1.0   21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
  *********************************************************************************************/
  public with sharing class EvidenceCaptureTriggerHandler {
    /******************************************************************************    
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    public static void onAfterInsert(List<Evidence_capture__c> newEvidenceCaptureList){
        Set<Id> compassCaptureId = getCompassCaptureIds(newEvidenceCaptureList);
        calculatePercentCompletion(compassCaptureId);
    }

    /******************************************************************************    
    * Changes (version)
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    public static void onAfterUpdate(List<Evidence_capture__c> newEvidenceCaptureList, 
                                        Map<Id, Evidence_capture__c> oldEvidenceCaptureMap){
        Set<Id> compassCaptureId = new Set<Id>();
        for(Evidence_capture__c evidenceCap : newEvidenceCaptureList){
            if(evidenceCap.Weekly_Evaluation__c != oldEvidenceCaptureMap.get(evidenceCap.Id).Weekly_Evaluation__c){
                compassCaptureId.add(evidenceCap.Capture_ID__c);
            }
        }
        if(compassCaptureId.size() > 0){
            calculatePercentCompletion(compassCaptureId);
        }
    }

    /******************************************************************************    
    * Changes (version)
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    public static Set<Id> getCompassCaptureIds(List<Evidence_capture__c> newEvidenceCaptureList){
        Set<Id> compassCaptureId = new Set<Id>();
        for(Evidence_capture__c evidenceCap : newEvidenceCaptureList){
            compassCaptureId.add(evidenceCap.Capture_ID__c);
        }
        return compassCaptureId;
    }

    /******************************************************************************    
    * Changes (version)
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    public static void calculatePercentCompletion(Set<Id> compassCaptureId){
        if(System.isBatch() || System.isFuture() || System.isQueueable() || System.isScheduled()){
            calculatePercentCompletionSynchronous(compassCaptureId);
        }else{
            calculatePercentCompletionFuture(compassCaptureId);
        }
    }
      
    /******************************************************************************    
    * Changes (version)
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    @future
    public static void calculatePercentCompletionFuture(Set<Id> compassCaptureId){
        calculatePercentCompletionSynchronous(compassCaptureId);
    }
      
    /******************************************************************************    
    * Changes (version)
    *               No.   Date(dd-mm-yyy)   Author                Description
    *               ----  ---------------   --------------------  -----------------------------
    * @version    1.0     21-06-2023        Rianno Rizarri        [CCC-DCM-90-DV] For calculation of the percent completion of compass capture
    ******************************************************************************/
    public static void calculatePercentCompletionSynchronous(Set<Id> compassCaptureId){
          List < Compass_Capture__c > compassCaptureList = new List < Compass_Capture__c > ();
        compassCaptureList = [SELECT Id , (SELECT Weekly_Evaluation__c FROM Assessment_Capture__r)
                                FROM Compass_Capture__c WHERE ID IN: compassCaptureId];
        Map<Id, Decimal> compassCapWeeklyEvaluationTotal = new Map<Id, Decimal>();
        List < Compass_Capture__c > compassCapToUpdateList = new List < Compass_Capture__c > ();
        for (Compass_Capture__c compassCap : compassCaptureList) {
            Compass_Capture__c comCapRec = new Compass_Capture__c();
            
            Decimal totalWeeklyEvaluation = 0;
            for(Evidence_capture__c evidenceCap : compassCap.Assessment_Capture__r){
                totalWeeklyEvaluation += evidenceCap.Weekly_Evaluation__c;
            }
            totalWeeklyEvaluation = totalWeeklyEvaluation/compassCap.Assessment_Capture__r.size();
            
            comCapRec.Id = compassCap.Id;
            comCapRec.Percent_Completion__c = totalWeeklyEvaluation;
            compassCapToUpdateList.add(comCapRec);
        }
        Update compassCapToUpdateList;
    }
}