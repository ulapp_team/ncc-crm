@isTest
public without sharing class NavigatorFileUploadController_Test {

    @TestSetup
    static void setup(){
        
        Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
        Insert testCampaign;

        Navigator__c testNavigator = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
        Insert testNavigator;

        ContentVersion testContent = new ContentVersion(
            Title = 'Test Title',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        Insert testContent;


    }

    @isTest static void updateFieldUrl_Test_Positive(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];
        ContentDocument testDocument = [SELECT Id FROM ContentDocument LIMIT 1];

        test.startTest();
        String resultStr = NavigatorFileUploadController.updateFieldUrl(testDocument.Id, testNavigator.Id, 'Header_Image__c');
        test.stopTest();

        System.assertEquals(resultStr, 'Success', 'Field URL Update not Done');
    }

    @isTest static void updateFieldUrl_Test_Negative(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];

        test.startTest();
        String resultStr = NavigatorFileUploadController.updateFieldUrl('incorrectId', testNavigator.Id, 'Header_Image__c');
        test.stopTest();

        System.assert(resultStr.contains('Error'), 'Method should return an error since it has an incorrect parameter value.');
    }

    @isTest static void updateObjectFieldUrl_Test_Positive(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];
        ContentDocument testDocument = [SELECT Id FROM ContentDocument LIMIT 1];

        test.startTest();
        String resultStr = NavigatorFileUploadController.updateObjectFieldUrl(testDocument.Id, testNavigator.Id, 'Navigator__c', 'Header_Image__c');
        test.stopTest();

        System.assertEquals(resultStr, 'Success', 'Field URL Update not Done');
    }

    @isTest static void updateObjectFieldUrl_Test_Negative(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];

        test.startTest();
        String resultStr = NavigatorFileUploadController.updateObjectFieldUrl('incorrectId', testNavigator.Id, 'Navigator__c', 'Header_Image__c');
        test.stopTest();

        System.assert(resultStr.contains('Error'), 'Method should return an error since it has an incorrect parameter value.');
    }

    

    @isTest static void getOptions_Test(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];

        test.startTest();
        List<NavigatorFileUploadController.OptionWrapper> options = NavigatorFileUploadController.getOptions(testNavigator.Id);
        test.stopTest();


        System.assert(options.size() > 0, 'Method should return a list with elements.');
    }
}