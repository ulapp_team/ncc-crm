public with sharing class CommunityCampaignRaidController {
    private static WithoutSharingClass withoutShare = new WithoutSharingClass(); 
    
    @AuraEnabled
    public static string getNamespacePrefix(){
        String prefix = String.isNotBlank(FlsUtils.prefix) ? FlsUtils.prefix : '';
        return prefix;
    }

    @AuraEnabled 
    public static string getRaidDetails(String campaignId, String sortByValue){
        
       
        Map<String,Object> returnMap = new Map <String,Object>();
        
        try{  
            // This will pull information from Raid object to display in table    
            List<Raid__c> raidInfoList = new List<Raid__c>();
            if(FlsUtils.isAccessible('Raid__c', new List<String>{'Assigned_To__c',
                'Date_Closed__c',
                'Description__c',
                'Escalated_To__c',
                'Event__c',
                'Mitigation_Strategy__c',
                'Next_Steps__c',
                'Priority__c',
                'Raised_by__c',
                'Resolution_Answer__c',
                'Status__c',
                'Target_Resolution_Date__c',
                'Type__c',
                'Date_Raised__c',
                'Raised_By_Email__c',
                'Campaign__c'
                })) {
                raidInfoList = withoutShare.getRaid(campaignId, sortByValue);
                returnMap.put('raidInfoList', raidInfoList);
            }
       

            // This will pull event name and display in Campaign Raid dropdown
            List<Event__c> eventInfoList = new List<Event__c>();
            if(FlsUtils.isAccessible('Event__c', new List<String>{'Name'})) {

                eventInfoList = withoutShare.getEvent(campaignId);
                returnMap.put('eventInfoList', eventInfoList);
            }    

            // This will pull type picklist value from Raid object
            List<String> typeList= new List<String>();
            Schema.DescribeFieldResult fieldResult = Raid__c.Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

            for( Schema.PicklistEntry pickListVal : ple){
                typeList.add(pickListVal.getLabel());
            }

            returnMap.put('typeList',typeList);

        } catch(QueryException e){
            throw new AuraHandledException(e.getMessage());     
        }
        
        return JSON.serialize(returnMap);                                    
    }

    @AuraEnabled
    public static void createRaid(String campaignId, String eventId, String email, String description, String type){
        final String STATUS_NEW = 'New';

        RAID__c newRaid = new RAID__c(
            Campaign__c = campaignId,
            Event__c = eventId,
            Description__c = description,
            Status__c = STATUS_NEW,
            Type__c = type,
            Date_Raised__c = Date.today()
        );

        String contactId = checkIfContactExist(email);

        if(String.isNotBlank(contactId)){
            newRaid.Raised_by__c = contactId;
        }

        try{
            if(FlsUtils.isCreateable(newRaid, new List<String>{
                    'Campaign__c',
                    'Event__c',
                    'Date_Raised__c',
                    'Description__c',
                    'Status__c',
                    'Type__c'})){

                    withoutShare.createRaidRecord(newRaid);
            }
        } catch(DmlException e){
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static String checkIfContactExist(String email){
        List<Contact> contacts = new List<Contact>();
        if(FlsUtils.isAccessible('Contact', new List<String>{'Firstname','Lastname', 'Email'})){
            contacts = withoutShare.getContact(email);
        }

        return !contacts.isEmpty() ? String.valueOf(contacts[0].Id) : '';
    }
    

    //Needs inner class declared without sharing for guest users to retrieve relevant records
    private without sharing class WithoutSharingClass {
        //needs tro be inside without sharing because guest user does not have access to contact records (for Raised_By__c)
      
        public void createRaidRecord(RAID__c raid){
            insert raid;
        }

        public List<Contact> getContact(String email){
            return [SELECT Id, Firstname, Lastname, Email FROM Contact WHERE Email = :email LIMIT 1];
        }

        public List<Event__c> getEvent(String campaignId){
            String soqlQuery = 'SELECT Id, Name';
            soqlQuery += ' FROM Event__c';
            soqlQuery += ' WHERE Campaign__c=: campaignId AND IsActive__c=TRUE';
            soqlQuery += ' ORDER BY Name';
          
            return Database.query(soqlQuery);
        }

        
        public List<Raid__c> getRaid(String campaignId, String sortByValue){
            String soqlQuery = 'SELECT Id, Name,';
            soqlQuery += ' Assigned_To__c,';
            soqlQuery += ' Date_Closed__c,';
            soqlQuery += ' Description__c,';
            soqlQuery += ' Escalated_To__c,';
            soqlQuery += ' Event__r.Name,';
            soqlQuery += ' Mitigation_Strategy__c,';
            soqlQuery += ' Next_Steps__c,';
            soqlQuery += ' Priority__c,';
            soqlQuery += ' Raised_by__r.Name,';
            soqlQuery += ' Resolution_Answer__c,';
            soqlQuery += ' Status__c,';
            soqlQuery += ' Target_Resolution_Date__c,';
            soqlQuery += ' Type__c,';
            soqlQuery += ' Date_Raised__c,';
            soqlQuery += ' Raised_By_Email__c';
            soqlQuery += ' FROM Raid__c';
            soqlQuery += ' WHERE Campaign__c=: campaignId';
            if(sortByValue != null){
                if (sortByValue == 'Event_Name') sortByValue = 'Event__r.Name';
                soqlQuery += ' ORDER BY '+ sortByValue +' ASC';
            }
            else{
                soqlQuery += ' ORDER BY Name DESC ';
            }
            return Database.query(soqlQuery);
        }
    }

   
}