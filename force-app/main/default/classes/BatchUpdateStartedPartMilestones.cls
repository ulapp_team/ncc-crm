/*******************************************************************************
 * @author       Angelo Rivera
 * @date         07.07.2022
 * @description  CCN-JOU-1849-DV - Updates Started Milestone Flag
 * @revision     07.07.2022- APRivera - Created
 *******************************************************************************/

global class BatchUpdateStartedPartMilestones implements Database.Batchable<SObject>, Schedulable, Database.Stateful{

    public String strQuery = '';
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        //CCN-2359 XEN REYES 2.DEC.2022
        strQuery = 'SELECT Id, Name, Start_Date__c, Milestone_Started__c FROM Participant_Milestone__c ' +
                    'WHERE Start_Date__c <= TODAY ' +
                    'AND Milestone_Started__c = FALSE ' + 
                    'AND ((Parent_Milestone__c != null AND Parent_Milestone__r.Is_Active__c = true AND Parent_Milestone__r.RecordType.Name = \'Milestone\') OR Parent_Milestone__c = null)';

        System.debug(LoggingLevel.DEBUG, 'QUERY: ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        List<Participant_Milestone__c> lstMilestones = (List<Participant_Milestone__c>)scope;
        List<Participant_Milestone__c> lstMilestonesToUpdate = new List<Participant_Milestone__c>();

        if (!lstMilestones.isEmpty()) {
            for(Participant_Milestone__c pm : lstMilestones){
                pm.Milestone_Started__c = true;
                lstMilestonesToUpdate.add(pm);
            }

            //CCN-2359 XEN REYES 2.DEC.2022
            Database.SaveResult[] srList = Database.update(lstMilestonesToUpdate, false);
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred. ' + sr.getId());                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage() + ' ' + err.getFields());
                    }
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

    global void execute(SchedulableContext sc) {
        BatchUpdateStartedPartMilestones  batchObj = new  BatchUpdateStartedPartMilestones ();
        Id batchProcessId = Database.executeBatch(batchObj,100);
    }
}