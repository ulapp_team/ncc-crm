/******************************************************************************    
* ChangeEventTypeController - ChangeEventType Aura Controller
*            No.  Date(dd-mm-yyy) Author            Description
*           ----  ---------   --------------------  -----------------------------
* @version   1.0  09-14-2023   Rex David            [CCN-EVE-3056-DV] Virtual Meeting Integration (POC: Microsoft Teams Link)
******************************************************************************/
public class ChangeEventTypeController {

    private static WithoutSharingClass withoutShare = new WithoutSharingClass(); 

    @AuraEnabled
    public static String saveData(String eventId, String newType, String newPlatform, String optionAnswer){

        Event__c eventRecord = new Event__c (Id = eventId);
        eventRecord.Event_Type__c = newType;
        eventRecord.Virtual_Meeting_Platform__c = newPlatform;
        eventRecord.ChangeEventTypeAction__c = optionAnswer;
        withoutShare.updateEvent(eventRecord);

        return eventRecord.Id;
    }
    
    @AuraEnabled
    public static changeEventTypeData getData(String eventId){
        changeEventTypeData wrapData;
        try{
            Event__c eventRec = withoutShare.getEvent(eventId);
            Boolean hasSessions = (eventRec.CC_Sessions__r != NULL && eventRec.CC_Sessions__r.size() > 0) ? true : false;
            Boolean hasMSMeetingsSession = checkHasMSMeetingSession(eventId);
            wrapData = new changeEventTypeData(eventRec.Event_Type__c != null ? eventRec.Event_Type__c : '', eventRec.Virtual_Meeting_Platform__c != null ? eventRec.Virtual_Meeting_Platform__c : '', hasMSMeetingsSession, hasSessions);
        }
        catch(Exception e){
            throw new AuraHandledException('Something went wrong in getData method: '+ e.getMessage());    
        }
        return wrapData;
    }

    @AuraEnabled 
    public static Boolean checkHasMSMeetingSession(String eventId){
        Boolean hasMSMeetingSession = false;
        for(Session__c session : withoutShare.getSessionWithMSTeamsInfo(eventId)){
            if(session.Meeting_Info__c.containsIgnoreCase('Microsoft Teams meeting')){
                hasMSMeetingSession = true;
                break;
            }
        }
        return hasMSMeetingSession;
    }

    @AuraEnabled 
    public static List<Session__c> removeMSMeetingInfo(List<Session__c> sessionList){
        List<Session__c> sessionToUpdateList = new List<Session__c>();
        try{
            for(Session__c session : sessionList){
                if(session.Meeting_Info__c != NULL && session.Meeting_Info__c.containsIgnoreCase('Microsoft Teams meeting')){
                    //Remove the MS Teams meeting link in the Meeting Info and Meeting URL if splitter exist '---'
                    if(session.Meeting_Info__c.containsIgnoreCase('---')){
                        List<String> meetInfoList = session.Meeting_Info__c.split('---');
                        if(meetInfoList.size()>0){
                            session.Meeting_Info__c = meetInfoList[0];
                        }
                        if(session.Meeting_URL__c.startsWith('https://teams.microsoft.com')){
                            session.Meeting_URL__c = null;
                        }
                        sessionToUpdateList.add(session);
                    }
                    //Remove the MS Teams meeting link in the Meeting Info is the splitter text doesn't exist '---'
                    else{
                        session.Meeting_Info__c = null;
                        session.Meeting_URL__c = null;
                        sessionToUpdateList.add(session);
                    }
                }
            }
        }
        catch(Exception e){
            System.debug('removeMSMeetingInfo >>> Something went wrong: ' + e.getLineNumber() + ' - ' + e.getMessage());
        }
        return sessionToUpdateList;
    }

    public class changeEventTypeData {
        @AuraEnabled
        public String eventType {get;set;}
        @AuraEnabled
        public String virtualMeetingPlatform {get;set;}
        @AuraEnabled
        public Boolean checkHasMSMeetingSession {get;set;}
        @AuraEnabled
        public Boolean checkHasSession {get;set;}

        public changeEventTypeData(String eventType, String virtualMeetingPlatform, Boolean checkHasMSMeetingSession, Boolean checkHasSession){
            this.eventType = eventType;
            this.virtualMeetingPlatform = virtualMeetingPlatform;
            this.checkHasMSMeetingSession = checkHasMSMeetingSession;
            this.checkHasSession = checkHasSession;
        }
    }


    public without sharing class WithoutSharingClass{
        public List<Session__c> getSessionWithMSTeamsInfo(String eventId){
            return [SELECT Id, Name, Meeting_Info__c, Meeting_URL__c FROM Session__c WHERE Event__c =: eventId AND Meeting_URL__c LIKE '%https://teams.microsoft.com%' ORDER BY CreatedDAte DESC];
        }

        public Event__c getEvent(String eventId){
            return [SELECT Id, Event_Type__c, Virtual_Meeting_Platform__c, (SELECT Id FROM CC_Sessions__r LIMIT 1) FROM Event__c WHERE Id =: eventId];
        }

        public Event__c updateEvent(Event__c eventRecord){
            update eventRecord;
            return eventRecord;
        }

    }
}