public with sharing class CustomReusableLookupController {
  @AuraEnabled
  public static List<sObject> fetchLookUpValues(
    String searchKeyword,
    String objectName
  ) {
    final String SEARCH_KEY = String.escapeSingleQuotes(searchKeyword) + '%';
    objectName = String.escapeSingleQuotes(objectName);
    List<sObject> returnList = new List<sObject>();

    String queryStr =
      'SELECT Id, Name {0} FROM ' +
      objectName +
      ' WHERE Name LIKE :SEARCH_KEY {1} ORDER BY Name LIMIT 10';

    if (objectName == 'Contact') {
      //1st list value is the additional fields, 2nd value is the additional conditions
      queryStr = String.format(
        queryStr,
        new List<String>{
          ', Email',
          'OR FirstName LIKE :SEARCH_KEY OR LastName LIKE :SEARCH_KEY'
        }
      );
    } else {
      queryStr = queryStr.replace('{0}', '');
      queryStr = queryStr.replace('{1}', '');
    }

    for (sObject obj : Database.query(queryStr)) {
      returnList.add(obj);
    }

    return returnList;
  }
}