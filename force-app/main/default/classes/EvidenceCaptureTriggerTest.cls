@isTest
public class EvidenceCaptureTriggerTest {
    @testSetup
    static void testSetup(){
        Apex_Trigger_Switch__c switchh = TestUtility.createApexTriggerSwitch('EvidenceCaptureTrigger', true);
        insert switchh;
    }
    @isTest
    public static void testInsertEvidenceCapture(){
        Compass_capture__c comCap = new Compass_capture__c();
        Insert comCap;
        Evidence_capture__c evidenceCap = new Evidence_capture__c();
        evidenceCap.Weekly_Evaluation__c = 0.2;
        evidenceCap.Capture_ID__c = comCap.Id;
        Test.startTest();
        Insert evidenceCap;
        Test.stopTest();
        system.assertEquals(0.2, [SELECT Percent_completion__c FROM Compass_capture__c WHERE Id =: comCap.Id].Percent_completion__c);
        
    }
    @isTest
    public static void testUpdateEvidenceCapture(){
        Compass_capture__c comCap = new Compass_capture__c();
        Insert comCap;
        
        Evidence_capture__c evidenceCap = new Evidence_capture__c();
        evidenceCap.Weekly_Evaluation__c = 0.2;
        evidenceCap.Capture_ID__c = comCap.Id;
        Insert evidenceCap;
        //system.assertEquals(0.2, [SELECT Percent_completion__c FROM Compass_capture__c WHERE Id =: comCap.Id].Percent_completion__c);
        Test.startTest();
        evidenceCap.Weekly_Evaluation__c = 0.4;
        Update evidenceCap;
        Test.stopTest();
        system.assertEquals(0.4, [SELECT Percent_completion__c FROM Compass_capture__c WHERE Id =: comCap.Id].Percent_completion__c);
        
    }
}