@isTest
public class countdowntimercontrollerTest {
    
    @testSetup
    static void setup(){
        
        
        Apex_Trigger_Switch__c aps_event = new Apex_Trigger_Switch__c();
        aps_event.Name = 'TeleMeetTrigger';
        aps_event.Active__c = false;
        insert aps_event;
        
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = 'Test Campaign';
        testCampaign.Account__c = acc.Id;
        Insert testCampaign;
        
        Connect_Configurator__c cc = new Connect_Configurator__c();
        cc.Active__c = true;
        cc.Campaign__c = testCampaign.Id;
        insert cc;
        
        List<Connect_Widget__c> cwList = new List<Connect_Widget__c>();
        Connect_Widget__c cw = new Connect_Widget__c();
        cw.Active__c = true;
        cw.Connect_Configurator__c = cc.Id;
        cw.Widget_Parent_Object__c = 'Connect';
        cw.Widget_Child_Object__c = 'Connect Participants';
        cw.Widget_Label__c = 'Widget Test 1';
        cw.Widget_Size__c = 'Classic Top Left S1';
        cw.Widget_View__c = 'List';
        cw.Widget_Reference_Type__c = 'VirtuVisit';
        cw.Widget_Logo__c = 'https://test.com';
        cwList.add(cw);
        insert cw;
        
        Contact testContact = NavigatorTestDataFactory.createContact('ExistingContact', 'ExistingContact@test.com', null);
        testContact.Specialties__c = 'Dental';
        Insert testContact;
        
        TeleMeet__c telemetRec = new TeleMeet__c();
        telemetRec.Contact__c = testContact.Id;
        telemetRec.Resource_Contact__c = testContact.Id;
        telemetRec.Time_Zone__c = 'AKDT';
        telemetRec.Start_Date_Time__c = System.now();
        telemetRec.End_Date_Time__c = System.now().addSeconds(1800);
        telemetRec.Status__c = 'Completed';
        telemetRec.Type__c = 'VirtuVisit';
        telemetRec.Subject__c = 'Subject';
        telemetRec.Description__c = 'Desccription';
        insert telemetRec;
        
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        
        Contact_Availability__c conAvaRec= new Contact_Availability__c();
        conAvaRec.Contact__c = testContact.Id;
        conAvaRec.TeleMeet__c = telemetRec.Id;
        conAvaRec.Specialties__c = 'Dental';
        conAvaRec.Start_Date_Time__c = System.now();
        conAvaRec.End_Date_Time__c = System.now().addSeconds(1800);
        insert conAvaRec;
        
        Connect_Agenda__c conAgenda = new Connect_Agenda__c();
        conAgenda.Completed__c = true;
        conAgenda.Connect__c = telemetRec.Id;
        conAgenda.Contact__c = testContact.Id;
        conAgenda.Start_Time__c = System.now().time();
        conAgenda.End_Time__c = System.now().addSeconds(1800).time();
        conAgenda.Description__c = 'Desccription';
        insert conAgenda;
        
        TeleMeet_Participant__c telemetPart = new TeleMeet_Participant__c();
        telemetPart.Contact__c = testContact.Id;
        telemetPart.Optional__c = true;
        telemetPart.Role__c = 'test';
        telemetPart.TeleMeet__c= telemetRec.Id;
        
        Practice_Management__c pracMan = new Practice_Management__c();
        pracMan.Name = 'test';
        insert pracMan;
        
        Telehealth_Consultation_Group__c telemetCon = new Telehealth_Consultation_Group__c();
        telemetCon.Contact__c = testContact.Id;
        telemetCon.Role__c = 'test';
        telemetCon.Practice_Management__c =pracMan.id;
    }
    
    @isTest
    static void getConnectWidgets(){
        Connect_Configurator__c cc = [Select Id From Connect_Configurator__c LIMIT 1];
        Connect_Widget__c cw = [Select Id From Connect_Widget__c LIMIT 1];
        List<ContentVersion> cvList = new List<ContentVersion>();
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,
            Guest_Record_fileupload__c = cc.Id + '-----1'
        );
        cvList.add(contentVersion);
        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,
            Guest_Record_fileupload__c = cw.Id
        );
        cvList.add(contentVersion2);
        
        Test.startTest();
        countdowntimercontroller.getConnectWidgets(cc.Id);
        countdowntimercontroller.getConfigurator(cc.Id);
        insert cvList;
        Test.stopTest();
        
    }
    @isTest
    static void getFieldValuesTest() {
        Contact con = [Select Id, Name from Contact Limit 1];
        TeleMeet__c telemetRec = [Select Id from TeleMeet__c LIMIT 1];
        Contact_Availability__c conAva = [Select Id from Contact_Availability__c LIMIT 1];
        ContentDocument oldCV = [Select Id from ContentDocument LIMIT 1];
        Practice_Management__c pracMan = [Select Id from Practice_Management__c LIMIT 1];
        Connect_Agenda__c conAgenda = [Select Id from Connect_Agenda__c LIMIT 1];
        Test.startTest();
        countdowntimercontroller.fetchTelemeetStartDate(telemetRec.Id);
        countdowntimercontroller.fetchConnectAgenda(telemetRec.Id);
        countdowntimercontroller.fetchConnectParticipant(telemetRec.Id);
        countdowntimercontroller.fetchTelehealthConsultationGroup(pracMan.Id);
        countdowntimercontroller.getTimeZoneSidKey();
        countdowntimercontroller.getConvertedDates(System.now(), 'AFT');
        countdowntimercontroller.queryRecords('Connect', 'Connect Participants', '', '', telemetRec.Id);
        countdowntimercontroller.queryRecords('Connect', 'Patient Information', '', '', telemetRec.Id);
        countdowntimercontroller.queryRecords('Practice Builder', 'Agenda Builder', '', '', telemetRec.Id);
        countdowntimercontroller.createConnectNotes(telemetRec.Id, 'Test');
        countdowntimercontroller.searchForConAgeRecord(conAgenda.Id);
        countdowntimercontroller.completeConnectAgenda(conAgenda.Id);
        countdowntimercontroller.getSFTimezone();
        Test.stopTest();
    }
}