@isTest
private class ChangeEventTypeControllerTest {
    // Updated by Paolo Quiambao [CCN-745] Oct292021
    @testSetup static void setup() {
        List < Apex_Trigger_Switch__c > atsList = new List < Apex_Trigger_Switch__c > ();
        Apex_Trigger_Switch__c aps_c = new Apex_Trigger_Switch__c();
        aps_c.Name = 'campaignTrigger';
        aps_c.Active__c = false;
        atsList.add(aps_c);

        Apex_Trigger_Switch__c aps_c2 = new Apex_Trigger_Switch__c();
        aps_c2.Name = 'SessionTrigger';
        aps_c2.Active__c = true;
        atsList.add(aps_c2);
        insert atsList;

        Contact con1 = new Contact();
        con1.FirstName = 'Test Contact';
        con1.LastName = 'Record 1';
        con1.Email = 'testemail@testemail.com';
        insert con1;

        Compass_Setting__c compSetting = new Compass_Setting__c();
        compSetting.Name = 'Default Settings';
        compSetting.Email_Sender_Id__c = '123';
        compSetting.Email_Sender_Name__c = 'NAME';
        insert compSetting;

    }

    @isTest
    static void testSaveData() {
    
        Account acc = new Account(Name ='Test Acc');
        insert acc;
        Campaign__c  campaignRec = new   Campaign__c();
        campaignRec.Account__c = acc.Id;
        campaignRec.Site_Picklist_Values__c = 'Site 1,Site 2,Site 3';
        insert campaignRec;
        
        Event__c newCampaign = new Event__c();
        newCampaign.Name = 'EventName';
        newCampaign.Campaign__c =campaignRec.Id;
        newCampaign.Site_Picklist_Values__c = 'Site 1,Site 2,Site 3';
        newCampaign.Event_Type__c = 'Virtual';
        newCampaign.Virtual_Meeting_Platform__c = 'MS Teams';
        insert newCampaign;

        List < Session__c > sessionList = new List < Session__c > ();
        Session__c sessOne = new Session__c();
        sessOne.Name = 'Test';
        sessOne.Event__c = newCampaign.Id;
        sessOne.IsActive__c = true;
        sessOne.Start_Date_Time__c = DateTime.Now();
        sessOne.End_Date_Time__c = DateTime.Now().addHours(1);
        sessOne.Time_Zone__c = 'AFT';
        sessOne.Meeting_Info__c = 'TEst --- Microsoft Teams meeting';
        sessOne.Meeting_URL__c = 'https://teams.microsoft.com';
        sessionList.add(sessOne);

        Session__c sessTwo = new Session__c();
        sessTwo.Name = 'Test2';
        sessTwo.Event__c = newCampaign.Id;
        sessTwo.IsActive__c = true;
        sessTwo.Start_Date_Time__c = DateTime.Now();
        sessTwo.End_Date_Time__c = DateTime.Now().addHours(1);
        sessTwo.Time_Zone__c = 'AFT';
        sessOne.Meeting_Info__c = 'TEst --- <p><span style="font-size: 24px; font-family: inherit;">Microsoft Teams meeting</span></p><p><strong style="font-family: inherit;">Join on your computer, mobile app or room device</strong></p><p><a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZmVhNjk0NGEtNTAwMi00YTc5LThhNjAtYzNkMzQxMGQxYWMx%40thread.v2/0?context=%7b%22Tid%22%3a%221f90bb8a-c3af-448d-bbfc-e4d93d9ac749%22%2c%22Oid%22%3a%22e2c170f3-2f3a-423f-855c-f2d3d3edf28e%22%7d" target="_blank" style="color: rgb(98, 100, 167);">Click here to join the meeting</a></p><p><span style="font-family: inherit;">Meeting ID: </span><span style="font-family: inherit; font-size: 16px;">210683410159</span></p><p><span style="font-family: inherit;">Passcode: </span><span style="font-family: inherit; font-size: 16px;">2FVXTA</span></p><p><span style="font-family: inherit;">Conference ID: </span><span style="font-family: inherit; font-size: 16px;">226349281</span></p><p><span style="font-family: inherit;">Toll Number: </span><span style="font-family: inherit; font-size: 16px;">+1 323-457-5649</span></p><p><span style="font-family: inherit;">Dial In Url: </span><span style="font-family: inherit; font-size: 16px;">https://dialin.teams.microsoft.com/3d1f6610-822e-400e-b434-126bbc28aaf6?id=226349281</span></p>';
        sessOne.Meeting_URL__c = 'https://teams.microsoft.com';
        sessionList.add(sessTwo);

        insert sessionList;

        test.startTest();
        Test.setMock(HttpCalloutMock.class, new AzureAPIMockHttpResponseGenerator());
        ChangeEventTypeController.changeEventTypeData changeEventTypeWrap = ChangeEventTypeController.getData(newCampaign.Id);
        String changeEventTypeCtrl = ChangeEventTypeController.saveData(newCampaign.Id, 'In-Person', '', 'VirtualToInPerson-RemoveVirtualPlatformInfo');
        ChangeEventTypeController.removeMSMeetingInfo([SELECT Id, Meeting_Info__c, Meeting_URL__c FROM Session__c WHERE Event__c =: newCampaign.Id]);
        test.stopTest();
    }

}