@isTest
public without sharing class NavigatorTriggerHandler_Test {

    @TestSetup
    static void setup(){
        
        Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
        Insert testCampaign;

        Navigator__c testNavigator = NavigatorTestDataFactory.createNavigator('testNavigator', testCampaign.Id);
        Insert testNavigator;
        
    }

    @isTest static void updateLoginPageUrl(){
        Navigator__c testNavigator = [SELECT Id FROM Navigator__c LIMIT 1];
        test.startTest();
        String testUrl = NavigatorTriggerHandler.getCommunityURL('test');
        test.stopTest();

        //Navigator__c resultNavigator = [SELECT Id, Login_Page_URL__c FROM Navigator__c LIMIT 1];
        //Boolean assertionResult = resultNavigator.Login_Page_URL__c.contains(resultNavigator.Id);
        //System.assert(assertionResult, 'Actual URL result does not match the actual URL result.');

    }
    
    @isTest static void getCommunityURL_test(){
        test.startTest();
        String testUrl = NavigatorTriggerHandler.getCommunityURL('test');
        test.stopTest();
        //System.assertEquals('', testUrl, 'Community URL result should match the expected URL result.');
    }

}