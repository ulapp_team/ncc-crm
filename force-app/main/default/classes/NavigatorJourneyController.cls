/*******************************************************************************
 * @author       Angelo Rivera
 * @date         28.06.2021
 * @description  Navigator Journey Controller
 * @revision     28.06.2021 - APRivera - Created
 *******************************************************************************/
public without sharing class NavigatorJourneyController {

    public class ContactJourney{
        @AuraEnabled public List<Journey_Participant__c> lstJourney {get; set;}
        @AuraEnabled public String journeyId {get; set;}
        @AuraEnabled public Boolean hasJourney {get; set;}
        @AuraEnabled public String communityURL {get; set;}
    }

    /*******************************************************************************
     * @description  Returns the list of contact's journey
     * @param        contactId - List of User Milestones to be processed
     * @return       ContactJourney
     * @revision     28.06.2021 - APRivera - Created
     *******************************************************************************/
    @AuraEnabled
    public static ContactJourney getContactJourney(String contactId, Id basecampId){
        ContactJourney contactJourney = new ContactJourney();
        
        // START: CCN-NAV-427-DV
        List<Navigator__c> basecampList = getBasecamp(basecampId);
        contactJourney.lstJourney = new List<Journey_Participant__c>(getJourneyParticipants(contactId, basecampList[0]));
		
        if(!contactJourney.lstJourney.isEmpty()){
            if(contactJourney.lstJourney.size() == 1){
                contactJourney.journeyId = contactJourney.lstJourney.get(0).Id;
                //contactJourney.communityURL = contactJourney.lstJourney.get(0).Journey_URL__c;
            }
            contactJourney.hasJourney = true;
        }else{
            contactJourney.hasJourney = false;
        }
		
        contactJourney.communityURL = JourneyParticipantServices.getCommunityURL('Navigator');
		// END: CCN-NAV-427-DV
        System.debug(LoggingLevel.DEBUG, '!@# contactJourney: ' + JSON.serializePretty(contactJourney));
        return contactJourney;
    }
    
    /*******************************************************************************
     * @description  Returns the list of Basecamps CCN-NAV-427-DV
     * @param        basecampId - Basecamp Id
     * @return       List<Navigator__c>
     *******************************************************************************/
    private static List<Navigator__c> getBasecamp(Id basecampId){
        return [SELECT Id, Name, Campaign__c FROM Navigator__c WHERE Id =: basecampId];
    }

    /*******************************************************************************
     * @description  retrieves the list of contact's journey
     * @param        contactId - List of User Milestones to be processed
     * @return       List<Journey_Participant__c> lstJourneyParticipants
     * @revision     28.06.2021 - APRivera - Created
     *******************************************************************************/
    public static List<Journey_Participant__c> getJourneyParticipants(String contactId, Navigator__c basecamp){
        List<Journey_Participant__c> lstJourneyParticipants = new List<Journey_Participant__c>();
        lstJourneyParticipants = [SELECT Id, Contact__r.Contact_Id__c, Journey_URL__c, Journey__r.Name
                                    FROM Journey_Participant__c
                                    WHERE Contact__r.Contact_Id__c =: contactId
                                 		AND Journey__r.Campaign__c =: basecamp.Campaign__c
                                 	ORDER BY Journey__r.Name];
        return lstJourneyParticipants;
    }


}