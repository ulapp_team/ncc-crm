/**
* Description: Handler Class for Contact Trigger
* Author: Jester Cabantog
* Version: 1.0
* Last modified date: Oct-4-2021
* Last modified by: Jayson Labnao
**/
// TODO: Contact_Id__c field should not be use since it looks like it's not considering duplicates.
public with sharing class ContactTriggerHandler {

    /*******************************************************************************************
    * @name: setUpNavigatorFields
    * @author: Jester Cabantog
    * @created: 01-04-2021
    * @description: Update Navigator related fields on the contact.
    * 
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy) Author         Description
    *            ----  ---------    --------------------  -----------------------------
    * @version   1.0    01-04-2021  Jester Cabantog       00675 Initial version.
    * @version   1.1    15-10-2021  Jayson Labnao         CCN857 Fixed Error in Issue Tracker.
    *********************************************************************************************/
    public static void setUpNavigatorFields(List<Contact> newList){
        String communityURL = getCommunityURL('Navigator');

        // Updated by JaysonLabnao [CCN857] Oct-15-2021
        for(Contact con : newList){
            // TODO remove Contact_Id__c logic
            if(String.isBlank(con.Contact_Id__c)){
                con.Contact_Id__c = generateContactId();
            } 
            if(String.isBlank(con.Login_Token__c)){
                con.Login_Token__c = generateLoginToken();
            } 

            if(con.Navigator__c != null && !String.isBlank(communityURL)){
                con.Navigator_Page_URL__c = communityURL+'/s/navigator-home?id='+ con.Navigator__c +'&contactId='+ con.Contact_Id__c +'&token='+ con.Login_Token__c;
            }
        }        
    }

    /*******************************************************************************************
    * @name: setUpNavigatorFields
    * @author: Jester Cabantog
    * @created:  01-04-2021
    * @description: Update Navigator related fields on the contact.
    * 
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy) Author         Description
    *            ----  ---------    --------------------  -----------------------------
    * @version   1.0   01-04-2021   Jester Cabantog       00675 Initial version.
    * @version   1.1    15-10-2021  Jayson Labnao         CCN857 Fixed Error in Issue Tracker.
    *********************************************************************************************/
    public static void setUpNavigatorFields(Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
        String communityURL = getCommunityURL('Navigator');

        // Updated by JaysonLabnao [CCN857] Oct-15-2021
        for(Contact con : newMap.values()){
            // TODO remove Contact_Id__c logic
            if(String.isBlank(con.Contact_Id__c)){
                con.Contact_Id__c = generateContactId();
            } 
            if(String.isBlank(con.Login_Token__c)){
                con.Login_Token__c = generateLoginToken();
            } 

            if((con.Navigator__c != null && !String.isBlank(communityURL) && 
                con.Login_Token__c != oldMap.get(con.Id).Login_Token__c || con.Navigator__c != oldMap.get(con.Id).Navigator__c)){
                con.Navigator_Page_URL__c = communityURL+'/s/navigator-home?id='+ con.Navigator__c +'&contactId='+ con.Contact_Id__c +'&token='+ con.Login_Token__c;
            }
        }        
    }
    
    /*******************************************************************************************
    * @name: generateContactId
    * @author: Jester Cabantog
    * @created:  01-04-2021
    * @description: Generates 11 digit random string as Contact's Id for Navigator.
    * 
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy) Author         Description
    *            ----  ---------    --------------------  -----------------------------
    * @version   1.0    01-04-2021  Jester Cabantog       00675 Initial version.
    * @version   1.1    15-10-2021  Jayson Labnao         Updated method's name.
    *********************************************************************************************/
    // Updated by JaysonLabnao [CCN857] Oct-15-2021
    public static String generateContactId(){
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        
        String token = '';
        while (token.length() <= 10) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            token += chars.substring(idx, idx+1);
        }
        
        return token;
    }

    /*******************************************************************************************
    * @name: generateLoginToken
    * @author: Jester Cabantog
    * @created:  01-04-2021
    * @description: Generates random string for Navigator user's login.
    * 
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy) Author         Description
    *            ----  ---------    --------------------  -----------------------------
    * @version   1.0    15-10-2021  Jayson Labnao         00675 Initial version.
    *********************************************************************************************/
    public static String generateLoginToken(){
        Integer len = 32;
        Blob blobKey = crypto.generateAesKey(128);
        return EncodingUtil.convertToHex(blobKey).substring(0, len);
    }
    
    /*******************************************************************************************
    * @name: getCommunityURL
    * @author: Jester Cabantog 
    * @created:  01-04-2021
    * @description: Get the Navigator's Community site URL.
    * 
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy) Author         Description
    *            ----  ---------    --------------------  -----------------------------
    * @version   1.0    01-04-2021  Jester Cabantog       00675 Initial version.
    * @version   1.1    15-10-2021  Jayson Labnao         CCN857 Fixed Error in Issue Tracker.
    *********************************************************************************************/
    public static String getCommunityURL(string communityName){
        if (!Test.isRunningTest()){
            // Updated by JaysonLabnao [CCN857] Oct-15-2021
            try{
                Site site = [SELECT Id FROM Site WHERE UrlPathPrefix = :communityName WITH SECURITY_ENFORCED LIMIT 1 ];
                String communityUrl = [SELECT SecureURL FROM SiteDetail WHERE DurableId = :site.Id WITH SECURITY_ENFORCED].SecureUrl;
                return communityUrl;
            }
            catch(Exception e){
                return '';
            }
        } else {
            // Updated by JaysonLabnao [CCN857] Oct-15-2021
            return 'unit-test-url';   
        }
    }
    
}