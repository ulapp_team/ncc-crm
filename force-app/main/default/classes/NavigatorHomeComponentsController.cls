/*******************************************************************************************
* @name: NavigatorHomeComponentsController
* @author: Jayson Labnao
* @created: 19-06-2021
* @description: Apex Controller for Navigator Home Page widgets
*
* Changes (version)
* -------------------------------------------------------------------------------------------
*            No.   Date(dd-mm-yyy)  Author                Description
*            ----  ---------        --------------------  -----------------------------
* @version   1.0   19-06-2021       Jayson Labnao          [All Navigator Home Page Tasks] - 
*                                                         Initial version.
*********************************************************************************************/
public without sharing class NavigatorHomeComponentsController {
    private static final String SURVEY_API_NAME = Survey__c.sObjectType.getDescribe().getName();
    private static final String COMMUNICATION_API_NAME = Communication__c.sObjectType.getDescribe().getName();
    private static final String EVENT_API_NAME = Event__c.sObjectType.getDescribe().getName();

    /*******************************************************************************************
    * @name: SessionWrapper
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Wrapper for Session__c object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021      Jayson Labnao         [CCN-502] - Initial version.
    * @version   1.1   06-10-2021      Jayson Labnao         [CCN-502] - Updated to display Date
    *                                                         and Time from the Session Details
    *                                                         field.
    *********************************************************************************************/
    public class SessionWrapper{
        @AuraEnabled 
        public Datetime startDateTime {get; set;}
        @AuraEnabled 
        public Datetime endDateTime {get; set;}
        @AuraEnabled 
        public String name {get; set;}
        @AuraEnabled 
        public String sessionTime {get; set;}
    }

    /*******************************************************************************************
    * @name: EventSessionWrapper
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Wrapper for Event__c and Session__c objects to display in Events Widget from
    *               Navigator Home Page. 
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-502] - Initial version.
    * @version   1.1   06-10-2021      Jayson Labnao          [CCN-502] - Updated to display Date
    *                                                         and Time from the Event Details
    *                                                         field.
    *********************************************************************************************/
    public class EventSessionWrapper{
        @AuraEnabled
        public NavigatorUtility.EventWrapper event {get; set;}
        @AuraEnabled
        public String eventTime {get; set;}
        @AuraEnabled
        public List<SessionWrapper> sessions {get; set;}
        @AuraEnabled
        public Boolean isSingleEvent {get; set;} // set to true if the event is the session itself.
    }

    /*******************************************************************************************
    * @name: getParticipantMilestones
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Wrapper for Event__c and Session__c objects to display in Events Widget
    *               (NavigatorHome_MyEvents) from Navigator Home Page. 
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-503] - Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static List<Participant_Milestone__c> getParticipantMilestones(String contactId){
        List<Participant_Milestone__c> pMilestones = [SELECT Id, Milestone__c, Milestone__r.Name, Milestone__r.Type__c, 
                                                             Milestone__r.Related_RecordId__c, Milestone_Progress__c, Milestone_Status__c
                                                      FROM Participant_Milestone__c 
                                                      WHERE Contact__r.Contact_Id__c = :contactId
                                                      AND Checkpoint__c <> TRUE 
                                                      AND Is_Active__c = TRUE];

        List<Participant_Milestone__c> pMilestonesReturn = new List<Participant_Milestone__c>();
        for(Participant_Milestone__c pMilestone : pMilestones){
            // exclude communication that are missed, upcoming, and on-going
            if(!(pMilestone.Milestone__r.Type__c == 'Communication' && pMilestone.Milestone_Status__c != 'Completed')){
                pMilestonesReturn.add(pMilestone);
            }
        }
        return pMilestonesReturn;
    }
    
    /*******************************************************************************************
    * @name: getEventsAndSessions
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Retrieves Event and Sessions to be displayed on Events Widget 
    *               (NavigatorHome_MyEvents)
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-502] - Initial version.
    * @version   1.1   06-10-2021       Jayson Labnao         [CCN-502] - Updated to display Date
    *                                                         and Time from the Event Details
    *                                                         field.
    *********************************************************************************************/
    @AuraEnabled
    public static List<EventSessionWrapper> getEventsAndSessions(String contactId, Integer month, Integer year, String navigatorId){

        // JS Date's month start at 0
        // CCN-3447 commented out as it is not necessary
        //month++;

        Navigator__c nav = [SELECT Id, Campaign__c FROM Navigator__c WHERE Id = : navigatorId];

        List<EventSessionWrapper> eventSessions = new List<EventSessionWrapper>();
       
        List<Event__c> eventsList = getEventsByParticipantMilestones(contactId, month, year, nav.Campaign__c);
        List<Session__c> sessionList = [SELECT Id, Name, Event__c, Start_Date_Time__c, End_Date_Time__c, Session_Details__c
                                        FROM Session__c
                                        WHERE Id IN (SELECT Session__c 
                                                    FROM Session_Participant__c 
                                                    WHERE Contact__r.Contact_Id__c = :contactId 
                                                    OR Participant__r.Member_Contact__r.Contact_Id__c =:contactId)
                                        AND End_Date_Time__c >= TODAY
                                        AND CALENDAR_MONTH(Start_Date_Time__c) = :month
                                        AND CALENDAR_YEAR(Start_Date_Time__c) = :year
                                        AND IsActive__c = TRUE
                                        AND (Campaign__c = : nav.Campaign__c 
                                        OR Event__r.Campaign__c = : nav.Campaign__c)
                                        ORDER BY Start_Date_Time__c ASC];
         
        // For demo only
        /*List<Event__c> eventsList = [SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, Event_Time_Details__c,
                    (SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c FROM CC_Sessions__r)
                FROM Event__c 
                WHERE Id = 'a4wDS000001i4UyYAI'];
        List<Session__c> sessionList = [SELECT Id, Name, Event__c, Start_Date_Time__c, End_Date_Time__c, Session_Details__c
                                        FROM Session__c
                                        WHERE Id IN ('a3hDS000000qc70YAA', 'a3hDS000000qc76YAA', 'a3hDS000000qc7ZYAQ')];
		*/
		// For demo only
		
        System.debug('sessionList >>> ' + sessionList);

        Map<Id, List<SessionWrapper>> eventSessionsMap = new Map<Id, List<SessionWrapper>>();
        for(Session__c session : sessionList){
            
            SessionWrapper newSessionWrap = new SessionWrapper();
            Datetime dateToday = Datetime.now();
            newSessionWrap.name = session.Name;
            if(session.Start_Date_Time__c != null){
                newSessionWrap.startDateTime = session.Start_Date_Time__c;
            }
            if(session.End_Date_Time__c != null){
                newSessionWrap.endDateTime = session.End_Date_Time__c;
            }
            if(session.Session_Details__c != null){
                String[] sessionTimeStrList = session.Session_Details__c.split(' to ');
                String startTimeStr = sessionTimeStrList[0];
                String endTimeStr = sessionTimeStrList[1];
                newSessionWrap.sessionTime = '';
                if(startTimeStr.length() > 14){
                    newSessionWrap.sessionTime += startTimeStr.substring(14, startTimeStr.length()).trim();
                }
                else{
                    newSessionWrap.sessionTime += startTimeStr;
                }
                newSessionWrap.sessionTime += ' - ';
                if(endTimeStr.length() > 14){
                    newSessionWrap.sessionTime += endTimeStr.substring(14, endTimeStr.length());
                }
                else{
                    newSessionWrap.sessionTime += endTimeStr;
                }
            }

            if(!eventSessionsMap.containsKey(session.Event__c)){
                eventSessionsMap.put(session.Event__c, new List<SessionWrapper>{newSessionWrap});
            }
            else{
                List<SessionWrapper> sessList = eventSessionsMap.get(session.Event__c);
                sessList.add(newSessionWrap);
                eventSessionsMap.put(session.Event__c, sessList);
            }
        }

        for(Event__c evt : eventsList){
            EventSessionWrapper eventSessionWrap = new EventSessionWrapper();

            if(evt.CC_Sessions__r == null || evt.CC_Sessions__r.size() == 0){
                eventSessionWrap.isSingleEvent = true;
            }
            else{
                eventSessionWrap.isSingleEvent = false;
                eventSessionWrap.sessions = eventSessionsMap.get(evt.Id);
            }
            eventSessionWrap.event = NavigatorUtility.buildEventObj(evt);
            eventSessions.add(eventSessionWrap);
        }
        
        System.debug('eventSessions >>> ' + eventSessions);
        return eventSessions;
    }

    /*******************************************************************************************
    * @name: getEventsByParticipantMilestones
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Retrieves Event Participant Milestones and returns list of events (NavigatorHome_MyEvents)
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-502] - Initial version.
    *********************************************************************************************/
    private static List<Event__c> getEventsByParticipantMilestones(String contactId, Integer month, Integer year, Id campaignId){
        List<Participant__c> lstParticipant                         = NavigatorHomeComponentService.getParticipantRecords(contactId, campaignId);
        List<Participant__c> lstParticipantSessions                 = NavigatorHomeComponentService.getParticipantRecordsViaSession(contactId, month, year, campaignId);
        //List<Participant_Milestone__c> lstParticipantMilestones   = getEventParticipantMilestones(contactId, campaignId, month, year);

        List<Event__c> eventsList   = new List<Event__c>();
        Set<Id> alreadyAddedIds     = new Set<Id>();

        if(!lstParticipant.isEmpty()){
            Set<Id> setRecordIds = new Set<Id>();

            for(Participant__c pm : lstParticipant){
                //Id recordId = (Id) pm.Milestone__r.Related_RecordId__c;
                //commented out if CCN-3447
                //if(recordId.getSobjectType() == Event__c.SObjectType){
                if(alreadyAddedIds.contains(pm.Event__c) == false) {
                    setRecordIds.add(pm.Event__c);
                }
                //}
            }

            if(!setRecordIds.isEmpty()){
                System.debug('setRecordIds >>> ' + setRecordIds);
                eventsList = getEvents(setRecordIds, month, year);
            }
        }

        if(!lstParticipantSessions.isEmpty()){
            Set<Id> setPMRecordIds = new Set<Id>();

            for(Participant__c pm : lstParticipantSessions){
                //Id recordId = (Id) pm.Milestone__r.Related_RecordId__c;
                if(alreadyAddedIds.contains(pm.Event__c) == false) {
                    setPMRecordIds.add(pm.Event__c);
                }
            }

            if(!setPMRecordIds.isEmpty()){
                System.debug('setRecordIds >>> ' + setPMRecordIds);
                eventsList.addAll(getEvents(setPMRecordIds, month, year));
            }
        }

        /*if(!lstParticipantMilestones.isEmpty()){
            Set<Id> setPMRecordIds = new Set<Id>();

            for(Participant_Milestone__c pm : lstParticipantMilestones){
                Id recordId = (Id) pm.Milestone__r.Related_RecordId__c;
                setPMRecordIds.add(recordId);
            }

            if(!setPMRecordIds.isEmpty()){
                System.debug('setRecordIds >>> ' + setPMRecordIds);
                eventsList.addAll(getEvents(setPMRecordIds, month, year));
            }
        }*/

        return eventsList;
    }

    /*******************************************************************************************
    * @name: getEvents
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Returns list of Event__c based on Id, month and year.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-502] - Initial version.
    *********************************************************************************************/
    public static List<Event__c> getEvents(Set<Id> setSessionIds, Integer month, Integer year){
        
        Set<Id> setEventIds = new Set<Id>();
        List<Id> idList     = new List<Id>();

        idList.addAll(setSessionIds);

        Id sId = Id.ValueOf(idList[0]);
        String sobjectType = sId.getSObjectType().getDescribe().getName();

        System.debug(sobjectType);

        if (sobjectType == 'Session__c') {
            //CCN-3447 added for-loop and changed return query
            for (Session__c rec: [SELECT Id, Event__c FROM Session__c WHERE Id IN : setSessionIds]) {
                setEventIds.add(rec.Event__c);
            }
        } else {
            setEventIds.addAll(setSessionIds);
        }
        
        return [SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, Event_Time_Details__c,
                    (SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c FROM CC_Sessions__r)
                FROM Event__c 
                WHERE Id IN :setEventIds
                AND IsActive__c = TRUE
                AND (CALENDAR_MONTH(End_Date_Time__c) >= :month)
                ORDER BY Start_Date_Time__c ASC];

        /*return [SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c, Time_Zone__c, Event_Time_Details__c,
                    (SELECT Id, Name, Start_Date_Time__c, End_Date_Time__c FROM CC_Sessions__r)
                FROM Event__c 
                WHERE Id IN :setEventIds
                AND IsActive__c = TRUE
                AND End_Date_Time__c >= TODAY
                AND (CALENDAR_MONTH(End_Date_Time__c) <= :month)
                // AND (CALENDAR_MONTH(Start_Date_Time__c) <= :month AND CALENDAR_MONTH(End_Date_Time__c) >= :month)
                // AND (CALENDAR_YEAR(Start_Date_Time__c) <= :year AND CALENDAR_YEAR(End_Date_Time__c) >= :year)
                ORDER BY Start_Date_Time__c ASC];*/
    }

    /*******************************************************************************************
    * @name: getEventParticipantMilestones
    * @author: Jayson Labnao
    * @created: 21-07-2021
    * @description: Returns list of Participant_Milestone__c based on Id contactId.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-07-2021       Jayson Labnao         [CCN-502] - Initial version.
    *********************************************************************************************/
    public static List<Participant_Milestone__c> getEventParticipantMilestones(String contactId, Id campaignId, Integer month, Integer year){
        return [SELECT Id, Contact__c, Contact__r.Contact_Id__c,
                       Milestone__r.Related_RecordId__c, Type__c, Milestone_Status__c
                FROM Participant_Milestone__c
                WHERE Contact__r.Contact_Id__c = :contactId
                AND Type__c = 'Event'
                AND CALENDAR_MONTH(Start_Date__c) <= :month
                AND CALENDAR_YEAR(End_Date__c) <= :year
                AND CALENDAR_MONTH(End_Date__c) >= :month
                AND Milestone__r.Journey__r.Campaign__c = : campaignId
                AND Milestone__r.Related_RecordId__c != NULL];
    }

    /*******************************************************************************************
    * @name: getNavigatorDetails
    * @author: Jayson Labnao
    * @created: 11-07-2021
    * @description: Returns Navigator Details.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   11-05-2021       Jayson Labnao         [01002] - Initial version.
    * @version   1.1   24-05-2022       Jayson Labnao         [CCN-NAV-1674-DV] Added new Navigator Fields 
    *********************************************************************************************/
    @AuraEnabled
    public static NavigatorUtility.NavigatorWrapper getNavigatorDetails(String navigatorId){
        
        Set<String> SobjectFields = Schema.getGlobalDescribe().get('Navigator__c').getDescribe().fields.getMap().keySet();    
        List<String> fieldsInList = new List<String>(SobjectFields);
        Navigator__c nav = Database.query('SELECT ' + String.join(fieldsInList, ',') + ', Change_Agent__r.Name,Change_Agent__r.Title,Change_Agent__r.Profile_Picture_URL__c FROM Navigator__c WHERE Id = :navigatorId');
        
        
        /*Navigator__c nav = [
            SELECT
                Id, Name, Live_Call_Number__c, Change_Agent__c, Change_Agent__r.Name, 
                Change_Agent__r.Title, Change_Agent__r.Profile_Picture_URL__c,
                Live_Call_Button__c, Schedule_Video_Call_Button__c, Change_Agent_Lead_Description__c
            FROM Navigator__c 
            WHERE Id = : navigatorId
        ];*/
        
        NavigatorUtility.NavigatorWrapper navWrapper = new NavigatorUtility.NavigatorWrapper();
        if(nav != null){
            navWrapper = NavigatorUtility.buildNavigatorObj(nav);
        }
        return navWrapper;
    }
    
    /*******************************************************************************************
    * @name: getNavigatorDetails
    * @author: Jayson Labnao
    * @created: 11-07-2021
    * @description: Returns Navigator Details.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   11-05-2021       Jayson Labnao         [01002] - Initial version.
    * @version   1.1   24-05-2022       Jayson Labnao         [CCN-NAV-1674-DV] Added new Navigator Fields 
    *********************************************************************************************/
    @AuraEnabled
    public static Navigator__c getNavigatorRecord(String navigatorId){
        
        Set<String> SobjectFields = Schema.getGlobalDescribe().get('Navigator__c').getDescribe().fields.getMap().keySet();    
        List<String> fieldsInList = new List<String>(SobjectFields);
        Navigator__c nav = Database.query('SELECT ' + String.join(fieldsInList, ',') + ',Change_Agent__r.Name,Change_Agent__r.Title,Change_Agent__r.Profile_Picture_URL__c FROM Navigator__c WHERE Id = :navigatorId');
                
        return nav;
    }

    /*******************************************************************************************
    * @name: getSectionAttributes
    * @author: Jayson Labnao
    * @created: 22-08-2021
    * @description: Returns Section Attributes for dynamic content display (NavigatorHome_About)
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   22-08-2021       Jayson Labnao         [CCN-476] - Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static List<NavigatorUtility.NavigatorSectionAttributeWrapper> getSectionAttributes(String navigatorId, String sectionName){
        List<Navigator_Section_Attribute__c> navSectionAttrList = [
            SELECT Id, Name, Type__c, Alignment__c, Sort_Order__c, Value__c 
            FROM Navigator_Section_Attribute__c 
            WHERE Navigator_Section__r.Navigator_Item__r.Navigator__c = :navigatorId
            AND Navigator_Section__r.Name LIKE :sectionName
            ORDER BY Sort_Order__c
        ];

        List<NavigatorUtility.NavigatorSectionAttributeWrapper> navSectionAttrWrapList = new List<NavigatorUtility.NavigatorSectionAttributeWrapper>();

        for(Navigator_Section_Attribute__c navSectionAttr : navSectionAttrList){
            NavigatorUtility.NavigatorSectionAttributeWrapper navSectionAttrWrap = NavigatorUtility.buildNavSectionAttributeObj(navSectionAttr);
            navSectionAttrWrapList.add(navSectionAttrWrap);
        }

        return navSectionAttrWrapList;
    }

    /*******************************************************************************************
    * @name: getContactCommunications
    * @author: Jayson Labnao
    * @created: 22-03-2021
    * @description: Returns Communication__c records related to contact (NavigatorHome_About)
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   22-03-2021       Jayson Labnao         [CCN-NAV-1221-DV] - Initial version.
    *********************************************************************************************/
    @AuraEnabled
    public static List<Communication__c> getContactCommunications(String contactId){

        try {
            
            List<Communication__c> commsList = [
                SELECT Id, Subject__c, Sender_Email_Address__c, Email_Date_and_Time__c, Body__c, Journey__c
                FROM Communication__c
                WHERE Id IN (
                    SELECT CC_Communication__c FROM Communication_Recipient__c WHERE Contact__r.Contact_Id__c = :contactId
                )
                ORDER BY Email_Date_and_Time__c DESC
            ];

            return commsList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}