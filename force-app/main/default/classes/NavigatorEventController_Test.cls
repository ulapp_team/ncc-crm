/*******************************************************************************
 * @author       Angelo Rivera
 * @date         23.07.2021
 * @description  Navigator Event Controller test class
 * @group          Test Class
 * @revision     23.07.2021 - APRivera - Created
 *******************************************************************************/

@IsTest
private class NavigatorEventController_Test {

    @TestSetup
    static void createData(){
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        testContact.Email = 'test@email.com';
        testContact.Contact_Id__c = ' ABC1234';
        insert testContact;

        Campaign__c testCampaign = new Campaign__c();
        testCampaign.Name = 'Test Campaign Name';
        insert testCampaign;

        Journey__c testJourney = new Journey__c();
        testJourney.Name = 'Test Journey';
        testJourney.Campaign__c = testCampaign.Id;
        testJourney.Status__c = 'Approved';
        insert testJourney;

        Journey_Participant__c testParticipant = new Journey_Participant__c();
        testParticipant.Journey__c = testJourney.Id;
        testParticipant.Contact__c = testContact.Id;
        insert testParticipant;
        
        Compass_Setting__c comp = TestDataFactory.createCSData(1);
        insert comp;
        
        Event__c e = new Event__c();
        e.Name = 'TestEvent';
        e.Campaign__c = testCampaign.Id;
        insert e;

        Milestone__c mStone = new Milestone__c();
        mStone.Journey__c = testJourney.Id;
        mStone.Name = 'TestMilestone';
        mStone.Type__c = 'Event';
        mStone.Related_RecordId__c = e.Id;
        insert mStone;

        Participant_Milestone__c pm = new Participant_Milestone__c();
        pm.Contact__c = testContact.Id;
        pm.Journey__c = testParticipant.Id;
        pm.Type__c = 'Event';
        insert pm;
    }


    @IsTest
    static void testJourneyController() {

        String contactId = [SELECT Id, Contact_Id__c FROM Contact LIMIT 1].Contact_Id__c;
        NavigatorEventController.ContactEvents contactEvents = new NavigatorEventController.ContactEvents();

        Test.startTest();
        contactEvents = NavigatorEventController.getContactEvents(contactId,'NoFilter');
        //System.assertEquals(contactEvents.listEvents.size(), 1);
        Test.stopTest();

    }
}