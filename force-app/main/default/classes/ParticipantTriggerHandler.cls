public class ParticipantTriggerHandler {
    // @author: [CCN-1153] Updated by dinoBrinas
    // @description: This is used to avoid the recursive on the apex trigger.
    // @date: Jan282022
    public static boolean bPreventAI = false;
    public static boolean bPreventAU = false;
    public static boolean sendEmail = false; //CCN-1139 Jan 24, 2022 Xen Reyes

    /******************************************************************************    
     * Changes (version)
     *            No.  Date(dd-mm-yyy) Author            Description
     *           ----  ---------   --------------------  -----------------------------
     * @version   1.0  ??           ??                   Initial version.
     * @version   2.0  23-04-2022   VonPernicia          [CCN-1483] Added new method "updateParticipantMilestoneMetrics" to call to update Participant Milestone Metics
     ******************************************************************************/
    public static void onAfterInsert(List < Participant__c > newParticipantList, Map < Id, Participant__c > newParticipantMap,
        List < Participant__c > oldParticipantList, Map < Id, Participant__c > oldParticipantMap) {

        List < Participant__c > updateParticipantMilestoneList = new List < Participant__c > ();

        for (Participant__c pt: newParticipantList) {
            if (!String.isEmpty(pt.Status__c)) {
                updateParticipantMilestoneList.add(pt);
            }
        }
        if (!updateParticipantMilestoneList.isEmpty()) {
            SessionParticipantTriggerUtility.updateParticipantMilestoneMetrics(updateParticipantMilestoneList);
        }

        /* sendEmail_Insert(newParticipantMap); //CCN-1139 Jan 24, 2022 Xen Reyes */

        if (!sendEmail) sendEmail(newParticipantList, oldParticipantMap, true); //CCN-1551 XEN REYES 29 April 2022
    }

    public static void onAfterUpdate(List < Participant__c > newParticipantList, Map < Id, Participant__c > newParticipantMap, List < Participant__c > oldParticipantList, Map < Id, Participant__c > oldParticipantMap) {

        // CCN-1550 XEN REYES 29 April 2022
        /* List<Participant__c> participantListToSend = new List<Participant__c>();
        for (Participant__c pt : newParticipantList){
            Participant__c oldPt = oldParticipantMap.get(pt.Id);
            if (pt.Status__c != oldPt.Status__c ){
                participantListToSend.add(pt);
            }
        }*/

        /* if(sendEmail) sendEmail_Update(newParticipantList); */

        if (!sendEmail) sendEmail(newParticipantList, oldParticipantMap, false); //CCN-1551 XEN REYES 29 April 2022
    }

    public static void onBeforeInsert(List < Participant__c > newParticipantList) {
        Set < Id > eventIds = new Set < Id > ();
        Map < Id, Survey__c > eventToSurveyMap = new Map < Id, Survey__c > ();

        /* CCN-EVE-2500-DV XEN REYES 29Jan2023 */
        Id participantRecTypeId = Schema.SObjectType.Participant__c.getRecordTypeInfosByName().get('Participant').getRecordTypeId();
        Id createNewParticipantRecTypeId = Schema.SObjectType.Participant__c.getRecordTypeInfosByName().get('Create New Participant').getRecordTypeId();
        
        // Added by PQuiambao [CCN-1049] Jan082022
        for (Participant__c participantRec: newParticipantList) {
            eventIds.add(participantRec.Event__c);

            /* CCN-EVE-2500-DV XEN REYES 29Jan2023 */
            if(participantRec.RecordTypeId == createNewParticipantRecTypeId) participantRec.RecordTypeId = participantRecTypeId;
        }

        // Added by PQuiambao [CCN-1049] Jan082022
        for (Survey__c s: [SELECT Id, Event__c, Survey_Catchup_Message__c, Survey_Catchup_Reminder_Message__c, Survey_Reminder_Message__c, Survey_Thank_You_Message__c
                FROM Survey__c WHERE Event__c IN: eventIds
            ]) {
            eventToSurveyMap.put(s.Event__c, s);
        }

        for (Participant__c participantRec: newParticipantList) {
            if (participantRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_REGISTERED && !participantRec.Process_from_Event_Page__c) {
                if (!Test.isRunningTest()) {
                    participantRec.addError('Participant Status: Registered is not allowed. There are no related Participant Session records.');
                }
            } else if (participantRec.Status__c == 'Attended') {
                participantRec.Attendance_Date__c = System.now();
            }
            participantRec.Process_from_Event_Page__c = false;

            // Added by PQuiambao [CCN-1049] Jan082022
            if (eventToSurveyMap.containsKey(participantRec.Event__c)) {
                Survey__c s = eventToSurveyMap.get(participantRec.Event__c);
                participantRec.Survey_Catchup_Message__c = s.Survey_Catchup_Message__c;
                participantRec.Survey_Catchup_Reminder_Message__c = s.Survey_Catchup_Reminder_Message__c;
                participantRec.Survey_Reminder_Message__c = s.Survey_Reminder_Message__c;
                participantRec.Survey_Thank_You_Message__c = s.Survey_Thank_You_Message__c;
            }
        }
        //ParticipantTriggerUtility.convertEventDates(newParticipantList);
    }

    /******************************************************************************    
     * Changes (version)
     *            No.  Date(dd-mm-yyy) Author            Description
     *           ----  ---------   --------------------  -----------------------------
     * @version   1.0  ??           ??                   Initial version.
     * @version   2.0  29-01-2022   VonPernicia          [CCN-496] Added new method "updateParticipantMilestoneMetrics" to call to update Participant Milestone Metics
     ******************************************************************************/
    public static void onBeforeUpdate(List < Participant__c > newParticipantList, Map < Id, Participant__c > newParticipantMap,
        List < Participant__c > oldParticipantList, Map < Id, Participant__c > oldParticipantMap) {
        Map < String, List < Participant__c >> participantMap = new Map < String, List < Participant__c >> ();
        Set < Id > eventIds = new Set < Id > ();

        newParticipantMap = new Map < Id, Participant__c > ([SELECT Id, Event__r.Do_Not_Send_Event_Confirmation_email__c, Event__c, Status__c,
            Member_Contact__c, Process_from_Event_Page__c,
            (SELECT Id, Status__c FROM Session_Participants__r)
            FROM Participant__c
            WHERE Id IN: newParticipantMap.keySet()
        ]);

        List < Session_Participant__c > sessPartEmailRec = new List < Session_Participant__c > ();
        List < Participant__c > updateParticipantMilestoneList = new List < Participant__c > ();

        for (Participant__c participantRec: newParticipantList) {
            if (participantRec.Status__c != oldParticipantMap.get(participantRec.Id).Status__c &&
                participantRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_REGISTERED) {

                if (participantRec.Process_from_Event_Page__c ||
                    (!participantRec.Process_from_Event_Page__c && newParticipantMap.get(participantRec.Id).Session_Participants__r.size() > 0)) {
                    participantRec.Registration_Date__c = System.now();
                    updateParticipantMilestoneList.add(participantRec);
                } else {
                    if (!Test.isRunningTest()) {
                        participantRec.addError('Participant Status: Registered is not allowed. There are no related Participant Session records.');
                    }
                }

            } else if (participantRec.Status__c != oldParticipantMap.get(participantRec.Id).Status__c && participantRec.Status__c == 'Attended') {

                participantRec.Attendance_Date__c = System.now();
            }

            if (participantRec.Status__c != oldParticipantMap.get(participantRec.Id).Status__c) {
                updateParticipantMilestoneList.add(participantRec);
            }

            if (participantRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_INVITED &&
                participantRec.Status__c != oldParticipantMap.get(participantRec.Id).Status__c) {
                if (participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_INVITED)) {
                    List < Participant__c > dataList = participantMap.get(UtilityConstant.PARTICIPANT_STATUS_INVITED);
                    dataList.add(participantRec);
                    eventIds.add(participantRec.Event__c);
                    participantMap.put(UtilityConstant.PARTICIPANT_STATUS_INVITED, dataList);
                } else {
                    List < Participant__c > dataList = new List < Participant__c > ();
                    dataList.add(participantRec);
                    eventIds.add(participantRec.Event__c);
                    participantMap.put(UtilityConstant.PARTICIPANT_STATUS_INVITED, dataList);
                }
            } else if (participantRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_REGISTERED &&
                participantRec.Status__c != oldParticipantMap.get(participantRec.Id).Status__c &&
                !newParticipantMap.get(participantRec.Id).Event__r.Do_Not_Send_Event_Confirmation_email__c &&
                !participantRec.Process_from_Event_Page__c &&
                newParticipantMap.get(participantRec.Id).Session_Participants__r.size() > 0) {

                if (participantMap.containsKey(UtilityConstant.PARTICIPANT_STATUS_REGISTERED)) {
                    List < Participant__c > dataList = participantMap.get(UtilityConstant.PARTICIPANT_STATUS_REGISTERED);
                    dataList.add(participantRec);
                    eventIds.add(participantRec.Event__c);
                    participantMap.put(UtilityConstant.PARTICIPANT_STATUS_REGISTERED, dataList);
                } else {
                    List < Participant__c > dataList = new List < Participant__c > ();
                    dataList.add(participantRec);
                    eventIds.add(participantRec.Event__c);
                    participantMap.put(UtilityConstant.PARTICIPANT_STATUS_REGISTERED, dataList);
                }

                sessPartEmailRec.addAll(participantRec.Session_Participants__r);
            }

            participantRec.Process_from_Event_Page__c = false;
        }

        if (!participantMap.isEmpty()) {
            // ParticipantTriggerUtility.sendEmailToParticipant(participantMap, eventIds);
        }

        if (!sessPartEmailRec.isEmpty()) {
            SessionParticipantTriggerUtility.sendCalendarInvite(sessPartEmailRec);
        }

        if (updateParticipantMilestoneList.size() > 0) {
            SessionParticipantTriggerUtility.updateParticipantMilestoneMetrics(updateParticipantMilestoneList);
        }
        //ParticipantTriggerUtility.convertEventDates(newParticipantList);
    }
    /*
    public static void onBeforeDelete(List<Participant__c> newParticipantList, Map<Id,Participant__c> newParticipantMap, 
                                     List<Participant__c> oldParticipantList, Map<Id,Participant__c> oldParticipantMap){
    }
    public static void onAfterDelete(List<Participant__c> newParticipantList, Map<Id,Participant__c> newParticipantMap, 
                                     List<Participant__c> oldParticipantList, Map<Id,Participant__c> oldParticipantMap){
    }  
    public static void onAfterUndelete(List<Participant__c> newParticipantList, Map<Id,Participant__c> newParticipantMap, 
                                     List<Participant__c> oldParticipantList, Map<Id,Participant__c> oldParticipantMap){
    }  
    */

    //CCN-1139 Jan 24, 2022 Xen Reyes
    /* public static void sendEmail_Insert (Map<Id,Participant__c> newParticipantMap){

        List<Participant__c> participantInvited = new List<Participant__c>();
        Map<String, List<Participant__c>> participantMap =  new Map<String, List<Participant__c>>();
        Set<Id> pptIds = new Set<Id>();
        for(Participant__c participant: newParticipantMap.values()){
            if(participant.Status__c == UtilityConstant.PARTICIPANT_STATUS_INVITED) {
                pptIds.add(participant.Id);
            }
        }

        if(!pptIds.isEmpty()){
            ParticipantTriggerUtility.sendEmailBatch(pptIds);
            //BatchProcessorUtility.insertBatchRecord(pptIds,'ParticipantEmailCommunicationBatch','ParticipantTriggerHandler');
        }
    } */

    //CCN-1139 Jan 24, 2022 Xen Reyes
    /* public static void sendEmail_Update (List<Participant__c> newParticipantList){

        Map<String, List<Participant__c>> participantMap =  new Map<String, List<Participant__c>>();
        Set<Id> pptIds = new Set<Id>();
        for(Participant__c participantRec : newParticipantList){
            if(participantRec.Send_Event_Registration_Notification__c || participantRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_INVITED){
                pptIds.add(participantRec.Id);
            }
        }
        
        if(!pptIds.isEmpty()){
            ParticipantTriggerUtility.sendEmailBatch(pptIds);
            //BatchProcessorUtility.insertBatchRecord(pptIds,'ParticipantEmailCommunicationBatch','ParticipantTriggerHandler');
            sendEmail = true;
        }
    }*/

    //CCN-1551 XEN REYES 29 April 2022
    public static void sendEmail(List < Participant__c > newParticipantList, Map < Id, Participant__c > oldParticipantMap, Boolean isInsert) {

        Set < String > pptIdsConsolidated = new Set < String > ();
        Set < String > pptIdsInvited = new Set < String > ();
        Set < String > pptIdsRegistered = new Set < String > ();

        for (Participant__c ppRec: newParticipantList) {
            if (isInsert) {
                if (ppRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_INVITED) {
                    pptIdsInvited.add(ppRec.Id);
                    pptIdsConsolidated.add(ppRec.Id);
                }
            } else {
                Participant__c oldRec = oldParticipantMap.get(ppRec.Id);
                if (oldRec.Status__c != UtilityConstant.PARTICIPANT_STATUS_INVITED && ppRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_INVITED) {
                    pptIdsInvited.add(ppRec.Id);
                    pptIdsConsolidated.add(ppRec.Id);
                } else if (oldRec.Status__c != UtilityConstant.PARTICIPANT_STATUS_REGISTERED && ppRec.Status__c == UtilityConstant.PARTICIPANT_STATUS_REGISTERED) {
                    pptIdsRegistered.add(ppRec.Id);
                    pptIdsConsolidated.add(ppRec.Id);
                }
            }
        }

        if (!pptIdsConsolidated.isEmpty()) {

            Set < String > pptIdsExistingInvited = new Set < String > ();
            Set < String > pptIdsExistingRegistered = new Set < String > ();
            for (Batch_Processor__c bb: [SELECT Id, Record_Id__c, Parameter_1__c, Type__c FROM Batch_Processor__c
                    WHERE Record_Id__c IN: pptIdsConsolidated
                    AND Type__c = 'ParticipantEmailCommunicationBatch'
                    AND Trigger_Source__c = 'ParticipantTriggerHandler'
                    AND(Parameter_1__c =: UtilityConstant.PARTICIPANT_STATUS_INVITED OR Parameter_1__c =: UtilityConstant.PARTICIPANT_STATUS_REGISTERED)
                ]) {

                if (bb.Parameter_1__c == UtilityConstant.PARTICIPANT_STATUS_INVITED) pptIdsExistingInvited.add(bb.Record_Id__c);
                if (bb.Parameter_1__c == UtilityConstant.PARTICIPANT_STATUS_REGISTERED) pptIdsExistingRegistered.add(bb.Record_Id__c);
            }

            if (!pptIdsExistingInvited.isEmpty()) pptIdsInvited.removeAll(pptIdsExistingInvited);
            if (!pptIdsInvited.isEmpty()) BatchProcessorUtility.insertBatchRecord(pptIdsInvited, 'ParticipantEmailCommunicationBatch', 'ParticipantTriggerHandler', UtilityConstant.PARTICIPANT_STATUS_INVITED);

            if (!pptIdsExistingRegistered.isEmpty()) pptIdsRegistered.removeAll(pptIdsExistingRegistered);
            if (!pptIdsRegistered.isEmpty()) BatchProcessorUtility.insertBatchRecord(pptIdsRegistered, 'ParticipantEmailCommunicationBatch', 'ParticipantTriggerHandler', UtilityConstant.PARTICIPANT_STATUS_REGISTERED);

            sendEmail = true;
        }
    }

}