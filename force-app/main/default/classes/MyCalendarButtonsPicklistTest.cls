@isTest
public class MyCalendarButtonsPicklistTest {

    static testMethod void getDefaultValueTest(){
        VisualEditor.DataRow dr = new VisualEditor.DataRow('Day, Week & Month', 'agendaDay,agendaWeek,month');
        MyCalendarButtonsPicklist mcbpl = new MyCalendarButtonsPicklist();
        dr = mcbpl.getDefaultValue();
        System.assert(dr != null);
    }

    static testMethod void getValuesTest(){
        VisualEditor.DynamicPickListRows dplr = new VisualEditor.DynamicPickListRows();
           MyCalendarButtonsPicklist mcbpl = new MyCalendarButtonsPicklist();
       	   dplr = mcbpl.getValues();
           System.assert(dplr != null);
    }
}