/*******************************************************************************************
  * @name: NavigatorController
  * @author: Jester Cabantog
  * @created: 25-01-2021
  * @description: This is the Main Class for Navigator Component pages
  *
  * Changes (version)
  * -------------------------------------------------------------------------------------------
  *            No.   Date(dd-mm-yyy)  Author                Description
  *            ----  ---------        --------------------  -----------------------------
  * @version   1.0   25-01-2021       Jester Cabantog       [00675] Initial version.
  *********************************************************************************************/
public without sharing class NavigatorController {

    /*******************************************************************************************
    * @name: NavigationItem
    * @author: Jayson Labnao
    * @created: 19-06-2021
    * @description: Wrapper class for Navigator Items (Tabs)
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   19-06-2021       Jayson Labnao         [00675] Initial version.
    *********************************************************************************************/
    public class NavigationItem{
        @AuraEnabled
        public Navigator_Item__c NavigatorItem {get; set;}
        @AuraEnabled
        public String IconUrl {get; set;}
    }


    /*******************************************************************************************
    * @name: getLoginPageInfo
    * @author: Jayson Labnao
    * @created: 21-02-2022
    * @description: Retrieve the navigator configuration detail/s
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   21-02-2022       Jayson Labnao         [CCN-1146] Initial version.
    * @version   2.0   03-10-2023       Jayson Labnao         [CCN-3417] Added 3 fields in the SOQL
    *********************************************************************************************/
    @AuraEnabled
    public static NavigatorUtility.NavigatorWrapper getLoginPageInfo(String navigatorId){
        Navigator__c navi = [SELECT Id, Enable_Token_Login__c, App_Logo__c, Branding_Logo__c, Landing_Page_Hero_Image_Link__c FROM Navigator__c WHERE Id = :navigatorId];
        return NavigatorUtility.buildNavigatorObj(navi);
    }

    /*******************************************************************************************
    * @name: getNavigationDetailsWithVerif
    * @author: Jester Cabantog
    * @created: 25-01-2021
    * @description: Verifies if the contact exist and if the login token is valid, if token login
    *               feature is enabled. Retrieves the Navigator Details, Contact Infos, and the 
    *               Navigation Tabs Navigator Items using getNavigationItems method.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   25-01-2021       Jester Cabantog       [00675] Initial version.
    * @version   2.0   21-02-2022       Jayson Labnao         [CCN-1146] Updated logic for login 
    *                                                         token feature
    *********************************************************************************************/
    @AuraEnabled 
    public static String getNavigationDetailsWithVerif(String contactId, String token){
        
        Contact customerContact = [
            SELECT Id, Contact_Id__c, Title, Name, FirstName, 
                LastName, Login_Token__c, Profile_Picture_URL__c, 
                Navigator__c, Navigator__r.Enable_Token_Login__c 
            FROM Contact 
            WHERE Contact_Id__c = :contactId 
            LIMIT 1
        ];
                                                                                                
        if(customerContact == null){
            return 'User does not exist.';
        }

        /* START - Jonah Baldero -  CCN-NAV-3417-DV - Sept 18, 2023
        if(customerContact.Navigator__r.Enable_Token_Login__c && customerContact.Login_Token__c != token){
            return 'Invalid Token';
        }
		END - Jonah Baldero -  CCN-NAV-3417-DV */

        return JSON.serialize(getNavigationItems(customerContact));  
    }
    
    /*******************************************************************************************
    * @name: getNavigationItems
    * @author: Jester Cabantog
    * @created: 25-01-2021
    * @description: Retrieves the Navigator Details, Contact Infos, and the Navigation Tabs
    *               Navigator Items
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   25-01-2021       Jester Cabantog       [00675] Initial version.
    * @version   2.1   19-06-2021       Jayson Labnao         Updated queries to get new details 
    *            from additional fields such as Branding_Logo__c, App_Logo__c, AWS_Link__c etc.
    * @version   2.2   28-03-2021       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    private static Map<String,Object> getNavigationItems(Contact customerContact){
        Map<String,Object> returnMap = new Map <String,Object>();
        try{
            //Add Contact details
            returnMap.put('contact', NavigatorUtility.buildContactObj(customerContact));
            
            //Add Navigator details
            Navigator__c navigationRecord;
            try{
                navigationRecord = [
                    SELECT Id, Name, Header_Text__c, Header_Image__c,
                        Footer_Image__c, Branding_Logo__c, App_Logo__c, 
                        AWS_Link__c, Mascot_Image__c, Mascot_Message__c,
                        Theme_Color_1__c, Theme_Color_2__c, Theme_Color_3__c, 
                        Theme_Color_4__c, Login_Page_URL__c
                    FROM Navigator__c 
                    WHERE Id = :customerContact.Navigator__c
                ];
                returnMap.put('navigationRecord', NavigatorUtility.buildNavigatorObj(navigationRecord));
            }
            catch(QueryException e){
                if(navigationRecord == null){
                    throw new AuraHandledException('No Navigation record for the user');    
                }
            }
            //Add Navigator Tabs for sidebar
            Map<Id, Navigator_Item__c> navigatorItems = new Map<Id, Navigator_Item__c>([
                SELECT Id, Name, Navigator__c, Sort_Order__c, 
                    Page_URL__c, SLDS_Icon_Name__c,Expanded_Items__c,Image_URL__c
                FROM Navigator_Item__c 
                WHERE Navigator__r.Id = :navigationRecord.Id
                ORDER BY Sort_Order__c ASC
            ]);

            List<NavigatorUtility.NavigatorItemWrapper> navTabList = new List<NavigatorUtility.NavigatorItemWrapper>();
            for(Navigator_Item__c navItem : navigatorItems.values()){
                navTabList.add(NavigatorUtility.buildNavItemWrapper(navItem));
            }

            returnMap.put('navItemList', navTabList);
        }
        catch(exception e){
            throw new AuraHandledException(e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        return returnMap;
    }
    
    /*******************************************************************************************
    * @name: sendTokenCode
    * @author: Jester Cabantog
    * @created: 25-01-2021
    * @description: Creates the token code and emails it to the user.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   25-01-2021       Jester Cabantog       [00675] Initial version.
    * @version   2.0   23-02-2022       Jayson Labnao         Updated login token generation logic
    *********************************************************************************************/
    @AuraEnabled  
    public static string sendTokenCode(String email){
        try{
            List<Contact> conList = [
                SELECT Name, Email, Login_Token__c 
                FROM Contact 
                WHERE Email = :email AND Login_Token__c != ''
            ];

            if(!conList.isEmpty()){
                
                Contact conRec = conList[0];
                Integer len = 32;
                Blob blobKey = crypto.generateAesKey(128);
                conRec.Login_Token__c = EncodingUtil.convertToHex(blobKey).substring(0, len);
                
                Update conRec;

                //Send Email
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {email}; 
                mail.setToAddresses(toAddresses);
                mail.setReplyTo('noreply@salesforce.com');
                mail.setSenderDisplayName('NCC Support');
                mail.setSubject('Login Token for ' + conRec.Name);
                mail.setPlainTextBody('Your token: ' + conRec.Login_Token__c);
                mail.setHtmlBody('Your token: ' +  conRec.Login_Token__c);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
            }
            
            else{
                return 'INVALID CONTACT';
            }
        }
        catch(exception e){
            throw new AuraHandledException('Error Sending Email');
        }
        return 'SUCCESS';
    }
    
    /*******************************************************************************************
    * @name: doLogin
    * @author: Jester Cabantog
    * @created: 25-01-2021
    * @description: Verifies the user's email and token (if login token is enabled), if contact/user 
    *               with the correct login token exist then contact record along with navigator Id
    *               is returned.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   25-01-2021       Jester Cabantog       [00675] Initial version.
    * @version   2.0   23-02-2022       Jayson Labnao         Updated login token generation logic
    * @version   2.1.  03-11-2023       Jonah Belle Baldero   Updated logic for user redirect upon login
    *********************************************************************************************/
    @AuraEnabled  
    //public static string doLogin(String email, string token){
	public static string doLogin(String email, string navigatorId){
        Contact navigatorUser;
        try{
            navigatorUser = [
                SELECT Id, Name, Email, Login_Token__c, Navigator__c,
                    Contact_Id__c, Navigator__r.Enable_Token_Login__c
                FROM Contact 
                WHERE Email = :email
                AND Navigator__c =: navigatorId LIMIT 1 //Jonah Baldero -  CCN-NAV-3417-DV - Nov 16, 2023
            ];
        }
        catch(Exception ex){
            //START - Oct 4, 2023 - Jonah - CCN-NAV-3417-DV
            	//throw new AuraHandledException('Invalid email or account does not exist. Please try again');
            	throw new AuraHandledException ('Error! Participant not found. Kindly reach out to your project team.');
            //END - Oct 4, 2023 - Jonah - CCN-NAV-3417-DV
        }

        /* START - Jonah Baldero -  CCN-NAV-3417-DV - Sept 18, 2023
            if(navigatorUser.Navigator__r.Enable_Token_Login__c && navigatorUser.Login_Token__c != token){
                throw new AuraHandledException('Invalid Token. Please try again');
            }
		END - Jonah Baldero -  CCN-NAV-3417-DV */

        Map<String,Object> returnMap = new Map <String,Object>();
        String navigationId = navigatorUser.Navigator__c;
        System.debug('navigatorId: '+navigationId);
        returnMap.put('contact', NavigatorUtility.buildContactObj(navigatorUser));
        //returnMap.put('navigatorId', navigatorUser.Navigator__c); jj
        returnMap.put('navigatorId', navigationId);

        //Get Home Community URL
        String baseUrl ='';
        
        //START - Jonah Baldero - CCN-NAV-3417-DV - Nov 3, 2023
        
        List<Network> network = [SELECT Name, UrlPathPrefix FROM Network WHERE Name = 'Navigator'];
		String base = URL.getSalesforceBaseUrl().toExternalForm(); 
        
        if (!network.isEmpty()){
            //baseUrl = base + '/' + network[0].UrlPathPrefix + '/s/navigator-home?id' + Navigator__c.Id + '&contactId' + navigatorUser.Id;
            baseUrl = base + '/' + network[0].UrlPathPrefix + '/s/navigator-home';
        }
    	
       /* for(Navigator_Item__c nav : [
            SELECT Id, Name, Navigator__c, Sort_Order__c, Page_URL__c, SLDS_Icon_Name__c 
            FROM Navigator_Item__c 
            WHERE Navigator__r.Id = :navigationId 
                AND Sort_Order__c = 1
        ]){
            baseUrl = nav.Page_URL__c;
        }*/
        //END - Jonah Baldero - CCN-NAV-3417-DV - Nov 3, 2023

        if(String.isBlank(baseUrl)){
            //START - Oct 4, 2023 - Jonah - CCN-NAV-3417-DV
            	//throw new AuraHandledException('Contact is not associated to any Navigator');
            	throw new AuraHandledException ('11Error! Participant not found. Kindly reach out to your project team.');
            //END - Oct 4, 2023 - Jonah - CCN-NAV-3417-DV
        }

        returnMap.put('baseUrl', baseUrl);

        return JSON.serialize(returnMap);        
    }
    
    /*******************************************************************************
   * @author       Leif Erickson de Gracia
   * @date         10/02/2023
   * @description  Get related navigator widget records
   * @revision     11/02/2023 - Leif Erickson de Gracia - Finalized
   *******************************************************************************/

    @AuraEnabled
    public static String getNavigatorWidgets(Id navigatorId, Id contactId){
        
        Set<String> SobjectFields = Schema.getGlobalDescribe().get('Navigator_Widget__c').getDescribe().fields.getMap().keySet();    
        List<String> fieldsInList = new List<String>(SobjectFields);
        List<Navigator_Widget__c> navigatorWidgetList = Database.query('SELECT ' + String.join(fieldsInList, ',') + ',Navigator__r.Campaign__c FROM Navigator_Widget__c WHERE Navigator__c =: navigatorId AND Is_Active__c = true');
        
        return JSON.serialize(navigatorWidgetList);
    }
}