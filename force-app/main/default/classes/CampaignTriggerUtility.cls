/*******************************************************************************************
  * @name: CampaignTriggerHandler 
  * @author: Minh
  * @created: 23-11-2021
  * @description: untility for CampaignTriggerHandler
  *
  * Changes (version)
  * -------------------------------------------------------------------------------------------
  *            No.  Date(dd-mm-yyy) Author         Description
  *            ----  ---------   --------------------  -----------------------------
  * @version   1.0  23-11-2021   Minh                Initial version.
  *********************************************************************************************/

public class CampaignTriggerUtility {

    public static void setSiteURL(List<Campaign__c> objList)
    {
        string siteURL = getCommunityURL('compass');
        for (Campaign__c oCampaign : objList ) {
            oCampaign.Campaign_URL__c = siteUrl + '/s/summarypage?id=' + oCampaign.Id;
        } 
    }
    
    public static String getCommunityURL(string communityName) {
        
        ConnectApi.Community myCommunity;
        string CommunityURL;
        try {
            Network myNetwork = [SELECT Id FROM Network WHERE Name = :communityName];
            
            if (!Test.isRunningTest()) {
                myCommunity = ConnectApi.Communities.getCommunity(myNetwork.id);
                CommunityURL = myCommunity.siteUrl;
            }
            else
                CommunityURL = 'some random url';
        
        } catch (Exception err) {
            System.Debug('TRY-CATCH-Exception-->' + err);
            CommunityURL = 'TRY-CATCH-Exception-->' + err;
        }
        
        return CommunityURL;
    }
}