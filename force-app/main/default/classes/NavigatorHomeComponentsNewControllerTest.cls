@isTest
public class NavigatorHomeComponentsNewControllerTest {
    @testSetup
    static void createData(){

        Apex_Trigger_Switch__c ats_pt = new Apex_Trigger_Switch__c();
        ats_pt.Name = 'ParticipantTrigger';
        ats_pt.Active__c = true;
        insert ats_pt;

        Apex_Trigger_Switch__c ats_sp = new Apex_Trigger_Switch__c();
        ats_sp.Name = 'SessionParticipantTrigger';
        ats_sp.Active__c = true;
        insert ats_sp;
        
        Apex_Trigger_Switch__c ats_pl = new Apex_Trigger_Switch__c();
        ats_pl.Name = 'ParkingLotTrigger';
        ats_pl.Active__c = true;
        insert ats_pl;

        Account acc = TestUtility.createAccRec('TestAccount');
        insert acc;
        
        Contact testContact = NavigatorTestDataFactory.createContact('testing', 'testing@test.com', 'ABCD1234');
        testContact.Contact_Id__c = '9LIshQ4sJWP';
        insert testContact;
        
        OrgWideEmailAddress owea = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        EmailTemplate eTemp = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test', IsActive = true);
        /*system.runAs( new User(Id = UserInfo.getUserId())){
        	insert eTemp;
        }*/
        
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/New_York', UserName='testUserNavigator@testorg.com');
        
        system.runAs(u){
            insert eTemp;
            Compass_Setting__c setting = new Compass_Setting__c();
            setting.Name = 'Default Settings';
            setting.Email_Sender_Name__c = 'NAME';
            setting.Email_Template_Invitation_Id__c = eTemp.Id;
            setting.Email_Template_Registration_Id__c = eTemp.Id;
            setting.Email_Sender_Id__c = owea.Id;
            setting.Email_Template_Issue_Id__c = eTemp.Id;
            setting.Email_Template_Parking_Lot_Assigned__c = eTemp.Id;
            setting.Email_Template_Parking_Lot_Closed__c = eTemp.Id;
            setting.Email_Template_Parking_Lot_Open__c = eTemp.Id;
            setting.Default__c = true;
            insert setting;
            
            Campaign__c testCampaign = NavigatorTestDataFactory.createCampaign('testCampaign');
            testCampaign.Account__c = acc.Id;
            insert testCampaign;
            
            Journey__c testJourney = NavigatorTestDataFactory.createJourney('testJourney', testCampaign.Id, 'Approved');
            testJourney.End_Date__c = Date.valueOf(System.DateTime.now().addDays(5));
            insert testJourney;
            
            Event__c testEvent = NavigatorTestDataFactory.createEvent('testEvent', testCampaign.Id, '1234ABCD', System.today());
            testEvent.Time_Zone__c = 'CET';
            insert testEvent;

            Participant__c pp =  new Participant__c();
            pp.Member_Contact__c = testContact.Id;
            pp.Event__c = testEvent.Id;
            pp.Send_Event_Registration_Notification__c = true;
            pp.Status__c = 'Invited';
            insert pp;
            
            Session__c testSession = NavigatorTestDataFactory.createSession('testSession', testEvent.Id, System.today());
            testSession.IsActive__c = TRUE;
            testSession.Start_Date_Time__c = System.now();
            testSession.End_Date_Time__c = System.now().addHours(1);
            testSession.Time_Zone__c = 'CET';
            insert testSession;
            
            Session_Participant__c sessionParticipant = new Session_Participant__c();
            sessionParticipant.Event__c = testEvent.Id;
            sessionParticipant.Session__c = testSession.Id;
            sessionParticipant.Contact__c = testContact.Id;
            sessionParticipant.Status__c = 'Registered';
            insert sessionParticipant;
            
            Milestone__c milestone = new Milestone__c();
            milestone.Journey__c = testJourney.Id;
            milestone.Name = 'testMilestone';
            milestone.Type__c = 'Event';
            milestone.Related_RecordId__c = testContact.Id;
            insert milestone;
            
            Journey_Participant__c testParticipant = NavigatorTestDataFactory.createJourneyParticipant(testJourney.Id, testContact.Id);
            Insert testParticipant;
            
            Participant_Milestone__c participantMilestone = new Participant_Milestone__c();
            participantMilestone.Milestone__c = milestone.Id; 
            participantMilestone.Contact__c = testContact.Id;
            participantMilestone.Is_Active__c = TRUE;
            participantMilestone.Start_Date__c = System.Date.today().addDays(-2);
            participantMilestone.End_Date__c = System.Date.today().addDays(2);
            participantMilestone.Journey__c = testParticipant.Id;
            participantMilestone.Checkpoint__c = true;
            insert participantMilestone;
            
            Navigator__c navigator = new Navigator__c();
            navigator.Name = 'TestNavigator';
            navigator.Campaign__c = testCampaign.Id;
            navigator.Change_Agent_Lead_Label__c = 'Test';
            navigator.About_Label__c = 'TestAbout';
            navigator.About_Image_Video_Link__c = 'salesforce.com';
            insert navigator;
            
            Map<Id, Batch_Processor__c> bpMap = new Map<Id, Batch_Processor__c>([SELECT Id FROM Batch_Processor__c]);
            System.debug(bpMap);
            Database.executeBatch(new ParticipantEmailCommunicationBatch(bpMap.keySet()), 1);

            Issue__c iss = new Issue__c();
            iss.Event__c = testEvent.Id;
            iss.Session__c = testSession.Id;
            iss.Date_Raised__c = System.today();
            iss.Raised_By__c = testContact.Id;
            iss.Raised_By_Email__c = testContact.Email;
            iss.Type__c = 'Workflow Issue';
            iss.Description__c = 'Test Description';
            iss.Resolution__c = 'Test Resolution';
            iss.Status__c = 'New';
            insert iss;

            iss.Status__c = 'Addressed';
            update iss;

            Parking_Lot__c pl = new Parking_Lot__c();
            pl.Event__c = testEvent.Id;
            pl.Session__c = testSession.Id;
            pl.Type__c = 'Issue';
            pl.Description__c = 'Test Description';
            pl.Resolution_Answer__c = 'Test Resolution';
            pl.Status__c = 'Open';
            pl.Raised_By__c = testContact.Id;
            pl.Assigned_To__c = testContact.Id;
            pl.Addressed_By__c = testContact.Id;
            insert pl;

            pl.Status__c = 'Closed';
            update pl;
        }
    }
    
    @isTest static void testGetEventRecordDetails() {
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id; 
            navWidget.Widget_Name__c = 'Event Activity';
            navWidget.Widget_Label__c = 'Event Activity';
            navWidget.Widget_View__c = 'Activity Date List';
            navWidget.Widget_Size__c = 'Classic Top Center S1';
            navWidget.Widget_Parent_Object__c = 'Event__c';
            navWidget.Widget_Child_Object__c = 'Session_Participant__c';
            navWidget.Conditions__c = 'Event/Session Registration Status';
            insert navWidget;
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getRecordDetails(JSON.serialize(navWidget), '9LIshQ4sJWP');
            Test.stopTest();
        }
    }
    
    @isTest static void testGetJourneyRecordDetails() {
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            List<Participant_Milestone__c> participantMilestoneList = [SELECT Id, Name, Milestone_Status__c FROM Participant_Milestone__c WHERE Contact__r.Contact_Id__c = '9LIshQ4sJWP'];
            
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id;
            navWidget.Widget_Name__c = 'Journey Milestones Ongoing';
            navWidget.Widget_Label__c = 'Journey Widget';
            navWidget.Widget_View__c = 'Activity List/Links';
            navWidget.Widget_Parent_Object__c = 'Journey__c';
            navWidget.Widget_Child_Object__c = 'Participant_Milestone__c';
            navWidget.Conditions__c = participantMilestoneList[0].Milestone_Status__c;
            insert navWidget;
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getRecordDetails(JSON.serialize(navWidget), '9LIshQ4sJWP');
            Test.stopTest();
        }
    }
    
    @isTest static void testGetCommunicationRecordDetails() {   
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';     
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id;
            navWidget.Widget_Label__c = 'Email Widget';
            navWidget.Widget_Name__c = 'Email Activity';
            navWidget.Widget_Parent_Object__c = 'Contact';
            navWidget.Widget_Child_Object__c = 'Communication_Recipient__c';
            insert navWidget;
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getRecordDetails(JSON.serialize(navWidget), '9LIshQ4sJWP');
            Test.stopTest();
        }
    }
    
    @isTest static void testGetEmailMessageDetails() {   
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';     
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id;
            navWidget.Widget_Label__c = 'Email Widget';
            navWidget.Widget_Name__c = 'Email Activity';
            navWidget.Widget_Parent_Object__c = 'Contact';
            navWidget.Widget_Child_Object__c = 'EmailMessage';
            insert navWidget;

            Navigator_Widget__c navWidgetQuery = [SELECT Id, Navigator__r.Campaign__c, Widget_Label__c, Widget_Name__c, Widget_Parent_Object__c, Widget_Child_Object__c FROM Navigator_Widget__c WHERE Id =: navWidget.Id LIMIT 1];
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getRecordDetails(JSON.serialize(navWidgetQuery), '9LIshQ4sJWP');
            Test.stopTest();
        }
    }
    
    @isTest static void testGetMilestones() {
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id;
            navWidget.Widget_Label__c = 'Email Widget';
            navWidget.Widget_Name__c = 'Email Activity';
            navWidget.Widget_Parent_Object__c = 'Contact';
            navWidget.Widget_Child_Object__c = 'Communication_Recipient__c';
            insert navWidget;
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getMilestones(navWidget, '9LIshQ4sJWP');
            Test.stopTest();
        }
    }
    
    @isTest static void testGetRelatedWidgetsDetails() {
        User u = [SELECT Id FROM User WHERE Username = 'testUserNavigator@testorg.com'];
        
        system.runAs(u){
            String query = DynamicSOQL('Navigator__c');
            query += ' WHERE name = \'TestNavigator\' LIMIT 1';
            Navigator__c navigator = Database.query(query);
            
            Navigator_Widget__c navWidget = new Navigator_Widget__c();
            navWidget.Navigator__c = navigator.Id;
            navWidget.Widget_Label__c = 'Email Widget';
            navWidget.Widget_Name__c = 'Email Activity';
            navWidget.Widget_Parent_Object__c = 'Contact';
            navWidget.Widget_Child_Object__c = 'Communication_Recipient__c';
            insert navWidget;
            
            Test.startTest();
            NavigatorHomeComponentsNewController.getRelatedWidgets(navigator.id);
            Test.stopTest();
        }
    }
    
    private static String DynamicSOQL(String objectName) 
    {
        
        String SobjectApiName = objectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        List<String>apiNames =  new list<String>();
        
        for(String apiName : fieldMap.keyset())
        {
            apiNames.add(apiName);
        }
        
        string allstring = string.join(apiNames,',');
        
        String query = 'SELECT ' + allstring + ' FROM ' + SobjectApiName;
        
        return query;
    }
}