@isTest(IsParallel=true)
public with sharing class CustomReusableLookupControllerTest {
  @testSetup
  public static void setupData() {
    //insert at least 2 types of object to check flexibility of the method to query different objects
    TestFactory.createContact('Test Fname', 'Lname', 'test@test.com.invalid');
    TestFactory.createContact(
      'Diff Test Fname',
      'Diff Lname',
      'test@test.com.invalid'
    );
    TestFactory.createSurvey(null, 'Test Survey');
    TestFactory.createSurvey(null, '2 Test Survey');
  }

  @isTest
  public static void testfetchLookUpValues_shouldReturnContact_whenFirstNameIsProvided() {
    Test.startTest();
    List<Contact> contactList = (List<Contact>) CustomReusableLookupController.fetchLookUpValues(
      'Test',
      'Contact'
    );
    Test.stopTest();

    System.assertEquals(
      1,
      contactList.size(),
      'There should only be one returned result for Contact First Name'
    );
  }

  @isTest
  public static void testfetchLookUpValues_shouldReturnContact_whenLastNameIsProvided() {
    Test.startTest();
    List<Contact> contactList = (List<Contact>) CustomReusableLookupController.fetchLookUpValues(
      'Diff L',
      'Contact'
    );
    Test.stopTest();

    System.assertEquals(
      1,
      contactList.size(),
      'There should only be one returned result for Contact Last Name'
    );
  }

  @isTest
  public static void testfetchLookUpValues_shouldReturnContact_whenFullNameIsProvided() {
    Test.startTest();
    List<Contact> contactList = (List<Contact>) CustomReusableLookupController.fetchLookUpValues(
      'Diff Test Fname Diff Lname',
      'Contact'
    );
    Test.stopTest();

    System.assertEquals(
      1,
      contactList.size(),
      'There should only be one returned result for Contact Name'
    );
  }

  @isTest
  public static void testfetchLookUpValues_shouldReturnResult_whenSearchKeywordIsProvided() {
    Test.startTest();
    List<Survey__c> surveyList = (List<Survey__c>) CustomReusableLookupController.fetchLookUpValues(
      'Test Survey',
      FlsUtils.prefix + 'Survey__c'
    );
    Test.stopTest();

    System.assertEquals(
      1,
      surveyList.size(),
      'There should only be one returned result for Survey'
    );
  }

  @isTest
  public static void testfetchLookUpValues_shouldReturnAllResult_whenNoSearchKeywordIsProvided() {
    Test.startTest();
    List<Survey__c> surveyList = (List<Survey__c>) CustomReusableLookupController.fetchLookUpValues(
      '',
      FlsUtils.prefix + 'Survey__c'
    );
    Test.stopTest();

    System.assertEquals(
      2,
      surveyList.size(),
      'There should only be one returned result for Survey'
    );
  }
}