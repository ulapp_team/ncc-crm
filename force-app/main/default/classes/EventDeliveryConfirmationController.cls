/******************************************************************************    
    * Changes (version)
    *            No.  Date(dd-mm-yyyy)          Author                      Description
    *           ----  -----------------    -------------------       -----------------------------
    * @version   1.0  23-06-2022             Von Pernicia            [CCN-EVE-2390-DV] Intial Version. Controller to pull information that will be display in the email template
    ******************************************************************************/ 
public with sharing class EventDeliveryConfirmationController {
    public String internalResourceId;
    public List<EventDeliveryConfirmationHelper.SessionWrapper> Sessions;

    public String campaignOwnerId {get;set;} 
    public String campaignId {get;set;}
    public String defined_format {get;set;} 
    public String FormattedDatetime;
    public String startOrEndDate {get; set;}
    public String timeOrDate {get; set;}
    public String confirmAllLink {get;set;} 
    public String declineAllLink {get;set;} 
    public Boolean showConfirmDeclineAllButton {get;set;} 

    public EventDeliveryConfirmationController(){
    
    }
    
    public void setinternalResourceId(String intResourceId){
        internalResourceId = intResourceId;
        if (internalResourceId != null){
            EventDeliveryConfirmationHelper.EventConfirmationWrapper returnedEventConfirmationWrapper = EventDeliveryConfirmationHelper.getSessionsDetails(internalResourceId); 
            List<EventDeliveryConfirmationHelper.SessionWrapper> returnedSessions = returnedEventConfirmationWrapper.sessionList != null ? returnedEventConfirmationWrapper.sessionList : new List<EventDeliveryConfirmationHelper.SessionWrapper>(); 
            if (returnedSessions.size() > 0) Sessions = returnedSessions;   
            confirmAllLink = returnedEventConfirmationWrapper.confirmAllLink; 
            declineAllLink = returnedEventConfirmationWrapper.declineAllLink; 
            showConfirmDeclineAllButton = returnedEventConfirmationWrapper.showConfirmDeclineAllButton; 
        }
    }

    public String getinternalResourceId(){
        return internalResourceId; 
    }

 
	public List<EventDeliveryConfirmationHelper.SessionWrapper> getSessions() {
		return Sessions;
    }

    // Format event Start Time based on event's owner timezone
    public String getFormattedDatetime() {
        return EventDeliveryConfirmationHelper.getFormattedDateTimeHelper(campaignOwnerId, campaignId, startOrEndDate, defined_format, timeOrDate);
    }
}