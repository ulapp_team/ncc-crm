/*******************************************************************************************
* @name: NavigatorUtility
* @author: Jayson Labnao
* @created: 04-04-2022
* @description: Utility Methods for Navigator
*
* Changes (version)
* -------------------------------------------------------------------------------------------
*            No.   Date(dd-mm-yyy)  Author                Description
*            ----  ---------        --------------------  -----------------------------
* @version   1.0   04-04-2022       Jayson Labnao         Initial Version
*********************************************************************************************/
public with sharing class NavigatorUtility {

    

    private static void debugException(Exception e){
        System.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber());
    }

    /*******************************************************************************************
    * @name: ContactWrapper
    * @author: Jayson Labnao
    * @created: 04-04-2022
    * @description: Wrapper class for Contact Object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   04-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public class ContactWrapper{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String contactId {get; set;}
        @AuraEnabled
        public String title {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String firstName {get; set;}
        @AuraEnabled
        public String lastName {get; set;}
        @AuraEnabled
        public String loginToken {get; set;}
        @AuraEnabled
        public String profilePicture {get; set;}
        @AuraEnabled
        public String navigator {get; set;}
        @AuraEnabled
        public Boolean navigatorEnableLoginToken {get; set;}
    }
    
    /*******************************************************************************************
    * @name: buildContactObj
    * @author: Jayson Labnao
    * @created: 04-04-2022
    * @description: Build the wrapper object from the Contact.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   04-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version.
    *********************************************************************************************/
    public static ContactWrapper buildContactObj(Contact con){
        
        ContactWrapper conWrap = new ContactWrapper();
        try{ 
            if(con.Id != null){ conWrap.id = con.Id; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Contact_Id__c != null){ conWrap.contactId = con.Contact_Id__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Title != null){ conWrap.title = con.Title; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Name != null){ conWrap.name = con.Name; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Email != null){ conWrap.name = con.Email; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.FirstName != null){ conWrap.firstName = con.FirstName; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.LastName != null){ conWrap.lastName = con.LastName; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Login_Token__c != null){ conWrap.loginToken = con.Login_Token__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Profile_Picture_URL__c != null){ conWrap.profilePicture = con.Profile_Picture_URL__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Navigator__c != null){ conWrap.navigator = con.Navigator__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(con.Navigator__c != null && con.Navigator__r.Enable_Token_Login__c != null){ conWrap.navigatorEnableLoginToken = con.Navigator__r.Enable_Token_Login__c; }
        }
        catch(Exception e){ debugException(e); }
        
        return conWrap;
    }

    /*******************************************************************************************
    * @name: NavigatorWrapper
    * @author: Jayson Labnao
    * @created: 04-04-2022
    * @description: Wrapper class for Navigator Object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   04-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    * @version   1.1   24-05-2022       Jayson Labnao         [CCN-NAV-1674-DV] Added new Navigator Fields
    * @version   1.2.  18-10-2023       Jonah Baldero         [CCN-NAV-3417-DV] Added new Navigator Fields 
    *********************************************************************************************/
    public class NavigatorWrapper{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String headerText {get; set;}
        @AuraEnabled
        public String headerImage {get; set;}
        @AuraEnabled
        public String footerImage {get; set;}
        @AuraEnabled
        public String brandingLogo {get; set;}
        @AuraEnabled
        public String appLogo {get; set;}
        @AuraEnabled
        public String awsLink {get; set;}
        @AuraEnabled
        public String mascotImage {get; set;}
        @AuraEnabled
        public String mascotMessage {get; set;}
        @AuraEnabled
        public String loginPageUrl {get; set;}
        @AuraEnabled
        public Boolean enableTokenLogin {get; set;}
        @AuraEnabled
        public String themeColor1 {get; set;}
        @AuraEnabled
        public String themeColor2 {get; set;}
        @AuraEnabled
        public String themeColor3 {get; set;}
        @AuraEnabled
        public String themeColor4 {get; set;}
        @AuraEnabled
        public String changeAgentId {get; set;}
        @AuraEnabled
        public String changeAgentName {get; set;}
        @AuraEnabled
        public String changeAgentTitle {get; set;}
        @AuraEnabled
        public String changeAgentProfilePicUrl {get; set;}
        @AuraEnabled
        public String liveCallNumber {get; set;}
        @AuraEnabled
        public Boolean liveCallButton {get; set;}
        @AuraEnabled
        public Boolean scheduleVideoCallButton {get; set;}
        @AuraEnabled
        public String changeAgentDescription {get; set;}
        
        //START - CCN-NAV-3417-DV - Jonah
        @AuraEnabled
        public String landingPageHeroImageLinkField {get; set;}
        //END - CCN-NAV-3417-DV - Jonah

        //START - CCN-NAV-3550-DV - Leif
        @AuraEnabled
        public String changeAgentLabel {get; set;}
        //END - CCN-NAV-3550-DV - Leif
    }
    
    /*******************************************************************************************
    * @name: buildNavigatorObj
    * @author: Jayson Labnao
    * @created: 04-04-2022
    * @description: Build the wrapper object from the Navigator.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   04-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    * @version   1.1   24-05-2022       Jayson Labnao         [CCN-NAV-1674-DV] Added new Navigator Fields 
    * @version   1.2.  18-10-2023       Jonah Baldero         [CCN-NAV-3417-DV] Added new Navigator Fields
    *********************************************************************************************/
    public static NavigatorWrapper buildNavigatorObj(Navigator__c nav){

        NavigatorWrapper navWrap = new NavigatorWrapper();

        try{ 
            if(!String.isBlank(nav.Id)){ navWrap.id = nav.Id; }
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Name)){ navWrap.name = nav.Name; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Header_Text__c)){ navWrap.headerText = nav.Header_Text__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{
            if(!String.isBlank(nav.Header_Image__c)){ navWrap.headerImage = nav.Header_Image__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{
            if(!String.isBlank(nav.Footer_Image__c)){ navWrap.footerImage = nav.Footer_Image__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Branding_Logo__c)){ navWrap.brandingLogo = nav.Branding_Logo__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.App_Logo__c)){ navWrap.appLogo = nav.App_Logo__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.AWS_Link__c)){ navWrap.awsLink = nav.AWS_Link__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Mascot_Image__c)){ navWrap.mascotImage = nav.Mascot_Image__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Mascot_Message__c)){ navWrap.mascotMessage = nav.Mascot_Message__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Login_Page_URL__c)){ navWrap.loginPageUrl = nav.Login_Page_URL__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(nav.Enable_Token_Login__c != null){ navWrap.enableTokenLogin = nav.Enable_Token_Login__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Theme_Color_1__c)){ navWrap.themeColor1 = nav.Theme_Color_1__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Theme_Color_2__c)){ navWrap.themeColor2 = nav.Theme_Color_2__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Theme_Color_3__c)){ navWrap.themeColor3 = nav.Theme_Color_3__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Theme_Color_4__c)){ navWrap.themeColor4 = nav.Theme_Color_4__c; } 
        }
        catch(Exception e){ debugException(e); }
        
        try{ 
            if(!String.isBlank(nav.Change_Agent__c)){ navWrap.changeAgentId = nav.Change_Agent__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Change_Agent__r.Name)){ navWrap.changeAgentName = nav.Change_Agent__r.Name; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Change_Agent__r.Title)){ navWrap.changeAgentTitle = nav.Change_Agent__r.Title; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(nav.Change_Agent__r.Profile_Picture_URL__c)){ navWrap.changeAgentProfilePicUrl = nav.Change_Agent__r.Profile_Picture_URL__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{
            if(!String.isBlank(nav.Live_Call_Number__c)){ navWrap.liveCallNumber = nav.Live_Call_Number__c; } 
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(nav.Live_Call_Button__c != null){ navWrap.liveCallButton = nav.Live_Call_Button__c; } 
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(nav.Schedule_Video_Call_Button__c != null){ navWrap.scheduleVideoCallButton = nav.Schedule_Video_Call_Button__c; } 
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(!String.isBlank(nav.Change_Agent_Lead_Description__c)){ navWrap.changeAgentDescription = nav.Change_Agent_Lead_Description__c; } 
        }
        catch(Exception e){ debugException(e); }
        
        //START - CCN-NAV-3417-DV - Jonah
        try{ 
            if(!String.isBlank(nav.Landing_Page_Hero_Image_Link__c)){ navWrap.landingPageHeroImageLinkField = nav.Landing_Page_Hero_Image_Link__c; } 
        }
        catch(Exception e){ debugException(e); }
        //END - CCN-NAV-3417-DV - Jonah

        //START - CCN-NAV-3550-DV - Leif
        try{ 
            if(!String.isBlank(nav.Change_Agent_Lead_Label__c)){ navWrap.changeAgentLabel = nav.Change_Agent_Lead_Label__c; } 
        }
        catch(Exception e){ debugException(e); }
        //END - CCN-NAV-3550-DV - Leif
        
        return navWrap;
        
    }

    /*******************************************************************************************
    * @name: NavigatorSectionAttributeWrapper
    * @author: Jayson Labnao
    * @created: 06-04-2022
    * @description: Wrapper class for Navigator Section Attribute Object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   06-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public class NavigatorSectionAttributeWrapper{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String type {get; set;}
        @AuraEnabled
        public String alignment {get; set;}
        @AuraEnabled
        public Decimal sortOrder {get; set;}
        @AuraEnabled
        public String value {get; set;}
    }

    /*******************************************************************************************
    * @name: buildNavSectionAttributeObj
    * @author: Jayson Labnao
    * @created: 06-04-2022
    * @description: Build the wrapper object from the Navigator_Section_Attribute__c.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   06-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public static NavigatorSectionAttributeWrapper buildNavSectionAttributeObj(Navigator_Section_Attribute__c navSectionAttr){

        NavigatorSectionAttributeWrapper navSectionAttrWrap = new NavigatorSectionAttributeWrapper();

        try{ 
            if(!String.isBlank(navSectionAttr.Id)){ navSectionAttrWrap.id = navSectionAttr.Id; }
        }
        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(navSectionAttr.Name)){ navSectionAttrWrap.name = navSectionAttr.Name; }
        }

        catch(Exception e){ debugException(e); }

        try{ 
            if(!String.isBlank(navSectionAttr.Type__c)){ navSectionAttrWrap.type = navSectionAttr.Type__c; }
        }
        catch(Exception e){ debugException(e); }
        
        try{ 
            if(!String.isBlank(navSectionAttr.Alignment__c)){ navSectionAttrWrap.alignment = navSectionAttr.Alignment__c; }
        }
        catch(Exception e){ debugException(e); }
        
        try{ 
            if(navSectionAttr.Sort_Order__c != null){ navSectionAttrWrap.sortOrder = navSectionAttr.Sort_Order__c; }
        }
        catch(Exception e){ debugException(e); }
        
        try{ 
            if(!String.isBlank(navSectionAttr.Value__c)){ navSectionAttrWrap.value = navSectionAttr.Value__c; }
        }
        catch(Exception e){ debugException(e); }

        return navSectionAttrWrap;
    }


    

    /*******************************************************************************************
    * @name: NavigatorItemWrapper
    * @author: Jayson Labnao
    * @created: 06-04-2022
    * @description: Wrapper class for Navigator_Item__c Object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   06-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public class NavigatorItemWrapper{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String navigator {get; set;}
        @AuraEnabled
        public Decimal sortOrder {get; set;}
        @AuraEnabled
        public String pageUrl {get; set;}
        @AuraEnabled
        public String sldsIconName {get; set;}
        @AuraEnabled
        public String expandedItems {get; set;}
        @AuraEnabled
        public String imageUrl {get; set;}
    }
    
    /*******************************************************************************************
    * @name: buildContactObj
    * @author: Jayson Labnao
    * @created: 04-04-2022
    * @description: Build the wrapper object from the Navigator_Item__c.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   06-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public static NavigatorItemWrapper buildNavItemWrapper(Navigator_Item__c navTab){
        
        // SELECT Id, Name, Navigator__c, Sort_Order__c, 
        // Page_URL__c, SLDS_Icon_Name__c,Expanded_Items__c,Image_URL__c
        NavigatorItemWrapper navTabWrap = new NavigatorItemWrapper();
        try{ 
            if(navTab.Id != null){ navTabWrap.id = navTab.Id; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Name != null){ navTabWrap.name = navTab.Name; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Navigator__c != null){ navTabWrap.navigator = navTab.Navigator__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Sort_Order__c != null){ navTabWrap.sortOrder = navTab.Sort_Order__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Page_URL__c != null){ navTabWrap.pageUrl = navTab.Page_URL__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.SLDS_Icon_Name__c != null){ navTabWrap.sldsIconName = navTab.SLDS_Icon_Name__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Expanded_Items__c != null){ navTabWrap.expandedItems = navTab.Expanded_Items__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(navTab.Image_URL__c != null){ navTabWrap.imageUrl = navTab.Image_URL__c; }
        }
        catch(Exception e){ debugException(e); }
        
        return navTabWrap;
    }


    
    /*******************************************************************************************
    * @name: EventWrapper
    * @author: Jayson Labnao
    * @created: 11-04-2022
    * @description: Wrapper class for Event Object
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   11-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public class EventWrapper{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public DateTime startDateTime {get; set;}
        @AuraEnabled
        public DateTime endDateTime {get; set;}
        @AuraEnabled
        public String timeZone {get; set;}
        @AuraEnabled
        public String eventTimeDetails {get; set;}

    }
    
    /*******************************************************************************************
    * @name: buildEventObj
    * @author: Jayson Labnao
    * @created: 11-04-2022
    * @description: Build the wrapper object from the Event.
    *
    * Changes (version)
    * -------------------------------------------------------------------------------------------
    *            No.   Date(dd-mm-yyy)  Author                Description
    *            ----  ---------        --------------------  -----------------------------
    * @version   1.0   11-04-2022       Jayson Labnao         [PDO/GPO Namespace Issues fix] 
    *                                                         Initial Version. 
    *********************************************************************************************/
    public static EventWrapper buildEventObj(Event__c evt){
        EventWrapper evtWrap = new EventWrapper();
        try{ 
            if(evt.Id != null){ evtWrap.id = evt.Id; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(evt.Name != null){ evtWrap.name = evt.Name; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(evt.Start_Date_Time__c != null){ evtWrap.startDateTime = evt.Start_Date_Time__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(evt.End_Date_Time__c != null){ evtWrap.endDateTime = evt.End_Date_Time__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(evt.Time_Zone__c != null){ evtWrap.timeZone = evt.Time_Zone__c; }
        }
        catch(Exception e){ debugException(e); }
        try{ 
            if(evt.Event_Time_Details__c != null){ evtWrap.eventTimeDetails = evt.Event_Time_Details__c; }
        }
        catch(Exception e){ debugException(e); }
        
        return evtWrap;
    }

}